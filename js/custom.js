
function openPage(pageName, elmnt) {
  // Hide all elements with class="tabcontent" by default */
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }

  // Show the specific tab content
  document.getElementById(pageName).style.display = "block";

}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
	
$('.btn-close').click(function(e){
	e.preventDefault();
	$(this).parent().parent().parent().fadeOut();
});
$('.btn-minimize').click(function(e){
	e.preventDefault();
	var $target = $(this).parent().parent().next('.box-content');
	if($target.is(':visible')) $('i',$(this)).removeClass('fa-chevron-up').addClass('fa-chevron-down');
	else 					   $('i',$(this)).removeClass('fa-chevron-down').addClass('fa-chevron-up');
	$target.slideToggle();
});	