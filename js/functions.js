//devolver las ciudades por provincia
$("#newProvincia").change(function(){
			searchByProvince($(this).val(), "newCiudad");
		});

		$("#editProvincia").change(function(){
			searchByProvince($(this).val(), "editCiudad");
		});

		$("#newCiudad").change(function() {
			$("#newCp").val($(this).find("option:selected").attr("cp"));
		});
		
		$("#editCiudad").change(function() {
			$("#editCp").val($(this).find("option:selected").attr("cp"));
		});


		var lastRowChanged = -1;
		function changeBackground(tr){
			if(lastRowChanged > 0){
				$("#"+lastRowChanged).removeAttr("style");
			}
			$(tr).attr("style","background-color:#F0EBED");
			lastRowChanged = $(tr).attr("id");
		}
		function clearBackground(tr){
			$(tr).removeAttr("style");
		}
		
		
		//devolver los tipos de liquidaciones de un concepto
function getTypesLiquidationByConcept(id){
	var frmTipo;
	frmTipo="#editConceptDataTypeLiquidation";
$.ajax({
		type: "GET",
		data: {q: id},
		url: "getTypeLiquidationByConcept.php",
		success: function (data) {
			//$(frmTipo).html(data);
			$(frmTipo).html(data);
			
		}
	});

}




/*
document.getElementById('previ').addEventListener('click', (e) => {
  e.preventDefault()
document.getElementById('closeOrNo').value = 0;
frmNewcloseBox.setAttribute('action', 'closeBoxPrevi.php');
frmNewcloseBox.setAttribute('target', '_blank');
document.getElementById('frmNewcloseBox').submit();


});*/





		function debListEmail(){
	    if(parseInt($("#iUserID").val()) !=0 && parseInt($("#iUserID").val()) !=-1){
			
		
		     
			 		  toastr.info("<br><button type='button' id='confirmationRevertYes' class='btn green'>Si</button><button type='button' id='confirmationRevertNo' class='btn default'>No</button>","Desea enviar la factura por email?",
					{
						closeButton: false,
						allowHtml: true,
						onShown: function (toast) {
							
							$("#confirmationRevertYes").click(function(){
								
						var checkBoxes = document.getElementsByTagName('input');
			var total=0;
					//alert(checkBoxes.length);
							var param="";
							for (var counter=0; counter < checkBoxes.length; counter++) {
											if (checkBoxes[counter].type.toUpperCase()=='CHECKBOX' && checkBoxes[counter].checked == true) {
															
															param += checkBoxes[counter].value + ",";
															total += parseFloat(document.getElementById("td_bill_"+checkBoxes[counter].value).innerHTML);
											}
							}
			param = param.substr(0,param.length-1);
//$total=document.getElementById("total").value;
window.open('copyPrintingByEmail.php?param='+param,'_blank');
								
								
					  
							});
							
							 $("#confirmationRevertNo").click(function(){
										  toastr.info('Cancelado por Usuario.', 'No se pudo generar el detalle.');
										  //toastr.success('Datos No guardados', 'Editar Cargo');
										   //$('#full-widthEdit').modal('hide');
										  
								  });
				  
						  }
					});
			 

	}	
}


		function chainPayments(){
	
				var checkBoxes = document.getElementsByTagName('input');
			var total=0;
			var nombre ="";
							var param="";
							//alert(checkBoxes.length);
							for (var counter=0; counter < checkBoxes.length; counter++) {
											if (checkBoxes[counter].type.toUpperCase()=='CHECKBOX' && checkBoxes[counter].checked == true) {
														
															param += checkBoxes[counter].value + ",";
															total += parseFloat(document.getElementById("td_bill_"+checkBoxes[counter].value).innerHTML);
															nombre += (document.getElementById("td_username_"+checkBoxes[counter].value).innerHTML) + " ";
											}
							}
			param = param.substr(0,param.length-1);
			const newData = nombre.replace(/&nbsp[;]?/ig, '');
		 document.getElementById('nombre').value = newData;
		 document.getElementById('param').value = param;
        document.getElementById('total').value = Math.round(total*100)/100;		
		
	}



	   function newChainPay(){

  toastr.info("<br><button type='button' id='confirmationRevertYes' class='btn green'>Si</button><button type='button' id='confirmationRevertNo' class='btn default'>No</button>","ATENCION va a cambiar el medio de pago, desea continuar?",
					{
						closeButton: false,
						allowHtml: true,
						onShown: function (toast) {
							
							$("#confirmationRevertYes").click(function(){
								
					  var newSummaryPayment = document.getElementById('newSummaryPayment').value;
	   var param = document.getElementById('param').value;
		var dataString = 'sAction=changePay&param=' + param + '&newSummaryPayment=' + newSummaryPayment;
		$.ajax({  
			type: "post",  
			data: dataString,
			dataType: "json",  
			url: "paymentsAll.php",  
			success: function(data) {
				if(data.status == "OK"){
					  toastr.success('Datos actualizados', 'El medio de pago fue modificado');
					//showSuccessToast("El pago fue cambiado");
					//var int=self.setInterval("refresh()",500);
					$("#frmsearch").submit();
				}else{
					 toastr.info(data.status, 'No se pudo cambiar la forma de pago.');
					//showErrorToast(data.status);
				}
			},
			error:function (xhr, ajaxOptions, thrownError){
				 toastr.info(xhr.responseText, 'No se pudo cambiar la forma de pago.');
					//showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
			}
			});
								
								
					  
							});
							
							 $("#confirmationRevertNo").click(function(){
										  toastr.info('Cancelado por Usuario.', 'No se pudo cambiar la forma de pago.');
										  //toastr.success('Datos No guardados', 'Editar Cargo');
										   //$('#full-widthEdit').modal('hide');
										  
								  });
				  
						  }
					});


		
	}



			function showuserInvoiceData(ID){
	    if(parseInt($("#iID").val()) !=0 && parseInt($("#iID").val()) !=-1){
		     
			 		  toastr.info("<br><button type='button' id='confirmationRevertYes' class='btn green'>Si</button><button type='button' id='confirmationRevertNo' class='btn default'>No</button>","Confirma generar el detalle del pago?",
					{
						closeButton: false,
						allowHtml: true,
						onShown: function (toast) {
							
							$("#confirmationRevertYes").click(function(){
								
					 $("#idBill").val(ID);
					window.open('detailsInvoice.php?id='+ID,'_blank');
								
								
					  
							});
							
							 $("#confirmationRevertNo").click(function(){
										  toastr.info('Cancelado por Usuario.', 'No se pudo generar el detalle.');
										  //toastr.success('Datos No guardados', 'Editar Cargo');
										   //$('#full-widthEdit').modal('hide');
										  
								  });
				  
						  }
					});
			 

	}	
}






		
		
		
			function showGeneratedMovement(ID,iPointSale){
	    if(parseInt($("#iID").val()) !=0 && parseInt($("#iID").val()) !=-1){
		     
			 		  toastr.info("<br><button type='button' id='confirmationRevertYes' class='btn green'>Si</button><button type='button' id='confirmationRevertNo' class='btn default'>No</button>","Confirma generar la factura?",
					{
						closeButton: false,
						allowHtml: true,
						onShown: function (toast) {
							
							$("#confirmationRevertYes").click(function(){
								
					 $("#sAction").val("generatedBill");
				 $("#idMeter").val(ID);
				 $("#pointSale").val(iPointSale);
				 frmuserBillingResult.setAttribute("target", "_blank");
				
				document.frmuserBillingResult.submit();
				
				$("#sAction").val("search");
				document.frmsearch.submit();
								
								
					  
							});
							
							 $("#confirmationRevertNo").click(function(){
										  toastr.info('Cancelado por Usuario.', 'No se pudo generar la factura.');
										  //toastr.success('Datos No guardados', 'Editar Cargo');
										   //$('#full-widthEdit').modal('hide');
										  
								  });
				  
						  }
					});
			 

	}	
}


function deleteMovement(ID){

	
	  if(parseInt($("#iID").val()) !=0 && parseInt($("#iID").val()) !=-1){
			
			
			
			  toastr.info("<br><button type='button' id='confirmationRevertYes' class='btn green'>Si</button><button type='button' id='confirmationRevertNo' class='btn default'>No</button>","Confirma eliminar el movimiento?",
					{
						closeButton: false,
						allowHtml: true,
						onShown: function (toast) {
							
							$("#confirmationRevertYes").click(function(){
								
				var dataString = 'sAction=deleteMovement&idMovements=' + ID;
			$.ajax({  
			type: "post",  
			data: dataString,
			dataType: "json",  
			url: "userBilling.php",  
			success: function(data) {
				if(data.status == "OK"){
					toastr.success('Datos guardados', 'El movimiento fue eliminado');
					
					$("#frmsearch").submit();
				}else{
					toastr.success('Datos No guardados', 'El movimiento no fue eliminado');
					
				}
			},
			error:function (xhr, ajaxOptions, thrownError){
					 toastr.info('Cancelado por Error.', 'No se pudo eliminar.');
			}
								
								
					  
							});
							
								});
							
							 $("#confirmationRevertNo").click(function(){
										  toastr.info('Cancelado por Usuario.', 'No se pudo eliminar la factura.');
										  //toastr.success('Datos No guardados', 'Editar Cargo');
										   //$('#full-widthEdit').modal('hide');
										  
								  });
				  
						  }
					});
			
			
			
			
		
		
	}				
	}
		
		
		
		//actualizacion segundo vencimiento
		
			function newUpdateBaby(){
				
				
				
				
				

			if(parseInt($("#sAction").val()) !=0 && parseInt($("#sAction").val()) !=-1){
				 
						  toastr.info("<br><button type='button' id='confirmationRevertYes' class='btn green'>Si</button><button type='button' id='confirmationRevertNo' class='btn default'>No</button>","Confirma la actualizacion?",
					{
						closeButton: false,
						allowHtml: true,
						onShown: function (toast) {
							
							$("#confirmationRevertYes").click(function(){
								
											$("#newMovementsAction").val("newMovements");
				  $.ajax({  
				  type: "post",  
				  data: $("#newActualizacion").serialize(),
				  dataType: "json",  
				  url: "updateSecond.php",  
				  success: function(data) {
					  if(data.status == "OK"){
						  
						  toastr.success('Datos actualizados', '2 vencimiento');
						 // $("#frmsearch").submit();
						  
					  }else{
						  toastr.success('Datos No actualizados', '2 vencimiento');
					  }
				  },
				  error:function (xhr, ajaxOptions, thrownError){
						  toastr.info('Cancelado por Error.', 'No se pudo actualizar.');
				  }
				  });
								
								
					  
							});
							
							 $("#confirmationRevertNo").click(function(){
										  toastr.info('Cancelado por Usuario.', 'No se pudo agregar el elemento.');
										  //toastr.success('Datos No guardados', 'Editar Cargo');
										   //$('#full-widthEdit').modal('hide');
										  
								  });
				  
						  }
					});





			
		}	
	}
		
		
		
		
		
		
		
		
				function newMovements(){

			if(parseInt($("#iUserID").val()) !=0 && parseInt($("#iUserID").val()) !=-1){
				 
						  toastr.info("<br><button type='button' id='confirmationRevertYes' class='btn green'>Si</button><button type='button' id='confirmationRevertNo' class='btn default'>No</button>","Confirma agregar la novedad?",
					{
						closeButton: false,
						allowHtml: true,
						onShown: function (toast) {
							
							$("#confirmationRevertYes").click(function(){
								
											$("#newMovementsAction").val("newMovements");
				  $.ajax({  
				  type: "post",  
				  data: $("#frmNewMovements").serialize(),
				  dataType: "json",  
				  url: "userBilling.php",  
				  success: function(data) {
					  if(data.status == "OK"){
						  
						  toastr.success('Datos guardados', 'Agregar Novedad');
						  $("#frmsearch").submit();
						  
					  }else{
						  toastr.success('Datos No guardados', 'Agregar Novedad');
					  }
				  },
				  error:function (xhr, ajaxOptions, thrownError){
						  toastr.info('Cancelado por Error.', 'No se pudo agregar la novedad.');
				  }
				  });
								
								
					  
							});
							
							 $("#confirmationRevertNo").click(function(){
										  toastr.info('Cancelado por Usuario.', 'No se pudo agregar el elemento.');
										  //toastr.success('Datos No guardados', 'Editar Cargo');
										   //$('#full-widthEdit').modal('hide');
										  
								  });
				  
						  }
					});





			
		}	
	}








		$("#btnNewSubmitNew").click(function() {
			newMovements();
			return false;
		});


		function showuserBillingData(ID,IDType){

			if(parseInt($("#iID").val()) !=0 && parseInt($("#iID").val()) !=-1){
				 
						  toastr.info("<br><button type='button' id='confirmationRevertYes' class='btn green'>Si</button><button type='button' id='confirmationRevertNo' class='btn default'>No</button>","Confirma generar la copia de la factura?",
					{
						closeButton: false,
						allowHtml: true,
						onShown: function (toast) {
							
							$("#confirmationRevertYes").click(function(){
								
								$("#idBill").val(ID);
								//document.frmuserCopyBillingResult.submit();
								
								 if (IDType==1){
								 
									 window.open('copyPrinting.php?id='+ID,'_blank');
								 } else {
								 
								 
								 if (IDType==3 || IDType==4){
								 
									  window.open('copyPrintingNote.php?id='+ID,'_blank');
									 
								 } else {
								 
								 
								 
								   if (IDType==5){
									  window.open('copyPrintingNs.php?id='+ID,'_blank');
								 
								 } else {
								 
								 
										if (IDType==6){ 
									   
										window.open('copyPrintingPa.php?id='+ID,'_blank');
										
									   } else {
									   
										window.open('copyPrintingAn.php?id='+ID,'_blank');
									   }
								 
								 
								 }
								 
									 
									 
									 
									 
									 
									 
									 
								 }
								 
								 }
					  
							});
							
							 $("#confirmationRevertNo").click(function(){
										  toastr.info('Cancelado por Usuario.', 'No se pudo eliminar el elemento.');
										  //toastr.success('Datos No guardados', 'Editar Cargo');
										   //$('#full-widthEdit').modal('hide');
										  
								  });
				  
						  }
					});





			
		}	
	}
	
	
	
	
	
	  //edicion

function newInvoiceSee(action){



switch (action) {

		case 'frmSearchSummary':

		 var impresion = document.querySelector('input[name="newPrintingOption"]:checked').value;
	 
	 if (impresion==4)  {
		   direccion = 'invoiceforcoopenextbematech.php';
	 } else {
		   direccion = 'invoiceForCoopeNext.php';
	 }

		form='#frmSearchSummary';	

		aswer=( ($('#cantPagos').val() > 0) || ($('#cantChequesT').val() > 0) || ($('#cantCheques').val() > 0));

        break;
		
		case 'frmSearchSummaryNext':

		direccion='invoiceUserNext.php';

		form='#frmSearchSummaryNext';	

		aswer=( ($('#cantPagosNext').val() > 0) || ($('#cantChequesNext').val() > 0));

        break;
		
		case 'frmSearchSummaryNote':

		direccion='invoiceUserNote.php';

		form='#frmSearchSummaryNote';	

		aswer=( ($('#totalNote').val() > 0) || ($('#tipoNote').val() > 0));

        break;

}



	if( aswer ){



		toastr.info("<br><button type='button' id='confirmationRevertYes' class='btn green'>Si</button><button type='button' id='confirmationRevertNo' class='btn default'>No</button>",'Desea generar el recibo?',

  {

      closeButton: false,

      allowHtml: true,

      onShown: function (toast) {

		  

          $("#confirmationRevertYes").click(function(){

            	$("#sAction").val(action);

				var iUserID = document.getElementById("iUserID");


switch (action) {

		case 'frmSearchSummary':

				var nombre = document.getElementById("nombre");

				var param = document.getElementById("param");
				
				var comentario = document.getElementById("comentario");
				
				var totalFactura = document.getElementById("total");

				var total = 0;

				var fechaR= document.getElementById("fecha");

				var newSummaryPayment = '';

				var cantPagos = document.getElementById("cantPagos").value;

				//var email = document.getElementsByName("newConceptRadio")[0].value;

				var email = $('input:radio[name=newConceptRadio]:checked').val();

				//document.getElementById("newConceptRadio").value;

				

				var cheques = '&no_data=0';

				var trans = '&no_datat=0';

				

				for (e = 1; e <= cantPagos; e++) {

				var newSPayment = document.getElementsByName("newSummaryPayment-"+e)[0].value;

				//alert(newSPayment);

				var newSPaymentImporte = document.getElementsByName("total-"+e)[0].value;

				//alert(newSPaymentImporte);

				total = total + parseFloat(newSPaymentImporte);

				newSummaryPayment +='&pago-'+e+'='+newSPayment+'&importe-'+e+'='+newSPaymentImporte;


				}

				

				var trans = '';

				var cantt = document.getElementById("cantChequesT").value;

				if (cantt > 0 ) {

					for (i = 1; i <= cantt; i++) {

						var bancoT = document.getElementsByName("bancoT-"+i)[0].value;

						var bancoDestinoT = document.getElementsByName("bancoDestinoT-"+i)[0].value;

						var fechaT = document.getElementsByName("fechaT-"+i)[0].value;

						var refT = document.getElementsByName("refT-"+i)[0].value;

						var valorChequeT = document.getElementsByName("valorChequeT-"+i)[0].value;

						total = total + parseFloat(valorChequeT);

						var numberT = document.getElementsByName("numberT-"+i)[0].value;

						trans +='&bancoT-'+i+'='+bancoT+'&valorChequeT-'+i+'='+valorChequeT+'&numberT-'+i+'='+numberT+'&bancoDestinoT-'+i+'='+bancoDestinoT+'&fechaT-'+i+'='+fechaT+'&refT-'+i+'='+refT;

					}

				}

				

				var cheques = '';

					var cant = document.getElementById("cantCheques").value;

					if (cant > 0 ) {

					for (i = 1; i <= cant; i++) {

						var banco = document.getElementsByName("banco-"+i)[0].value;

						var fecha = document.getElementsByName("fecha-"+i)[0].value;

						var valorCheque = document.getElementsByName("valorCheque-"+i)[0].value;

						total = total + parseFloat(valorCheque);

						var number = document.getElementsByName("number-"+i)[0].value;

						cheques +='&banco-'+i+'='+banco+'&fecha-'+i+'='+fecha+'&valorCheque-'+i+'='+valorCheque+'&number-'+i+'='+number;

					}

					}

				
				//window.open('invoiceForCoopeNext.php?iUserID='+iUserID.value+'&nombre='+nombre.value+'&param='+param.value+'&comentario='+comentario.value+'&totalFactura='+totalFactura.value+'&total='+total+'&'+newSummaryPayment+cheques+trans+'&fecha='+fechaR.value+'&email='+email,'_blank');
window.open(direccion+'?impresion='+impresion+'&iUserID='+iUserID.value+'&nombre='+nombre.value+'&param='+param.value+'&comentario='+comentario.value+'&totalFactura='+totalFactura.value+'&total='+total+'&'+newSummaryPayment+cheques+trans+'&fecha='+fechaR.value+'&email='+email,'_blank');

				

        break;
		
		case 'frmSearchSummaryNext':
//pago a cuenta


				var nombre = document.getElementById("nombreNext");

				//var param = document.getElementById("param");
				
				var comentario = document.getElementById("comentarioNext");
				
				var totalFactura = document.getElementById("totalNext");

				var total = 0;

				var fechaR = document.getElementById("fechaNext");

				var newSummaryPayment = '';

				var cantPagos = document.getElementById("cantPagosNext").value;

				//var email = document.getElementsByName("newConceptRadio")[0].value;

				//var email = $('input:radio[name=newConceptRadio]:checked').val();

				//document.getElementById("newConceptRadio").value;

				

				var cheques = '&no_data=0';

				var trans = '&no_datat=0';

				

				for (e = 1; e <= cantPagos; e++) {

				var newSPayment = document.getElementsByName("newSummaryPaymentNext-"+e)[0].value;

				//alert(newSPayment);

				var newSPaymentImporte = document.getElementsByName("totalNext-"+e)[0].value;

				//alert(newSPaymentImporte);

				total = total + parseFloat(newSPaymentImporte);

				newSummaryPayment +='&pago-'+e+'='+newSPayment+'&importe-'+e+'='+newSPaymentImporte;


				}

				

			/*	var trans = '';

				var cantt = document.getElementById("cantChequesT").value;

				if (cantt > 0 ) {

					for (i = 1; i <= cantt; i++) {

						var bancoT = document.getElementsByName("bancoT-"+i)[0].value;

						var bancoDestinoT = document.getElementsByName("bancoDestinoT-"+i)[0].value;

						var fechaT = document.getElementsByName("fechaT-"+i)[0].value;

						var refT = document.getElementsByName("refT-"+i)[0].value;

						var valorChequeT = document.getElementsByName("valorChequeT-"+i)[0].value;

						total = total + parseFloat(valorChequeT);

						var numberT = document.getElementsByName("numberT-"+i)[0].value;

						trans +='&bancoT-'+i+'='+bancoT+'&valorChequeT-'+i+'='+valorChequeT+'&numberT-'+i+'='+numberT+'&bancoDestinoT-'+i+'='+bancoDestinoT+'&fechaT-'+i+'='+fechaT+'&refT-'+i+'='+refT;

					}

				}*/

				

				var cheques = '';

					var cant = document.getElementById("cantChequesNext").value;
					

					if (cant > 0 ) {

					for (i = 1; i <= cant; i++) {

						var banco = document.getElementsByName("bancoNext-"+i)[0].value;

						var fecha = document.getElementsByName("fechaNext-"+i)[0].value;

						var valorCheque = document.getElementsByName("valorChequeNext-"+i)[0].value;

						total = total + parseFloat(valorCheque);

						var number = document.getElementsByName("numberNext-"+i)[0].value;

						cheques +='&banco-'+i+'='+banco+'&fecha-'+i+'='+fecha+'&valorCheque-'+i+'='+valorCheque+'&number-'+i+'='+number;

					}

					}

				
				window.open('invoiceUserNext.php?iUserID='+iUserID.value+'&nombre='+nombre.value+'&comentario='+comentario.value+'&totalFactura='+totalFactura.value+'&total='+total+'&'+newSummaryPayment+cheques+'&fecha='+fechaR.value,'_blank');

	

        break;
		
		
		
		case 'frmSearchSummaryNote':
//notas


				var nombre = document.getElementById("nombreNote");
				
				var comentario = document.getElementById("comentarioNote");
				
				var totalFactura = document.getElementById("totalNote");
				
				var tipo = document.getElementById("tipoNote");

				var fechaR = document.getElementById("fechaNote");


				window.open('invoiceUserNote.php?iUserID='+iUserID.value+'&nombre='+nombre.value+'&comentario='+comentario.value+'&totalFactura='+totalFactura.value+'&fecha='+fechaR.value+'&tipo='+tipo.value,'_blank');

	

        break;

}





				

				
				
				
				
				
				toastr.success('Ha agregado un recibo.', 'Recibo generado correctamente.');

				$('#full-width').modal('hide');
				
				
				//var iUserID = document.getElementById("iUserID");
				document.getElementById("iUserID").value = iUserID.value;
				
				//$("#iUserID").val(iUserID);

				$("#frmsearch").submit();
				

          });

		  

		   $("#confirmationRevertNo").click(function(){

						toastr.info('Cancelado por Usuario.', 'Datos No Guardados.');

						//toastr.success('Datos No guardados', 'Editar Cargo');

						 $('#full-width').modal('hide');

						

				});



        }

  });

		



	

		

		}else{

			toastr.warning('Completo todos los datos?','Datos No guardados');

			//$.notific8(data.status);

			//showErrorToast("El usuario no existe. Modifique los datos y reintente");

		}

	}


function searchByProvince(id, selectId){
		var dataString = 'sAction=searchByProvince&newProvince=' + id;
		$.ajax({  
		type: "post",  
		data: dataString,  
		dataType: "json",  
		url: "providerABM.php",  
		success: function(data) {
			if(data){
					$("#"+selectId).html("");
					var htmlToAdd = "<option cp='' id='' value=''></option>";
				$.each(data,function(index, value){
					htmlToAdd+="<option cp='"+value.zip+"' id='"+value.idProvince+"' value='"+value.id+"'>"+data[index].name+"</option>";
				});
				$("#"+selectId).html(htmlToAdd);
			}else{
				showErrorToast("No se recuperaron datos de provincia");
			}
			return "finished";  
		},
		error:function (xhr, ajaxOptions, thrownError){
				showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		}
		});  
	}





	//devolver las ciudades por provincia
$("#newProvinciaUser").change(function(){
	searchByProvinceUser($(this).val(), "newCiudadUser");
});

$("#editProvinciaUser").change(function(){
	searchByProvinceUser($(this).val(), "editCiudadUser");
});

$("#newCiudadUser").change(function() {
	$("#newCpUser").val($(this).find("option:selected").attr("cp"));
});

$("#editCiudadUser").change(function() {
	$("#editCpUser").val($(this).find("option:selected").attr("cp"));
});

function searchByProvinceUser(id, selectId){
var dataString = 'sAction=searchByProvince&newProvince=' + id;
$.ajax({  
type: "post",  
data: dataString,  
dataType: "json",  
url: "usersABM.php",  
success: function(data) {
	if(data){
			$("#"+selectId).html("");
			var htmlToAdd = "<option cp='' id='' value=''></option>";
		$.each(data,function(index, value){
			htmlToAdd+="<option cp='"+value.zip+"' id='"+value.idProvince+"' value='"+value.id+"'>"+data[index].name+"</option>";
		});
		$("#"+selectId).html(htmlToAdd);
	}else{
		showErrorToast("No se recuperaron datos de provincia");
	}
	return "finished";  
},
error:function (xhr, ajaxOptions, thrownError){
		showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
}
});  
}
	
	 function getId(inputTmp){
		 var nombre = inputTmp.name;
		 var valor  = inputTmp.value;
		 var res = nombre.split("-");
		 var precio = document.getElementsByName('newPrecio-'+res[1])[0].value;
		 var cantidad = document.getElementsByName('newCantidad-'+res[1])[0].value;
		 var total = document.getElementsByName('newPrecioTotal-'+res[1]);
		 
		total[0].value =  (cantidad*precio).toFixed(4);                       
            }
	
	
	function selectBillByInfo(){
		var newNume = document.getElementById('newNume').value;
		var newPoint = document.getElementById('newPoint').value;
		var newType = document.getElementById('newType').value;
		var newProvider = document.getElementById('newProvider').value;
		var dataString = 'sAction=searchBillByInfo&newNume=' + newNume + '&newPoint='+newPoint+'&newType='+newType+'&newProvider='+newProvider;
		$.ajax({  
		type: "post",  
		data: dataString,  
		dataType: "json",  
		url: "providerABM.php",  
		success: function(data) {
			if(data){
				if(data.queryStatus == "OK"){
					
					toastr.success('Factura no encontrada.', 'Puede guardar sus datos.');
				
				} else {
					toastr.error('Algo salio mal','Factura Duplicada');
				}
			}else{
				toastr.error('Algo salio mal','Factura Duplicada');
			}
			return "finished";  
		},
		error:function (xhr, ajaxOptions, thrownError){
				toastr.error('Algo salio mal','Factura Duplicada');
		}
		});  
	}	
	
	
	
	function checkYearByBill(){
		var newNume = document.getElementById('newDate').value;
		var dataString = 'sAction=searchBillByInfo&newNume=' + newNume;
		$.ajax({  
		type: "post",  
		data: dataString,  
		dataType: "json",  
		url: "movementsInProviderABM.php",  
		success: function(data) {
			if(data){
				if(data.status == "OK"){
					
					toastr.success('Año correcto.', 'Puede guardar sus datos.');
				
				} else {
					toastr.error('Algo salio mal','Año cerrado');
				}
			}else{
				toastr.error('Algo salio mal','Año Cerrado');
			}
			return "finished";  
		},
		error:function (xhr, ajaxOptions, thrownError){
				toastr.error('Algo salio mal','Año Cerrado');
		}
		});  
	}	
	
	
	
	function checkYearByBillUser(){
		var newNume = document.getElementById('newDate').value;
		var dataString = 'sAction=searchBillByInfo&newNume=' + newNume;
		$.ajax({  
		type: "post",  
		data: dataString,  
		dataType: "json",  
		url: "movementsOutProviderABMUser.php",  
		success: function(data) {
			if(data){
				if(data.status == "OK"){
					
					toastr.success('Año correcto.', 'Puede guardar sus datos.');
				
				} else {
					toastr.error('Algo salio mal','Año cerrado');
				}
			}else{
				toastr.error('Algo salio mal','Año Cerrado');
			}
			return "finished";  
		},
		error:function (xhr, ajaxOptions, thrownError){
				toastr.error('Algo salio mal','Año Cerrado');
		}
		});  
	}	
	
	

	function selectCityById(idProvince, selectId, idCity){
		var dataString = 'sAction=searchByProvince&newProvince=' + idProvince;
		$.ajax({  
		type: "post",  
		data: dataString,  
		dataType: "json",  
		url: "providerABM.php",  
		success: function(data) {
			if(data){
					$("#"+selectId).html("");
					var htmlToAdd = "<option cp='' id='' value=''></option>";
				$.each(data,function(index, value){
					if(value.id != idCity){
						htmlToAdd+="<option cp='"+value.zip+"' id='"+value.idProvince+"' value='"+value.id+"'>"+data[index].name+"</option>";
					}else{
						htmlToAdd+="<option cp='"+value.zip+"' id='"+value.idProvince+"' value='"+value.id+"' selected='selected'>"+data[index].name+"</option>";
					}
				});
				$("#"+selectId).html(htmlToAdd);
			}else{
				showErrorToast("No se recuperaron datos de provincia");
			}
			return "finished";  
		},
		error:function (xhr, ajaxOptions, thrownError){
				showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		}
		});  
	}	
	


	function selectCityByIdUser(idProvince, selectId, idCity){
		var dataString = 'sAction=searchByProvince&newProvince=' + idProvince;
		$.ajax({  
		type: "post",  
		data: dataString,  
		dataType: "json",  
		url: "usersABM.php",  
		success: function(data) {
			if(data){
					$("#"+selectId).html("");
					var htmlToAdd = "<option cp='' id='' value=''></option>";
				$.each(data,function(index, value){
					if(value.id != idCity){
						htmlToAdd+="<option cp='"+value.zip+"' id='"+value.idProvince+"' value='"+value.id+"'>"+data[index].name+"</option>";
					}else{
						htmlToAdd+="<option cp='"+value.zip+"' id='"+value.idProvince+"' value='"+value.id+"' selected='selected'>"+data[index].name+"</option>";
					}
				});
				$("#"+selectId).html(htmlToAdd);
			}else{
				showErrorToast("No se recuperaron datos de provincia");
			}
			return "finished";  
		},
		error:function (xhr, ajaxOptions, thrownError){
				showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		}
		});  
	}

/*	
function getCiudades(id,tipo,cit){

		var frmTipo;
		if(tipo==1){
			frmTipo="#newCiudad";
		} else{
			frmTipo="#editCiudad";
		}
		//getCpByCity('',tipo);
		
$.ajax({
			type: "GET",
			data: {q: id,sub:cit,t:tipo},
			url: "getCiudades.php",
			success: function (data) {
				$(frmTipo).html(data);
			}
		});

}

function getCpByCity(val,tipo){

		var frmTipo;
		if(tipo==1){
			frmTipo="newCp";
		} else{
			frmTipo="editCp";
		}
		
document.getElementById(frmTipo).value=+val;

}*/

//devolver los niveles por empleador
function getNiveles(id,tipo,cit){

		var frmTipo;
		if(tipo==1){
			frmTipo="#newNivel";
		} else{
			frmTipo="#editNivel";
		}
		
		
$.ajax({
			type: "GET",
			data: {q: id,sub:cit},
			url: "getLevelsBySchool.php",
			success: function (data) {
				$(frmTipo).html(data);
			}
		});

}


//devolver los niveles por empleador para agregar conceptos
function getNivelesConcept(id,tipo,cit){

		var frmTipo;
		if(tipo==1){
			frmTipo="#newNivelConcept";
		} else{
			frmTipo="#editNivelConcept";
		}
		
		
$.ajax({
			type: "GET",
			data: {q: id,sub:cit},
			url: "getLevelsBySchool.php",
			success: function (data) {
				$(frmTipo).html(data);
			}
		});

}

//devolver los niveles por empleador para agregar conceptos
function getNivelesConceptDel(id,tipo,cit){

		var frmTipo;
		if(tipo==1){
			frmTipo="#newNivelConceptDel";
		} else{
			frmTipo="#editNivelConceptDel";
		}
		
		
$.ajax({
			type: "GET",
			data: {q: id,sub:cit},
			url: "getLevelsBySchool.php",
			success: function (data) {
				$(frmTipo).html(data);
			}
		});

}

$().ready(function() 
	{
		$('.pasar').click(function() { return !$('#origen option:selected').remove().appendTo('#destino'); });  
		$('.quitar').click(function() { return !$('#destino option:selected').remove().appendTo('#origen'); });
		$('.pasartodos').click(function() { $('#origen option').each(function() { $(this).remove().appendTo('#destino'); }); });
		$('.quitartodos').click(function() { $('#destino option').each(function() { $(this).remove().appendTo('#origen'); }); });
		$('.submit').click(function() { $('#destino option').prop('selected', 'selected'); });
	});
	
$().ready(function() 
	{
		$('.pasarE').click(function() { return !$('#origenE option:selected').remove().appendTo('#destinoE'); });  
		$('.quitarE').click(function() { return !$('#destinoE option:selected').remove().appendTo('#origenE'); });
});


//chequear los valores del agregar conceptos
function uncheck(val){
     var checkbox1 = document.getElementById('P'+val);
     var checkbox2 = document.getElementById('E'+val); 
     var checkbox3 = document.getElementById('D'+val); 
	 
    checkbox1.onclick = function(){ 
    if(checkbox1.checked != false){ 
    checkbox2.checked =null;
    checkbox3.checked =null;
	}
     } 
	 
    checkbox2.onclick = function(){ 
    if(checkbox2.checked != false){ 
    checkbox1.checked=null;
    checkbox3.checked=null;
     }
     } 
	 
	checkbox3.onclick = function(){ 
    if(checkbox3.checked != false){ 
    checkbox1.checked=null;
    checkbox2.checked=null;
     }
     } 
   
	
	}


function uncheckCa(val){
     var checkbox1 = document.getElementById('P'+val);
     var checkbox2 = document.getElementById('E'+val); 
	 
	 
	 if ($('#P'+val).is(":checked")){
//quitamos los seleccionados
$('#P'+val).attr('checked', true);
$('#E'+val).attr('checked', false);
}else{
//seleccionamos
$('#P'+val).attr('checked', true);
$('#E'+val).attr('checked', true);
}
 
	 
	 
  /*  checkbox1.onclick = function(){ 
    if(checkbox1.checked != false){ 
    checkbox2.checked =null;
	}
     }*/ 
	 
	}

//devolver los conceptos por convenio
function getChargesByAgreementBill(id,tipo,cit,origen,forSchool){
$.ajax({
			type: "GET",
			data: {q: id,sub:cit,por:forSchool},
			url: "getChargesByAgreementBill.php",
			success: function (data) {
				if (origen==1){
				$('#origen tr:not(:first)').remove();
			    $('#origen').append(data);
				} else {
				$('#origenConcept tr:not(:first)').remove();
			    $('#origenConcept').append(data);
				}
			}
		});

}


	function getConceptValue(){
	
	 //retrieve data
  var selLanguage = document.getElementById("newMovementsConcept");
  //set up output string
  var result = "";
  result += "";
 
  //step through options
  for (i = 0; i < selLanguage.length; i++){
   //examine current option
   currentOption = selLanguage[i];
   //print it if it has been selected
   if (currentOption.selected == true){
   result += currentOption.value + ",";
   } // end if
  } // end for loop
 var conceptID = result.substring(0, result.length-1);
  //finish off the list and print it out
		var dataString = 'sAction=getConceptValue&conceptID=' + conceptID;

		$.ajax({  
		type: "post",  
		data: dataString,  
		dataType: "json",  
		url: "userBilling.php",  
		success: function(data) {
			if(data){
				if(data.queryStatus == "OK"){
					$("#newMovementsValue").val(data.value);
				}else{
					showErrorToast(data.queryStatus);
				}
			}else{
				showErrorToast("NO DATA RETURNED");
			}
		 },
		 error:function (xhr, ajaxOptions, thrownError){
			showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		 }
		});  
		return false;  
	}




//devolver los conceptos por cargo
function getConceptsByCharges(cargo,convenio,school,level,type,origen,personal,pBs){
$.ajax({
			type: "GET",
			data: {cargo:cargo,convenio:convenio,school:school,level:level,type:type,personal:personal,pBs:pBs,origen:origen},
			url: "getConceptsByCharges.php",
			success: function (data) {
				if (origen==1){
				$('#origenNewConcept tr:not(:first)').remove();
			    $('#origenNewConcept').append(data);
				} else {
				$('#origenEditConcept tr:not(:first)').remove();
			    $('#origenEditConcept').append(data);
				}
			}
		});

}


//devolver los conceptos por colegio, asi los borra si no van m�s
function getConceptsBySchool(id,scholl,levell,typeLiquidationl){
$.ajax({
			type: "GET",
			data: {id: id,school:scholl,level:levell,type:typeLiquidationl},
			url: "getConceptBySchol.php",
			success: function (data) {
				
				$('#origenConcept tr:not(:first)').remove();
			    $('#origenConcept').append(data);
				
			}
		});

}

//para agregar empleados que no figuran en la liquidacin
function newPersonalForBill(iID){
	document.getElementById("iBill").value = iID;
	getPersonalByAgreementBillNo(iID);
}

//devolver los empleados que no liquidaron
function getPersonalByAgreementBillNo(id){
$.ajax({
			type: "GET",
			data: {q: id},
			url: "getPersonalByAgreementBillNo.php",
			success: function (data) {
				if (origen==1){
				$('#origenPersonalBill tr:not(:first)').remove();
			    $('#origenPersonalBill').append(data);
				} else {
				$('#origenPersonalBill tr:not(:first)').remove();
			    $('#origenPersonalBill').append(data);
				}
			}
		});

}

//para agregar los conceptos por recibo
function newConceptsForAgreementBill(iID,type){
	document.getElementById("iBill").value = iID;
	getChargesByAgreementBill(type,1,0,1);
}

//devolver los conceptos por convenio
function getChargesByAgreement(id,tipo,cit){
		var frmTipo;
		if(tipo==1){
			frmTipo="#origen";
		} else{
			frmTipo="#origenE";
			$('#destinoE option').each(function() { $(this).remove().appendTo('#origenE'); });	
		}
$.ajax({
			type: "GET",
			data: {q: id,sub:cit},
			url: "getChargesByAgreement.php",
			success: function (data) {
				//$(frmTipo).html(data);
				$(frmTipo).html(data);
				!$('#origenE option:selected').remove().appendTo('#destinoE');
			}
		});

}

//para agregar los conceptos por recibo

function newConceptsForAgreement (iID,type){
	document.getElementById("iBill").value = iID;
	getChargesByAgreement(type,1,0);
}


//muestra conceptos permanentes
	function conceptsPermanent(ID,type){
		var dataString = 'sAction=searchConceptPermanent&IDBILL=' + ID+'&type='+type;
		$.ajax({  
		type: "post",  
		data: dataString, 	
		dataType: "json",  
		url: "movementsABM.php",  
		success: function(data) {
			
				$('#origenPermanente tr:not(:first)').remove();
				
			if(data){
				if(data.queryStatus == "OK"){
				document.getElementById("iBillP").value = ID;
				var filas = data.query.length;
			
				
		for (  i = 0 ; i < filas; i++){ //cuenta la cantidad de registros

			var nuevafila= "<tr><td>" +
			data.query[i].idConceptP + "</td><td>" + 
			data.query[i].name + "</td><td>" + 
			" <input  type='text' name='cantPerma"+data.query[i].movementIDP+"' id='cantPerma"+data.query[i].movementIDP+"' value=' " + data.query[i].cant + "' onBlur='saveToValueConceptPermanet("+data.query[i].movementIDP+",1)' onClick='showEdit(this);'></td><td>" +
			+ data.query[i].valueP +
			"</td><td> <label><input data-index='1' name='destinoPermanent[]' type='checkbox' value=" +
		    data.query[i].movementIDP + "><span></span></label></td></tr>"

			$("#origenPermanente").append(nuevafila)

		}
					/*$("#editNivel").val(data.name);
					$("#iEditID").val(data.id);*/

				}else{
						toastr.error('Algo salio mal','No hay conceptos permanentes.');
				}
			}else{
				showErrorToast("NO DATA RETURNED");
			}
		 },
		 error:function (xhr, ajaxOptions, thrownError){
			showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		 }
		});  
		return false;  
	}

	//muestra familiares
	function getSalary(ID,type){
		var dataString = 'sAction=searchFamily&IDBILL=' + ID;
		$.ajax({  
		type: "post",  
		data: dataString, 	
		dataType: "json",  
		url: "personalABM.php",  
		success: function(data) {
			
				$('#origenSalary tr:not(:first)').remove();
				
			if(data){
				if(data.queryStatus == "OK"){
				document.getElementById("iBillS").value = ID;
				var filas = data.query.length;
			
				
		for (  i = 0 ; i < filas; i++){ //cuenta la cantidad de registros

			var nuevafila= "<tr><td>" +
			data.query[i].idCharges + "</td><td>" +
			data.query[i].name + "</td><td>" +
			data.query[i].dni + "</td></tr>"

			$("#origenSalary").append(nuevafila)

		}
					/*$("#editNivel").val(data.name);
					$("#iEditID").val(data.id);*/

				}else{
					
					
						toastr.error('Algo salio mal','No hay conceptos permanentes.');
					
					
					
					
				}
			}else{
				toastr.error('Algo salio mal','No hay conceptos permanentes.');
				//showErrorToast("NO DATA RETURNED");
			}
		 },
		 error:function (xhr, ajaxOptions, thrownError){
			showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		 }
		});  
		return false;  
	}
	
	
	
	

//devolver los cargos por convenios
function getChargesByAgreementIn(id,tipo,cit){
		var frmTipo;
		if(tipo==1){
			frmTipo="#newCargo";
		} else{
			frmTipo="#editCargo";
		}
$.ajax({
			type: "GET",
			data: {q: id,sub:cit},
			url: "getChargesByAgreementIn.php",
			success: function (data) {
				//$(frmTipo).html(data);
				$(frmTipo).html(data);
				
			}
		});

}



function getServicesByAgreementIn(id,tipo,cit){
	var frmTipo;
	if(tipo==1){
		frmTipo="#newServicio-";
	} else{
		frmTipo="#editServicio-";
	}
$.ajax({
		type: "GET",
		data: {q: id,sub:cit},
		url: "getServicesByAgreementIn.php",
		success: function (data) {
			//$(frmTipo).html(data);
			$(frmTipo).html(data);
			
		}
	});

}



function getServicesByAgreementInAll(id,tipo,cit){
	var frmTipo;
	if(tipo==1){
		frmTipo="#newServicio-";
	} else{
		frmTipo="#editServicio-"; 
	}
	var result = 0;
$.ajax({
		type: "GET",
		data: {q: id,sub:cit},
		async: false,
		url: "getServicesByAgreementIn.php",
		success: function (data) {
			//$(frmTipo).html(data);
			result = 1;
			$(frmTipo).html(data);
	
			
		}
	});

	return result;

}




//devolver los conceptos por convenios
function getConceptsByAgreement(id,tipo,cit){
		var frmTipo;
		if(tipo==1){
			frmTipo="#newCon";
		} else{
			frmTipo="#editCon";
		}
$.ajax({
			type: "GET",
			data: {q: id,sub:tipo},
			url: "getConceptsByAgreements.php",
			success: function (data) {
				//$(frmTipo).html(data);
				$(frmTipo).html(data);
				
			}
		});

}

//devolver los tipos de liquidaciones de un concepto
function getTypesLiquidationByConcept(id){
		var frmTipo;
		frmTipo="#editConceptDataTypeLiquidation";
$.ajax({
			type: "GET",
			data: {q: id},
			url: "getTypeLiquidationByConcept.php",
			success: function (data) {
				//$(frmTipo).html(data);
				$(frmTipo).html(data);
				
			}
		});

}


//elinaci�n de las diferentes entidades
 function eliminar(ID, action){
	 var r='';
	 var direccion='';
	 switch (action) {
    case 'deleteProvider':
        r="Desea Eliminar este Proveedor!";
		direccion='providerABM.php';
        break;
		  case 'deleteProductsMovementsIn':
        r="Desea Eliminar este Movimiento!";
		direccion='inABM.php';
        break;
		 case 'deleteProductsByOut':
        r="Desea Eliminar esta carga!";
		direccion='outABM.php';
        break;
			 case 'deleteProductsByOutUser':
        r="Desea Eliminar esta carga!";
		direccion='outABMUser.php';
        break;
	case 'deleteProviderLevel':
        r="Desea Eliminar esta categoría!";
		direccion='levelsABM.php';
        break;
		case 'deleteProductsCategory':
        r="Desea Eliminar esta categoría!";
		direccion='categoryABM.php';
		break;
		case 'deleteProducts':
        r="Desea Eliminar este producto!";
		direccion='productsABM.php';
		break;
		case 'deleteWorks':
        r="Desea Eliminar esta obra!";
		direccion='works.php';
        break;
    case 'deleteSchoolCharges':
         r="Desea Eliminar este Cargo!";
		 direccion='chargesABM.php';
        break;
		 case 'deleteMappeoConcept':
         r="Desea Eliminar este Mappeo Seguro!";
		 direccion='conceptAfipABM.php';
        break;
    case 'deleteUserAdmin':
         r="Desea Eliminar este Usuario!";
		 direccion='sysUserABM.php';
        break;
		case 'deleteUser':
			r="Desea Eliminar este Usuario!";
			direccion='usersABM.php';
		   break;
	case 'deleteSocialWork':
         r="Desea Eliminar esta Obra Social!";
		 direccion='socialWork.php';
		break;
		case 'deleteServices':
			r="Desea Eliminar este Servicio!";
			direccion='services.php';
		   break;
		case 'deleteSchoolActivitie':
         r="Desea Eliminar esta Actividad!";
		 direccion='activitiesABM.php';
        break;
		case 'deletePersonal':
         r="Desea Eliminar esta Persona!";
		 direccion='personalABM.php';
        break;
		case 'deletePersonalByBill':
         r="Desea Eliminar esta Persona Monotributo!";
		 direccion='personalByBill.php';
        break;
		case 'deleteAgreement':
         r="Desea Eliminar este Convenio!";
		 direccion='agreementABM.php';
        break;
		case 'deletePersonalBySchool':
         r="Desea Eliminar esta Asignaci&oacute;n!";
		 direccion='personalBySchool.php';
        break;
		case 'deleteServiceByUser':
         r="Desea Eliminar esta Unidad de Servicio!";
		 direccion='usersByServices.php';
        break;
		case 'deleteChargesByAgreement':
         r="Desea Eliminar este Cargo por Convenio!";
		 direccion='chargesByAgreement.php';
        break;
		case 'deleteConcept':
         r="Desea Eliminar este Concepto!";
		 direccion='conceptABM.php';
        break;
		case 'deleteCategory':
         r="Desea Eliminar esta Categoria!";
		 direccion='categoryABM.php';
        break;
		case 'deleteCategoryConcept':
         r="Desea Eliminar esta Categoria!";
		 direccion='category_concept_ABM.php';
        break;
		case 'deleteLiquidationType':
         r="Desea Eliminar este Tipo de Liquidaci&oacute;n!";
		 direccion='liquidationTypeABM.php';
        break;
		case 'deleteConceptAfip':
         r="Desea Eliminar este Concepto Afip!";
		 direccion='conceptAfipSueldoABM.php';
        break;
    
}

	toastr.options = {
  "closeButton": true,
  "debug": true,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-top-center",
  "preventDuplicates": true,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}	


		toastr.info("<br><button type='button' id='confirmationRevertYes' class='btn green'>Si</button><button type='button' id='confirmationRevertNo' class='btn default'>No</button>",r,
  {
      closeButton: false,
      allowHtml: true,
      onShown: function (toast) {
		  
          $("#confirmationRevertYes").click(function(){
			  
			      $.ajax({
			   type: "POST",
			   url: direccion,
			   data:{'sAction':action, 'iID':ID},
			   dataType:"json",
			   success: function(data)
			   {
				   
					if(data.status == "OK"){
						toastr.success('Ha eliminado el elemento seleccionado.', 'Dato Eliminado correctamente.');
						$("#formSearch").submit();
						if (action=='deleteUser') {window.location = direccion;}
					}else{
						toastr.error('Algo salio mal','No se pudo eliminar el elemento');
					}
			   },
			   error:function (xhr, ajaxOptions, thrownError){
					toastr.error('Algo salio mal','No se pudo eliminar el elemento');
					//$.notific8(data.status);
						//showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
				}
			 });
	
          });
		  
		   $("#confirmationRevertNo").click(function(){
						toastr.info('Cancelado por Usuario.', 'No se pudo eliminar el elemento.');
						//toastr.success('Datos No guardados', 'Editar Cargo');
						 //$('#full-widthEdit').modal('hide');
						
				});

        }
  });

/*
if (r == true) {
    $.ajax({
			   type: "POST",
			   url: direccion,
			   data:{'sAction':action, 'iID':ID},
			   dataType:"json",
			   success: function(data)
			   {
				   
					if(data){

						window.location = direccion;
						

					}else{
						alert("Algo Salio Mal!");
					}
			   }
			 });
} else {
    alert("Accion Cancelada!");
}
		
		
		return false; // avoid to execute the actual submit of the form.*/
		
	 }
	 

var e=0;
	
	function agregar_EditDevoluciones(){

$( "#descripcionEditDevo" ).clone().attr('id', $( "#descripcionEditDevo" ).attr("id")+(++e)).appendTo( "#editCargosDevo" );
$( "#descripcionEditDevo"+e ).css("display","");

$("#descripcionEditDevo"+e+" select" ).each(function(index){
	$(this).attr("name",$(this).attr("name")+e);
	
});
$("#descripcionEditDevo"+e+" input" ).each(function(index){
	$(this).attr("name",$(this).attr("name")+e);
	//alert($(this).attr("name"));
});


$("INPUT[name='cantEditCargoDevo]").each
(
    function(index, element)
    {
    $(this).val(e);
    }
);
document.getElementById("cantEditCargoDevo").value = e;

}
	 
	 
var s=0;
	
	function agregar_EditCargos(){

	$( "#descripcionEdit" ).clone().attr('id', $( "#descripcionEdit" ).attr("id")+(++s)).appendTo( "#editCargos" );
	$( "#descripcionEdit"+s ).css("display","");

	$("#descripcionEdit"+s+" select" ).each(function(index){
		$(this).attr("name",$(this).attr("name")+s);
		
	});
	$("#descripcionEdit"+s+" input" ).each(function(index){
		$(this).attr("name",$(this).attr("name")+s);
		//alert($(this).attr("name"));
	});

	$("#descripcionEdit"+s+" div" ).each(function(index){
		$(this).attr("id",$(this).attr("id")+s);
		//alert($(this).attr("name"));
	});


	$("INPUT[name='cantEditCargo]").each
	(
	  function(index, element)
	  {
	  $(this).val(s);
	  }
	);
	document.getElementById("cantEditCargo").value = s;

}	

function agregar_NewGeneracion(){

	$( "#descripcionEdit" ).clone().attr('id', $( "#descripcionEdit" ).attr("id")+(++s)).appendTo( "#editCargos" );
	$( "#descripcionEdit"+s ).css("display","");

	$("#descripcionEdit"+s+" select" ).each(function(index){
		$(this).attr("name",$(this).attr("name")+s);
		
	});
	$("#descripcionEdit"+s+" input" ).each(function(index){
		$(this).attr("name",$(this).attr("name")+s);
		//alert($(this).attr("name"));
	});

	$("#descripcionEdit"+s+" div" ).each(function(index){
		$(this).attr("id",$(this).attr("id")+s);
		//alert($(this).attr("name"));
	});


	$("INPUT[name='cantEditCargo]").each
	(
	    function(index, element)
	    {
	    $(this).val(s);
	    }
	);
	document.getElementById("cantEditCargo").value = s;

}	
 
	var z=0;
	
function agregar_EditEmpleadores(){

	$( "#descripcionEditEmpleador" ).clone().attr('id', $( "#descripcionEditEmpleador" ).attr("id")+(++z)).appendTo( "#editCargos" );
	$( "#descripcionEditEmpleador"+z ).css("display","");


	$("#descripcionEditEmpleador"+z+" input" ).each(function(index){
		$(this).attr("name",$(this).attr("name")+z);
		//alert($(this).attr("name"));
	});


	$("INPUT[name='cantEditEmpleadores]").each
	(
	    function(index, element)
	    {
	    $(this).val(z);
	    }
	);
	document.getElementById("cantEditEmpleadores").value = z;

}	  
	 
//mustra informaci�n de un proveedor	 
function showProviderData(ID){
	var dataString = 'sAction=searchProviderById&providerID=' + ID;
	$.ajax({  
	type: "post",  
	data: dataString, 	
	dataType: "json",  
	url: "providerABM.php",  
	success: function(data) {
		
		if(data){
			if(data.queryStatus == "OK"){

		    //elimino todo de la tabla de cargos				
				var tableHeaderRowCount = 1;
				var table = document.getElementById('editCargos');
				var rowCount = table.rows.length;
				for (var i = tableHeaderRowCount; i < rowCount; i++) {
				    table.deleteRow(tableHeaderRowCount);
				}
				s=0;
				
				$("#editProveedor").val(data.name);
				$("#editRazon").val(data.businessName);
				$("#iEditID").val(data.id);
				$("#editCuit").val(data.cuit);
				$("#editDomicilio").val(data.address);
				$("#editTelefono").val(data.phone);
				
				 document.getElementById('editImage').src = 'img/providers/' + data.logo;
				//$("#editImg").val(data.logo);
				$("#editImgTest").val(data.logo);
				$("#editLocalidad").val(data.location);
				//$("#editCp").val(data.cp);
				$("#editFax").val(data.fax);
				
				$("#editTelefonoAdicional").val(data.phoneAlter);
				$("#editHorario").val(data.schedule);
				$("#editEmail").val(data.email);
				$("textarea#editObser").val(data.memo);
				$("#editCliente").val(data.number);
				$("#editCpe").val(data.cpe);

				$("#editCategoria option").each(function(){
					if($(this).val() == data.category){
						$(this).attr("selected","'selected'");
						$("#editCategoria").val(data.category);
					}						
				});


				$("#editProvincia option").each(function(){
					if($(this).val() == data.province){
						$(this).attr("selected","'selected'");
						$("#editProvincia").val(data.province);
					}						
				});
				
				selectCityById(data.province,"editCiudad",data.city);
				//getCiudades(data.province,2,data.city);
				$("#editCp").val(data.cp);
					
//si tiene cargos asignados vemos
				if (data.charges.length>0){

					for (g = 0; g < data.charges.length; g++) {
						
						$( "#descripcionEdit" ).clone().attr('id', $( "#descripcionEdit" ).attr("id")+(++s)).appendTo( "#editCargos" );
						$( "#descripcionEdit"+s ).css("display","");

						$("#descripcionEdit"+s+" select" ).each(function(index){
							$(this).attr("name",$(this).attr("name")+s);
							$(this).val(data.charges[g]['idCharge']);
							$(this).attr("selected","'selected'");
							if($(this).val() == data.charges[g]['idCharge']){
													$(this).attr("selected","'selected'");
												}	
							
						});
						$("#descripcionEdit"+s+" input" ).each(function(index){
							//alert(this);
							$(this).attr("name",$(this).attr("name")+s);
							$('input[name=editCargoName-]'+s).val(data.charges[g]['name']);
							$("#editCargoName-"+s).val(data.charges[g]['name']);
							$("#editCargoTelefono-".s).val(data.charges[g]['phone']);
							$("#editCargoCel-"+s).val(data.charges[g]['cellPhone']);
							$("#editCargoEmail-"+s).val(data.charges[g]['email']);
							//alert($(this).attr("name"));
						});

						$("INPUT[name='editCargoName-"+s+"']").each
						(
						    function(index, element)
						    {
						    $(this).val(data.charges[g]['name']);
						    }
						);

						$("INPUT[name='editCargoTelefono-"+s+"']").each
						(
						    function(index, element)
						    {
						    $(this).val(data.charges[g]['phone']);
						    }
						);

						$("INPUT[name='editCargoCel-"+s+"']").each
						(
						    function(index, element)
						    {
						    $(this).val(data.charges[g]['cellPhone']);
						    }
						);

						$("INPUT[name='editCargoEmail-"+s+"']").each
						(
						    function(index, element)
						    {
						    $(this).val(data.charges[g]['email']);
						    }
						);

						$("INPUT[name='cantEditCargo]").each
						(
						    function(index, element)
						    {
						    $(this).val(s);
						    }
						);
						document.getElementById("cantEditCargo").value = s;

					}
									
				} 
				
			}else{
				showErrorToast(data.queryStatus);
			}
		}else{
			showErrorToast("NO DATA RETURNED");
		}
	 },
	 error:function (xhr, ajaxOptions, thrownError){
		showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
	 }
	});  
	return false;  
}
	

//mustra informaci�n de un colegio y su cobro mensual	 
	function showSchoolDataBilling(ID){
		var dataString = 'sAction=searchSchoolByIdBilling&billingID=' + ID;
		$.ajax({  
		type: "post",  
		data: dataString, 	
		dataType: "json",  
		url: "facturacionABM.php",  
		success: function(data) {
			
			if(data){
				if(data.queryStatus == "OK"){
					$("#editEmpleador").val(data.name);
					$("#editCuit").val(data.cuit);
					$("#iEditID").val(data.id);
					$("#editRazon").val(data.businessName);
					$("#editCliente").val(data.number);
					$("#editTotalValue").val(data.totalValue);
				}else{
					showErrorToast(data.queryStatus);
				}
			}else{
				showErrorToast("NO DATA RETURNED");
			}
		 },
		 error:function (xhr, ajaxOptions, thrownError){
			showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		 }
		});  
		return false;  
	}
	
	
	
	
	 	function showMovementsIn(ID){
		var dataString = 'sAction=searchMovementsByIdBilling&billingID=' + ID;
		$.ajax({  
		type: "post",  
		data: dataString, 	
		dataType: "json",  
		url: "inABM.php",  
		success: function(data) {
			
			if(data){
				if(data.queryStatus == "OK"){
					$("#iEditID").val(data.IdMo);
					$("#iEditBill").val(data.idBill);
					$("#iProduct").val(data.products);
					
					$("#editProvider option").each(function(){
						if($(this).val() == data.provider){
							$(this).attr("selected","'selected'");
							$("#editProvider").val(data.provider);
						}						
					});
					
					$("#editCantidad").val(data.quantity);
					$("#ediCantidadCopia").val(data.quantity);
					$("#editPrecio").val(data.uniValue);
					$("#editPrecioCopia").val(data.uniValue);
					$("#editTotal").val(data.totalValue);
					$("#editTotalCopia").val(data.totalValue);
					$("#editIva").val(data.iva);
					$("#editReferencia").val(data.ref);
					$("#editNumero").val(data.billNumber);
					$("#editPVenta").val(data.point);
					$("#editFecha").val(data.dateBegin);
					$("#editMaterial").val(data.description);
				}else{
					showErrorToast(data.queryStatus);
				}
			}else{
				showErrorToast("NO DATA RETURNED");
			}
		 },
		 error:function (xhr, ajaxOptions, thrownError){
			showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		 }
		});  
		return false;  
	}
	
	
	
	function showMovementsIn_history(ID){
		var dataString = 'sAction=searchMovementsByIdBilling&billingID=' + ID;
		$.ajax({  
		type: "post",  
		data: dataString, 	
		dataType: "json",  
		url: "inABM_history.php",  
		success: function(data) {
			
			if(data){
				if(data.queryStatus == "OK"){
					$("#iEditID").val(data.IdMo);
					$("#iEditBill").val(data.idBill);
					$("#iProduct").val(data.products);
					
					$("#editProvider option").each(function(){
						if($(this).val() == data.provider){
							$(this).attr("selected","'selected'");
							$("#editProvider").val(data.provider);
						}						
					});
					
					$("#editCantidad").val(data.quantity);
					$("#ediCantidadCopia").val(data.quantity);
					$("#editPrecio").val(data.uniValue);
					$("#editPrecioCopia").val(data.uniValue);
					$("#editTotal").val(data.totalValue);
					$("#editTotalCopia").val(data.totalValue);
					$("#editIva").val(data.iva);
					$("#editReferencia").val(data.ref);
					$("#editNumero").val(data.billNumber);
					$("#editPVenta").val(data.point);
					$("#editFecha").val(data.dateBegin);
					$("#editMaterial").val(data.description);
				}else{
					showErrorToast(data.queryStatus);
				}
			}else{
				showErrorToast("NO DATA RETURNED");
			}
		 },
		 error:function (xhr, ajaxOptions, thrownError){
			showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		 }
		});  
		return false;  
	}
	
	
	
	
	
	
	 	function showMovementsInForNote(ID){
		var dataString = 'sAction=searchMovementsByIdBilling&billingID=' + ID;
		$.ajax({  
		type: "post",  
		data: dataString, 	
		dataType: "json",  
		url: "inABM.php",  
		success: function(data) {
			
			if(data){
				if(data.queryStatus == "OK"){
					$("#iEditIDFC").val(data.IdMo);
					$("#iEditBillFC").val(data.idBill);
					$("#iProductFC").val(data.products);
					
					$("#editProviderFC option").each(function(){
						if($(this).val() == data.provider){
							$(this).attr("selected","'selected'");
							$("#editProviderFC").val(data.provider);
						}						
					});
					
					$("#editCantidadFC").val(data.quantity);
					$("#ediCantidadCopiaFC").val(data.quantity);
					$("#editPrecioFC").val(data.uniValue);
					$("#editPrecioCopiaFC").val(data.uniValue);
					$("#editTotalFC").val(data.totalValue);
					$("#editTotalCopiaFC").val(data.totalValue);
					$("#editIvaFC").val(data.iva);
					$("#editReferenciaFC").val(data.ref);
					$("#editNumeroFC").val(data.billNumber);
					$("#editPVentaFC").val(data.point);
					$("#editFechaFC").val(data.dateBegin);
					$("#editMaterialFC").val(data.description);
				}else{
					showErrorToast(data.queryStatus);
				}
			}else{
				showErrorToast("NO DATA RETURNED");
			}
		 },
		 error:function (xhr, ajaxOptions, thrownError){
			showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		 }
		});  
		return false;  
	}
	

	function showMovementsOut(ID){
		var dataString = 'sAction=searchMovOutById&productsID=' + ID;
		$.ajax({  
		type: "post",  
		data: dataString, 	
		dataType: "json",  
		url: "outABM.php",  
		success: function(data) {
			
			if(data){
				if(data.queryStatus == "OK"){
					$("#iEditID").val(data.id);
					$("#editStock").val(data.quantity);
					
					
					$("#editPrecio").val(data.uniValue);
					
					$("#editPrecioTotal").val(data.totalValue);
					
					$("#editReferencia").val(data.ref);
					$("#editSerial").val(data.serial);
					
					$("#editFecha").val(data.dateBegin);
					
				}else{
					showErrorToast(data.queryStatus);
				}
			}else{
				showErrorToast("NO DATA RETURNED");
			}
		 },
		 error:function (xhr, ajaxOptions, thrownError){
			showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		 }
		});  
		return false;  
	}
	
		//mustra informaci�n de un nivel de un colegio
		function showProductsCategoryData(ID){
			var dataString = 'sAction=searchProductsCategoryById&nivelID=' + ID;
			$.ajax({  
			type: "post",  
			data: dataString, 	
			dataType: "json",  
			url: "categoryABM.php",  
			success: function(data) {
				
				if(data){
					if(data.queryStatus == "OK"){
	
						$("#editNivel").val(data.name);
						$("#iEditID").val(data.id);
	
					}else{
						showErrorToast(data.queryStatus);
					}
				}else{
					showErrorToast("NO DATA RETURNED");
				}
			 },
			 error:function (xhr, ajaxOptions, thrownError){
				showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
			 }
			});  
			return false;  
		}	
		
		
			function showProductsCategoryDataUser(ID){
			var dataString = 'sAction=searchProductsCategoryById&nivelID=' + ID;
			$.ajax({  
			type: "post",  
			data: dataString, 	
			dataType: "json",  
			url: "categoryABMUser.php",  
			success: function(data) {
				
				if(data){
					if(data.queryStatus == "OK"){
	
						$("#editNivel").val(data.name);
						$("#iEditID").val(data.id);
	
					}else{
						showErrorToast(data.queryStatus);
					}
				}else{
					showErrorToast("NO DATA RETURNED");
				}
			 },
			 error:function (xhr, ajaxOptions, thrownError){
				showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
			 }
			});  
			return false;  
		}	

		function showServicesData(ID){
			var dataString = 'sAction=searchServicesById&nivelID=' + ID;
			$.ajax({  
			type: "post",  
			data: dataString, 	
			dataType: "json",  
			url: "services.php",  
			success: function(data) {
				
				if(data){
					if(data.queryStatus == "OK"){
	
						$("#editNivel").val(data.name);
						$("#iEditID").val(data.id);
	
					}else{
						showErrorToast(data.queryStatus);
					}
				}else{
					showErrorToast("NO DATA RETURNED");
				}
			 },
			 error:function (xhr, ajaxOptions, thrownError){
				showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
			 }
			});  
			return false;  
		}	
	

		function showWorksData(ID){
			var dataString = 'sAction=searchWorksById&nivelID=' + ID;
			$.ajax({  
			type: "post",  
			data: dataString, 	
			dataType: "json",  
			url: "works.php",  
			success: function(data) {
				
				if(data){
					if(data.queryStatus == "OK"){
	
						$("#editNivel").val(data.name);
						$("#iEditID").val(data.id);
						$("#editService option").each(function(){
							if($(this).val() == data.idService){
								$(this).attr("selected","'selected'");
								$("#editService").val(data.idService);
							}						
						});
	
					}else{
						showErrorToast(data.queryStatus);
					}
				}else{
					showErrorToast("NO DATA RETURNED");
				}
			 },
			 error:function (xhr, ajaxOptions, thrownError){
				showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
			 }
			});  
			return false;  
		}	
		
		function showWorksDataUser(ID){
			var dataString = 'sAction=searchWorksById&nivelID=' + ID;
			$.ajax({  
			type: "post",  
			data: dataString, 	
			dataType: "json",  
			url: "worksUser.php",  
			success: function(data) {
				
				if(data){
					if(data.queryStatus == "OK"){
	
						$("#editNivel").val(data.name);
						$("#iEditID").val(data.id);
						$("#editService option").each(function(){
							if($(this).val() == data.idService){
								$(this).attr("selected","'selected'");
								$("#editService").val(data.idService);
							}						
						});
	
					}else{
						showErrorToast(data.queryStatus);
					}
				}else{
					showErrorToast("NO DATA RETURNED");
				}
			 },
			 error:function (xhr, ajaxOptions, thrownError){
				showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
			 }
			});  
			return false;  
		}
	
	//mustra informaci�n de un nivel de un colegio
	function showProviderLevelData(ID){
		var dataString = 'sAction=searchProviderLevelsById&nivelID=' + ID;
		$.ajax({  
		type: "post",  
		data: dataString, 	
		dataType: "json",  
		url: "levelsABM.php",  
		success: function(data) {
			
			if(data){
				if(data.queryStatus == "OK"){

					$("#editNivel").val(data.name);
					$("#iEditID").val(data.id);

				}else{
					showErrorToast(data.queryStatus);
				}
			}else{
				showErrorToast("NO DATA RETURNED");
			}
		 },
		 error:function (xhr, ajaxOptions, thrownError){
			showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		 }
		});  
		return false;  
	}	
	
	
		function showProviderLevelDataUser(ID){
		var dataString = 'sAction=searchProviderLevelsById&nivelID=' + ID;
		$.ajax({  
		type: "post",  
		data: dataString, 	
		dataType: "json",  
		url: "levelsABMUser.php",  
		success: function(data) {
			
			if(data){
				if(data.queryStatus == "OK"){

					$("#editNivel").val(data.name);
					$("#iEditID").val(data.id);

				}else{
					showErrorToast(data.queryStatus);
				}
			}else{
				showErrorToast("NO DATA RETURNED");
			}
		 },
		 error:function (xhr, ajaxOptions, thrownError){
			showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		 }
		});  
		return false;  
	}	
	
	
	
	
		//mustra informaci�n de un tipo de liquidacion
	function showLiquidationTypeData(ID){
		var dataString = 'sAction=searchLiquidationTypeById&nivelID=' + ID;
		$.ajax({  
		type: "post",  
		data: dataString, 	
		dataType: "json",  
		url: "liquidationTypeABM.php",  
		success: function(data) {
			
			if(data){
				if(data.queryStatus == "OK"){

					$("#editNivel").val(data.name);
					$("#iEditID").val(data.id);

				}else{
					showErrorToast(data.queryStatus);
				}
			}else{
				showErrorToast("NO DATA RETURNED");
			}
		 },
		 error:function (xhr, ajaxOptions, thrownError){
			showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		 }
		});  
		return false;  
	}	
	
		//mustra informaci�n de un sindicato
	function showSchoolLaborData(ID){
		var dataString = 'sAction=searchSchoolLaborById&laborID=' + ID;
		$.ajax({  
		type: "post",  
		data: dataString, 	
		dataType: "json",  
		url: "laborABM.php",  
		success: function(data) {
			
			if(data){
				if(data.queryStatus == "OK"){

					$("#editSindicato").val(data.name);
					$("#iEditID").val(data.id);

				}else{
					showErrorToast(data.queryStatus);
				}
			}else{
				showErrorToast("NO DATA RETURNED");
			}
		 },
		 error:function (xhr, ajaxOptions, thrownError){
			showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		 }
		});  
		return false;  
	}	
	
	
	//mustra informaci�n de un cargo de un colegio
	function showSchoolChargesData(ID){
		var dataString = 'sAction=searchSchoolChargesById&cargoID=' + ID;
		$.ajax({  
		type: "post",  
		data: dataString, 	
		dataType: "json",  
		url: "chargesABM.php",  
		success: function(data) {
			
			if(data){
				if(data.queryStatus == "OK"){

					$("#editCargo").val(data.name);
					$("#iEditID").val(data.id);

				}else{
					showErrorToast(data.queryStatus);
				}
			}else{
				showErrorToast("NO DATA RETURNED");
			}
		 },
		 error:function (xhr, ajaxOptions, thrownError){
			showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		 }
		});  
		return false;  
	}
	
	//solamente para ver las observaciones formulario school
	function viewObservacionesData(valor,campo){
		if(valor == true){
			$(campo).removeAttr("style");
		}else{
			$(campo).attr("style","display:none");
		}	
	}

//agregar cargos por colegio	
var e=0;
	
	function agregar_NewCargos(){

		$('.selectNew').select2();
		$('.selectNew').select2("destroy");

$( "#descripcion" ).clone().attr('id', $( "#descripcion" ).attr("id")+(++e)).appendTo( "#newCargos" );
$( "#descripcion"+e ).css("display","");

$("#descripcion"+e+" select" ).each(function(index){
	$(this).attr("name",$(this).attr("name")+e);
	
});
$("#descripcion"+e+" input" ).each(function(index){
	$(this).attr("name",$(this).attr("name")+e);
	//alert($(this).attr("name"));
});


$("#descripcion"+e+" div" ).each(function(index){
	$(this).attr("id",$(this).attr("id")+e);
	//alert($(this).attr("name"));
});

$("INPUT[name='cantNewCargo]").each
(
    function(index, element)
    {
    $(this).val(e);
    }
);
document.getElementById("cantNewCargo").value = e;

$('.selectNew').select2(); 

}

// eliminar los cargos de una escuela o novedades
	 function eliminarCargo(o){

		var p=o.parentNode.parentNode;
         p.parentNode.removeChild(p);
	
	 }
	 
	 

	 function mostrarDiv(selectObject,o){

		var p=o.parentNode.parentNode.id;
		const myArray = p.split("-");


if (selectObject==1) {
	$( "#actaNacimiento-"+myArray[1] ).css("display","");
	$( "#actaMatrimonio-"+myArray[1] ).css("display","none");
	$( "#actaDefuncion-"+myArray[1] ).css("display","none");
} else {
	if (selectObject==2) {
	$( "#actaNacimiento-"+myArray[1] ).css("display","none");
	$( "#actaMatrimonio-"+myArray[1] ).css("display","");
	$( "#actaDefuncion-"+myArray[1] ).css("display","none");
} else {
	if (selectObject==3) {
	$( "#actaNacimiento-"+myArray[1] ).css("display","none");
	$( "#actaMatrimonio-"+myArray[1] ).css("display","none");
	$( "#actaDefuncion-"+myArray[1] ).css("display","");
} else {
	$( "#actaNacimiento-"+myArray[1] ).css("display","none");
	$( "#actaMatrimonio-"+myArray[1] ).css("display","none");
	$( "#actaDefuncion-"+myArray[1] ).css("display","none");
}
}
}

	/*	switch(selectObject) {
  case 1:
    //es acata nacimiento
	console.log('puto');
	console.log(myArray[1]);
	$( "#actaNacimiento-"+myArray[1] ).css("display","");
	document.getElementById("actaNacimiento-"+myArray[1]).style.display = "none";
	$( "#actaMatrimonio-"+myArray[1] ).css("display","none");
	$( "#actaDefuncion-"+myArray[1] ).css("display","none");
    break;
  case 2:
     //es acata matrimonio
	$( "#actaNacimiento-"+myArray[1] ).css("display","none");
	$( "#actaMatrimonio-"+myArray[1] ).css("display","");
	$( "#actaDefuncion-"+myArray[1] ).css("display","none");
    break;
	 case 3:
      //es acata defuncion
	$( "#actaNacimiento-"+myArray[1] ).css("display","none");
	$( "#actaMatrimonio-"+myArray[1] ).css("display","none");
	$( "#actaDefuncion-"+myArray[1] ).css("display","");
    break;
	$( "#actaNacimiento-"+myArray[1] ).css("display","none");
	$( "#actaMatrimonio-"+myArray[1] ).css("display","none");
	$( "#actaDefuncion-"+myArray[1] ).css("display","none");
  default:
    // code block
}*/

	
	 }	 
	 
	 
	 
	 	 function mostrarDivEdit(selectObject,o){

		var p=o.parentNode.id;
		const myArray = p.split("-");

if (selectObject==1) {
	$( "#actaNacimientoEdit-"+myArray[1] ).css("display","");
	$( "#actaMatrimonioEdit-"+myArray[1] ).css("display","none");
	$( "#actaDefuncionEdit-"+myArray[1] ).css("display","none");
} else {
	if (selectObject==2) {
	$( "#actaNacimientoEdit-"+myArray[1] ).css("display","none");
	$( "#actaMatrimonioEdit-"+myArray[1] ).css("display","");
	$( "#actaDefuncionEdit-"+myArray[1] ).css("display","none");
} else {
	if (selectObject==3) {
	$( "#actaNacimientoEdit-"+myArray[1] ).css("display","none");
	$( "#actaMatrimonioEdit-"+myArray[1] ).css("display","none");
	$( "#actaDefuncionEdit-"+myArray[1] ).css("display","");
} else {
	$( "#actaNacimientoEdit-"+myArray[1] ).css("display","none");
	$( "#actaMatrimonioEdit-"+myArray[1] ).css("display","none");
	$( "#actaDefuncionEdit-"+myArray[1] ).css("display","none");
}
}
}

	/*	switch(selectObject) {
  case 1:
    //es acata nacimiento
	console.log('puto');
	console.log(myArray[1]);
	$( "#actaNacimiento-"+myArray[1] ).css("display","");
	document.getElementById("actaNacimiento-"+myArray[1]).style.display = "none";
	$( "#actaMatrimonio-"+myArray[1] ).css("display","none");
	$( "#actaDefuncion-"+myArray[1] ).css("display","none");
    break;
  case 2:
     //es acata matrimonio
	$( "#actaNacimiento-"+myArray[1] ).css("display","none");
	$( "#actaMatrimonio-"+myArray[1] ).css("display","");
	$( "#actaDefuncion-"+myArray[1] ).css("display","none");
    break;
	 case 3:
      //es acata defuncion
	$( "#actaNacimiento-"+myArray[1] ).css("display","none");
	$( "#actaMatrimonio-"+myArray[1] ).css("display","none");
	$( "#actaDefuncion-"+myArray[1] ).css("display","");
    break;
	$( "#actaNacimiento-"+myArray[1] ).css("display","none");
	$( "#actaMatrimonio-"+myArray[1] ).css("display","none");
	$( "#actaDefuncion-"+myArray[1] ).css("display","none");
  default:
    // code block
}*/

	
	 }	
	 
	 
	 
	 

//agregar empleadores por persona	
var f=0;
	
	function agregar_NewEmpleadores(){

$( "#descripcionEmpleador" ).clone().attr('id', $( "#descripcionEmpleador" ).attr("id")+(++f)).appendTo( "#newEmpleadores" );
$( "#descripcionEmpleador"+f ).css("display","");
$("#descripcionEmpleador"+f+" input" ).each(function(index){
	$(this).attr("name",$(this).attr("name")+f);
	//alert($(this).attr("name"));
});

$("INPUT[name='cantNewEmpleadores]").each
(
    function(index, element)
    {
    $(this).val(f);
    }
);
document.getElementById("cantNewEmpleadores").value = f;
}	 

// eliminar los empleadores de una persona
	 function eliminarEmpleador(o){

		var p=o.parentNode.parentNode;
         p.parentNode.removeChild(p);
	
	 }

//actividades

//mustra informaci�n de un colegio	 
	 	function showSchoolActivitieData(ID){
		var dataString = 'sAction=searchActivitieById&actitivieID=' + ID;
		$.ajax({  
		type: "post",  
		data: dataString, 	
		dataType: "json",  
		url: "activitiesABM.php",  
		success: function(data) {
			
			if(data){
				if(data.queryStatus == "OK"){

//elimino todo de la tabla de cargos				
var tableHeaderRowCount = 1;
var table = document.getElementById('editCargos');
var rowCount = table.rows.length;
for (var i = tableHeaderRowCount; i < rowCount; i++) {
    table.deleteRow(tableHeaderRowCount);
}
s=0;
				

					$("#editDipregep").val(data.ips);
					$("#iEditID").val(data.id);
					$("#editSub").val(data.sub);
					$("#editEx").val(data.case);
					$("#editFecha").val(data.dateCase);
					$("#editCue").val(data.cue);
					$("#editCuit").val(data.cuit);
					$("textarea#editObser").val(data.memo);
					
					 document.getElementById('editImage').src = 'img/schools/signature/' + data.signature;
					//$("#editImg").val(data.logo);
					$("#editImgTest").val(data.signature);
	
					$("#editEmpleador option").each(function(){
						if($(this).val() == data.idSchool){
							$(this).attr("selected","'selected'");
							$("#editEmpleador").val(data.idSchool).trigger("change");
						}						
					});
					
					
					
					$("#editNivel option").each(function(){
						if($(this).val() == data.idLevel){
							$(this).attr("selected","'selected'");
							$("#editNivel").val(data.idLevel);
						}						
					});
					
					$("#editType option").each(function(){
						if($(this).val() == data.type){
							$(this).attr("selected","'selected'");
							$("#editType").val(data.type);
						}						
					});
				
					
//si tiene cargos asignados vemos
if (data.charges.length>0){


for (g = 0; g < data.charges.length; g++) {
	

	
$( "#descripcionEdit" ).clone().attr('id', $( "#descripcionEdit" ).attr("id")+(++s)).appendTo( "#editCargos" );
$( "#descripcionEdit"+s ).css("display","");

$("#descripcionEdit"+s+" select" ).each(function(index){
	$(this).attr("name",$(this).attr("name")+s);
	$(this).val(data.charges[g]['idCharge']);
	$(this).attr("selected","'selected'");
	if($(this).val() == data.charges[g]['idCharge']){
							$(this).attr("selected","'selected'");
						}	
	
});
$("#descripcionEdit"+s+" input" ).each(function(index){
	//alert(this);
	$(this).attr("name",$(this).attr("name")+s);
	$('input[name=editCargoName-]'+s).val(data.charges[g]['name']);
	$("#editCargoName-"+s).val(data.charges[g]['name']);
	$("#editCargoTelefono-".s).val(data.charges[g]['phone']);
	$("#editCargoCel-"+s).val(data.charges[g]['cellPhone']);
	$("#editCargoEmail-"+s).val(data.charges[g]['email']);
	//alert($(this).attr("name"));
});

$("INPUT[name='editCargoName-"+s+"']").each
(
    function(index, element)
    {
    $(this).val(data.charges[g]['name']);
    }
);

$("INPUT[name='editCargoTelefono-"+s+"']").each
(
    function(index, element)
    {
    $(this).val(data.charges[g]['phone']);
    }
);

$("INPUT[name='editCargoCel-"+s+"']").each
(
    function(index, element)
    {
    $(this).val(data.charges[g]['cellPhone']);
    }
);

$("INPUT[name='editCargoEmail-"+s+"']").each
(
    function(index, element)
    {
    $(this).val(data.charges[g]['email']);
    }
);

$("INPUT[name='cantEditCargo]").each
(
    function(index, element)
    {
    $(this).val(s);
    }
);
document.getElementById("cantEditCargo").value = s;




}
					
} 
					
		
				
				
				}else{
					showErrorToast(data.queryStatus);
				}
			}else{
				showErrorToast("NO DATA RETURNED");
			}
		 },
		 error:function (xhr, ajaxOptions, thrownError){
			showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		 }
		});  
		return false;  
	}	 



function showDataExportGestion(ID,IDType){
		 if(parseInt($("#iID").val()) !=0 && parseInt($("#iID").val()) !=-1){

		toastr.info("<br><button type='button' id='confirmationRevertYes' class='btn green'>Si</button><button type='button' id='confirmationRevertNo' class='btn default'>No</button>",'Desea ver los Datos?',
  {
      closeButton: false,
      allowHtml: true,
      onShown: function (toast) {
		  
          $("#confirmationRevertYes").click(function(){
            switch (IDType) {
  
    case 23:
	if (ID==23) {
		$cliente= document.getElementById('iUserIDC').value;
	$carpeta = document.getElementById('iEditIDC').value;
	} else {
		$cliente= document.getElementById('iUserIDE').value;
	$carpeta = document.getElementById('iEditID').value;
	}
	
        window.open('uploads/carpetas/'+$cliente+'/'+$carpeta,'_blank');
        break;
    case 2:
        window.open('exportPersonal.php?id='+ID,'_blank');
        break;
    case 3:
        window.open('exportProducts.php?id='+ID,'_blank');
        break;
		 case 33:
        window.open('exportProductsAll.php?id='+ID,'_blank');
        break;
    case 4:
	//alert('holasss');
	
        window.open('exportSearch.php?type='+ID+'&searchEmpleador='+document.getElementById('searchEmpleador').value+'&level='+document.getElementById('searchNivel').value
		+'&searchCuit='+document.getElementById('searchCuit').value
		+'&searchIps='+document.getElementById('searchIps').value
		+'&searchName='+document.getElementById('searchName').value
		+'&searchLast='+document.getElementById('searchLast').value
		+'&searchCuil='+document.getElementById('searchCuil').value,'_blank');
        break;
    case 5:
	//alert('holasss');
	
        window.open('exportSearch.php?type='+ID+'&searchEmpleador='+document.getElementById('searchEmpleador').value+'&level='+document.getElementById('searchNivel').value
		+'&searchCuit='+document.getElementById('searchCuit').value
		+'&searchIps='+document.getElementById('searchIps').value
		+'&searchName='+document.getElementById('searchName').value
		+'&searchLast='+document.getElementById('searchLast').value
		+'&searchCuil='+document.getElementById('searchCuil').value,'_blank');
        break;
    case 6:
        day = "Saturday";
		 break;
		 case 7:
        window.open('exportConceptAfip.php?sAction='+ID,'_blank');
        break;
		case 8:
			window.open('exportDeliquentTotal.php','_blank');
			break;
}
			    
          });
		  
		   $("#confirmationRevertNo").click(function(){
						toastr.info('Cancelado por Usuario.', 'Datos No Exportados.');
						//toastr.success('Datos No guardados', 'Editar Cargo');
						 $('#full-width').modal('hide');
						
				});

        }
  });
		

	
		
		}else{
			toastr.warning('Completo todos los datos?','Datos No guardados');
			//$.notific8(data.status);
			//showErrorToast("El usuario no existe. Modifique los datos y reintente");
		}	
}	

//para exportar datos en pdf

function showDataExport(ID,IDType){
		 if(parseInt($("#iID").val()) !=0 && parseInt($("#iID").val()) !=-1){

		toastr.info("<br><button type='button' id='confirmationRevertYes' class='btn green'>Si</button><button type='button' id='confirmationRevertNo' class='btn default'>No</button>",'Desea Exportar los Datos?',
  {
      closeButton: false,
      allowHtml: true,
      onShown: function (toast) {
		  
          $("#confirmationRevertYes").click(function(){
            switch (IDType) {
  
    case 1:
        window.open('exportProvider.php?id='+ID,'_blank');
        break;
    case 2:
        window.open('exportPersonal.php?id='+ID,'_blank');
        break;
    case 3:
        window.open('exportProducts.php?id='+ID,'_blank');
        break;
		 case 33:
        window.open('exportProductsAll.php?id='+ID,'_blank');
        break;
    case 4:
	//alert('holasss');
	
        window.open('exportSearch.php?type='+ID+'&searchEmpleador='+document.getElementById('searchEmpleador').value+'&level='+document.getElementById('searchNivel').value
		+'&searchCuit='+document.getElementById('searchCuit').value
		+'&searchIps='+document.getElementById('searchIps').value
		+'&searchName='+document.getElementById('searchName').value
		+'&searchLast='+document.getElementById('searchLast').value
		+'&searchCuil='+document.getElementById('searchCuil').value,'_blank');
        break;
    case 5:
	//alert('holasss');
	
        window.open('exportSearch.php?type='+ID+'&searchEmpleador='+document.getElementById('searchEmpleador').value+'&level='+document.getElementById('searchNivel').value
		+'&searchCuit='+document.getElementById('searchCuit').value
		+'&searchIps='+document.getElementById('searchIps').value
		+'&searchName='+document.getElementById('searchName').value
		+'&searchLast='+document.getElementById('searchLast').value
		+'&searchCuil='+document.getElementById('searchCuil').value,'_blank');
        break;
    case 6:
        day = "Saturday";
		 break;
		 case 7:
        window.open('exportConceptAfip.php?sAction='+ID,'_blank');
        break;
		case 8:
			window.open('exportDeliquentTotal.php','_blank');
			break;
}
			    
          });
		  
		   $("#confirmationRevertNo").click(function(){
						toastr.info('Cancelado por Usuario.', 'Datos No Exportados.');
						//toastr.success('Datos No guardados', 'Editar Cargo');
						 $('#full-width').modal('hide');
						
				});

        }
  });
		

	
		
		}else{
			toastr.warning('Completo todos los datos?','Datos No guardados');
			//$.notific8(data.status);
			//showErrorToast("El usuario no existe. Modifique los datos y reintente");
		}	
}	


	//muestra familiares
	function duplicateInvoice(ID,type){
		var dataString = 'sAction=duplicateInvoice&IDBILL=' + ID +'&type='+type;
		
		$.ajax({  
		type: "post",  
		data: dataString, 	
		dataType: "json",  
		url: "movementsABM.php",  
		success: function(data) {	
			if(data){
				
				if(data.queryStatus == "OK"){
	 
				//var filas = data.query.length;
				
			    var prueba = ' <div class="page-content-col" id="'+data.query[0].idNewBill+'">'+
                               ' <div class="row">'+
                                   ' <div class="col-md-12">'+
                                       ' <div class="portlet light portlet-fit bordered">'+
                                           ' <div class="portlet-title">'+
                                                '<div class="caption">'+
                                                    '<i class="icon-settings font-red"></i>'+
                                                   ' <span class="caption-subject font-red sbold uppercase">Datos Empleador</span>'+
													'<p>'+
													'<strong>'+data.query[0].school+' CUIT: '+data.query[0].scuit+' - '+data.query[0].level+': '+data.query[0].ips+'</strong><br>'+
													'CUI: '+data.query[0].cui+' '+data.query[0].city+' - '+data.query[0].province+'' +
													'</p>'+
													'<i class="icon-settings font-green"></i>'+
                                                    '<span class="caption-subject font-green sbold uppercase">Datos Empleado</span>'+
													'<p>'+
													'<strong>'+data.query[0].name+'<br>DNI: '+data.query[0].dni+' CUIL: '+data.query[0].cuit+'<br>'+
													'CARGO: '+data.query[0].nameCharge+' '+data.query[0].charge +' - FECHA INGRESO: '+data.query[0].highDate+'<BR>'+
													'ANTIGUEDAD: '+data.query[0].antiquity+' A&ntilde;os / '+data.query[0].antiquityM+' Meses </strong>'+
													'</p>'+
                                               ' </div>'+
                                    '<div class="col-md-3 ">'+
                                       ' <div class="portlet box blue-hoki">'+
                                            '<div class="portlet-title">'+
                                                '<div class="caption">'+
                                                   '<i class="icon-speech"></i>Observaciones </div>'+
                                           ' </div>'+
                                            '<div class="portlet-body">'+
                                              '  <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100px;">'+
											'	<div class="scroller" style="height: 100px; overflow: hidden; width: auto;" data-rail-visible="1" data-rail-color="yellow" data-handle-color="#a1b2bd" data-initialized="1">'+
                                             '    '+data.query[0].observaciones+'</div><div class="slimScrollBar" style="background: rgb(161, 178, 189) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 29px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 142.857px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; border-radius: 7px; background: yellow none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px; display: none;"></div></div>'+
                                            '</div>'+
                                        '</div>'+
                                       '<STRONG>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+data.query[0].typeAsign+'</STRONG>'+
										'<br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; AFEC: '+data.query[0].affectation+' / SEC: '+data.query[0].sequence+'  '+
                                   ' </div>'+
                                   ' <div class="col-md-3 ">'+
                                        '<div class="portlet box red">'+
                                            '<div class="portlet-title">'+
                                                '<div class="caption">'+
												'<i class="icon-speech"></i>'+
                                                '  Memo </div> '+
                                            '</div>'+
                                           ' <div class="portlet-body">'+
                                               ' <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100px;">'+
												'<div class="scroller" style="height: 100px; overflow: hidden; width: auto;" data-always-visible="1" data-rail-visible="1" data-rail-color="blue" data-handle-color="red" data-initialized="1" contenteditable="true" onBlur="saveToDatabase(this,4,'+data.query[0].idNewBill+')" onClick="showEdit(this);">'+
												'</div>'+
												'<div class="slimScrollBar" style="background: red none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px; height: 181.818px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: block; border-radius: 7px; background: blue none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div></div>'+
                                           ' </div>'+
                                       ' </div>'+
                                        '<STRONG> SUMA: <input type="text" style="font-size:20px; text-align:center;background-color: #b6edf7;WIDTH: 228px; HEIGHT: 98px" size="28" name="totalTN'+data.query[0].idNewBill+'" id="totalTN'+data.query[0].idNewBill+'" value="0"/></STRONG>'+
                                   ' </div>'+
                                             ' <div class="actions">'+
                                                    '<div class="btn-group btn-group-devided" data-toggle="buttons">'+
                                                      '  <h3>PERIODO: '+data.query[0].month+' - '+data.query[0].year+'<br>'+
														'</h3>'+
													''+data.query[0].agreement+'<br>'+
													'Periodo Recibo:<br>'+
													'<input type="text" class="focusable" onBlur="saveToDatabase(this,6,'+data.query[0].idNewBill+')" onClick="showEdit(this);" style="width:60px;height:15px" name="periodoM'+data.query[0].idNewBill+'" id="periodoM'+data.query[0].idNewBill+'" value="'+data.query[0].monthR+'"/> Mes <br>'+
													'<input type="text" class="focusable" onBlur="saveToDatabase(this,5,'+data.query[0].idNewBill+')" onClick="showEdit(this);" style="width:60px;height:15px" name="periodoA'+data.query[0].idNewBill+'" id="periodoA'+data.query[0].idNewBill+'" value="'+data.query[0].yearR+'"/> A&ntilde;o<br>'+
                                                    '</div>'+
                                               ' </div>'+
                                            '</div>'+
                                            '<div class="portlet-body">'+
                                               ' <div class="table-toolbar">'+
                                                    '<div class="row">'+
                                                      '  <div class="col-md-8">'+
												
                                                            '<div class="btn-group">'+
                                                               ' <a class="btn sbold green" id="sample_editable_'+data.query[0].idNewBill+'_new" data-target="#full-width" onclick="newConceptsForAgreementBill('+data.query[0].idNewBill+','+data.query[0].agre+');" data-toggle="modal"><i class="fa fa-plus"></i> Agregar </a>'+
                                                           ' </div>'+
															' <div class="btn-group">'+
                                                             '   <a class="btn default" id="sample_editable_'+data.query[0].idNewBill+'_new" data-target="#full-widthEdit" onclick="conceptsPermanent('+data.query[0].idNewBill+',1);" data-toggle="modal"><i class="fa fa-plus"></i> Permanentes </a>'+
                                                          '  </div>'+
														   ' <div class="btn-group">'+
                                                                '<a class="btn default" id="sample_editable_'+data.query[0].idNewBill+'_new" data-target="#full-widthEdit" onclick="conceptsPermanent('+data.query[0].idNewBill+',2);" data-toggle="modal"><i class="fa fa-plus"></i> Diferencias </a>'+
                                                            '</div>'+
															 ' <div class="btn-group">'+
                                                                '<a class="btn sbold green" id="sample_editable_'+data.query[0].idNewBill+'_new" data-target="#full-widthDevoluciones" onclick="showPersonalBySchoolEditBill('+data.query[0].pBs+','+data.query[0].idNewBill+','+data.query[0].month+','+data.query[0].year+');" data-toggle="modal"><i class="fa fa-plus"></i> Descuentos </a>'+
                                                            '</div>'+
															 ' <div class="btn-group">'+
                                                                '<a class="btn sbold green" id="sample_editable_'+data.query[0].idNewBill+'_new" data-target="#full-widthDescuentos" onclick="showPersonalBySchoolEditBillDes('+data.query[0].pBs+','+data.query[0].idNewBill+',2);" data-toggle="modal"><i class="fa fa-plus"></i> Devoluciones </a>'+
                                                            '</div>'+
															' <div class="btn-group">'+
                                                             '   <a class="btn default" id="sample_editable_'+data.query[0].idNewBill+'_new" data-target="#full-widthSalario" onclick="getSalary('+data.query[0].idNewBill+','+data.query[0].agre+');" data-toggle="modal"><i class="fa fa-plus"></i> Familiares </a>'+
                                                           ' </div>'+
														  
                                                       ' </div>'+
                                                       ' <div class="col-md-4">'+
                                                           ' <div class="btn-group pull-right">'+
                                                                '<button class="btn green btn-outline dropdown-toggle" data-toggle="dropdown">Tools'+
                                                                  '  <i class="fa fa-angle-down"></i>'+
                                                                '</button>'+
                                                               ' <ul class="dropdown-menu pull-right">'+
																' <li>'+
                                                         
                                                                      '  <a href="javascript:;" onclick="deleteInvoice('+data.query[0].idNewBill+');"> Eliminar </a>'+
                                                                   ' </li>'+
                                                                    
                                                              '  </ul>'+
                                                           ' </div>'+
                                                       ' </div>'+
                                                    '</div>'+
                                                '</div>'+
                                               ' <table class="table table-striped table-hover table-bordered" id="'+data.query[0].idNewBill+'">'+
                                                   ' <thead>'+
                                                       ' <tr>'+
                                                           ' <th> ID </th>'+
                                                           ' <th> DESCRIPCION </th>'+
                                                           ' <th> CANT. </th>'+
                                                           ' <th> REM </th>'+
                                                            '<th> NO REM </th>'+
                                                           ' <th> RETENC. </th>'+
                                                           ' <th> SUMA </th>'+
                                                            '<th> ELIMINAR </th>'+
                                                       ' </tr>'+
                                                   ' </thead>'+
                                                    '<tbody>'+
											' </tbody>'+
											   '<tfoot>'+
											 ' <tr>'+		
						      '<td></td>'+
						      '<td></td>'+
						      '<td></td>'+			
							'<td> <input type="text" class="focusable" name="totalRe'+ data.query[0].idNewBill + '" id="totalRe'+ data.query[0].idNewBill + '" value="0"/></td>'+
				            '<td> <input type="text" class="focusable" name="totalRen'+ data.query[0].idNewBill + '" id="totalRen'+ data.query[0].idNewBill + '" value="0"/></td>'+
				            '<td> <input type="text" class="focusable" name="totalRet'+ data.query[0].idNewBill + '" id="totalRet'+ data.query[0].idNewBill + '" value="0"/></td>'+
				              '<td></td>'+
							'<td> <input type="text" name="totalT'+ data.query[0].idNewBill + '" id="totalT'+ data.query[0].idNewBill + '" value="0"/></td>'+
				 ' </tr>'+
				 ' </tfoot>'+
                                               ' </table>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                               ' </div>'+
                            '</div>';
							
	$("#"+ID).before(prueba);

			      for (x=0;x<data.query.length;x++){			  
var row = $("<tr>");
//comienzo if		
if (typeof data.query[x].tipo !== 'undefined'){
	switch(data.query[x].tipo) {
    case "1":
        row.append($("<td>"+data.query[x].idConcept+"</td>"))
				.append($('<td contenteditable="true" onBlur="saveToDatabase(this,1 ,'+ data.query[x].movement + ','+data.query[x].idNewBill+')" onClick="showEdit(this);">'+ data.query[x].concepName +' </td>'))
				.append($('<td contenteditable="true" onBlur="saveToDatabase(this,3 ,'+ data.query[x].movement + ','+data.query[x].idNewBill+')" onClick="showEdit(this);">1</td>'))
				.append($('<td></td>'))
				.append($('<td contenteditable="true"onBlur="saveToDatabase(this, 2 , '+ data.query[x].movement + ','+data.query[x].idNewBill+')" onClick="showEdit(this);"> <input type="text" class="focusable" name="cantRen'+ data.query[x].idNewBill + '[]" id="'+ data.query[x].movement + '" onBlur="saveToDatabase(this, 2 , '+ data.query[x].movement + ','+data.query[x].idNewBill+')" onClick="showEdit(this);" onChange="totalRen('+ data.query[x].idNewBill + ','+ data.query[x].movement + ')" value="'+ data.query[x].value +'"/>	</td>'))
				.append($('<td></td>'))
				.append($('<td><input data-index="1" class="focusable" name="sumNeto'+ data.query[x].idNewBill + '" id="byNeto'+ data.query[x].movement +'" onclick="sumNeto(this,'+ data.query[x].idNewBill +');" type="checkbox" value="'+ data.query[x].value +','+data.query[x].tipo+'"> </td>'))
				.append($('<td><a  href="javascript:;" onclick="deleteConcept(this,'+ data.query[x].movement + ','+ data.query[x].idNewBill + ');" class="btn red-mint"><span class="glyphicon glyphicon-trash"></span></a></td>'));
				$("#"+data.query[x].idNewBill+" tbody").append(row);
        break;
    case "2":
       row.append($("<td>"+data.query[x].idConcept+"</td>"))
				.append($('<td contenteditable="true" onBlur="saveToDatabase(this,1 ,'+ data.query[x].movement + ','+data.query[x].idNewBill+')" onClick="showEdit(this);">'+ data.query[x].concepName +' </td>'))
				.append($('<td contenteditable="true" onBlur="saveToDatabase(this,3 ,'+ data.query[x].movement + ','+data.query[x].idNewBill+')" onClick="showEdit(this);">1</td>'))
				.append($('<td contenteditable="true" onBlur="saveToDatabase(this, 2 , '+ data.query[x].movement + ','+data.query[x].idNewBill+')" onClick="showEdit(this);"> <input type="text" class="focusable" name="cantRe'+ data.query[x].idNewBill + '[]" id="'+ data.query[x].movement + '" onBlur="saveToDatabase(this, 2 , '+ data.query[x].movement + ','+data.query[x].idNewBill+')" onClick="showEdit(this);" onChange="totalRe('+ data.query[x].idNewBill + ','+ data.query[x].movement + ')" value="'+ data.query[x].value +'"/>	</td>'))
				.append($('<td></td>'))
				.append($('<td></td>'))
				.append($('<td><input data-index="1" class="focusable" name="sumNeto'+ data.query[x].idNewBill + '" id="byNeto'+ data.query[x].movement +'" onclick="sumNeto(this,'+ data.query[x].idNewBill +');" type="checkbox" value="'+ data.query[x].value +','+data.query[x].tipo+'"> </td>'))
				.append($('<td><a  href="javascript:;" onclick="deleteConcept(this,'+ data.query[x].movement + ','+ data.query[x].idNewBill + ');" class="btn red-mint"><span class="glyphicon glyphicon-trash"></span></a></td>'));
				$("#"+data.query[x].idNewBill+" tbody").append(row);
        break;
		  case "3":
       row.append($("<td>"+data.query[x].idConcept+"</td>"))
				.append($('<td contenteditable="true" onBlur="saveToDatabase(this,1 ,'+ data.query[x].movement + ','+data.query[x].idNewBill+')" onClick="showEdit(this);">'+ data.query[x].concepName +' </td>'))
				.append($('<td contenteditable="true" onBlur="saveToDatabase(this,3 ,'+ data.query[x].movement + ','+data.query[x].idNewBill+')" onClick="showEdit(this);">1</td>'))
				.append($('<td contenteditable="true" onBlur="saveToDatabase(this, 2 , '+ data.query[x].movement + ','+data.query[x].idNewBill+')" onClick="showEdit(this);"> <input type="text" class="focusable" name="cantRe'+ data.query[x].idNewBill + '[]" id="'+ data.query[x].movement + '" onBlur="saveToDatabase(this, 2 , '+ data.query[x].movement + ','+data.query[x].idNewBill+')" onClick="showEdit(this);" onChange="totalRe('+ data.query[x].idNewBill + ','+ data.query[x].movement + ')" value="'+ data.query[x].value +'"/>	</td>'))
				.append($('<td></td>'))
				.append($('<td></td>'))
				.append($('<td><input data-index="1" class="focusable" name="sumNeto'+ data.query[x].idNewBill + '" id="byNeto'+ data.query[x].movement +'" onclick="sumNeto(this,'+ data.query[x].idNewBill +');" type="checkbox" value="'+ data.query[x].value +','+data.query[x].tipo+'"> </td>'))
				.append($('<td><a  href="javascript:;" onclick="deleteConcept(this,'+ data.query[x].movement + ','+ data.query[x].idNewBill + ');" class="btn red-mint"><span class="glyphicon glyphicon-trash"></span></a></td>'));
				$("#"+data.query[x].idNewBill+" tbody").append(row);
        break;
		  case "4":
       row.append($("<td>"+data.query[x].idConcept+"</td>"))
				.append($('<td contenteditable="true" onBlur="saveToDatabase(this,1 ,'+ data.query[x].movement + ','+data.query[x].idNewBill+')" onClick="showEdit(this);">'+ data.query[x].concepName +' </td>'))
				.append($('<td contenteditable="true" onBlur="saveToDatabase(this,3 ,'+ data.query[x].movement + ','+data.query[x].idNewBill+')" onClick="showEdit(this);">1</td>'))
				.append($('<td></td>'))
				.append($('<td></td>'))
				.append($('<td contenteditable="true" onBlur="saveToDatabase(this, 2 , '+ data.query[x].movement + ','+data.query[x].idNewBill+')" onClick="showEdit(this);"> <input type="text" class="focusable" name="cantRet'+ data.query[x].idNewBill + '[]" id="'+ data.query[x].movement + '" onBlur="saveToDatabase(this, 2 , '+ data.query[x].movement + ','+data.query[x].idNewBill+')" onClick="showEdit(this);" onChange="totalRet('+ data.query[x].idNewBill + ','+ data.query[x].movement + ')" value="'+ data.query[x].value +'"/>	</td>'))
				.append($('<td><input data-index="1" class="focusable" name="sumNeto'+ data.query[x].idNewBill + '" id="byNeto'+ data.query[x].movement +'" onclick="sumNeto(this,'+ data.query[x].idNewBill +');" type="checkbox" value="'+ data.query[x].value +','+data.query[x].tipo+'"> </td>'))
				.append($('<td><a  href="javascript:;" onclick="deleteConcept(this,'+ data.query[x].movement + ','+ data.query[x].idNewBill + ');" class="btn red-mint"><span class="glyphicon glyphicon-trash"></span></a></td>'));
				$("#"+data.query[x].idNewBill+" tbody").append(row);
        break;
    default:
       toastr.error('Hubo un error en agregar conceptos tipo','Datos No agregados');
} //fin suit
//fin del if
}	 else {
	toastr.error('Hubo un error en agregar conceptos fuera','Datos No agregados');
}						
											
}
//fin de for	
toastr.success('Ha agregado.', 'Dato agregado correctamente.');												

					
//fin de l queryStatus
				}else{

						toastr.error('Algo salio mal 1','No se pudo duplicar el recibo.');

				}
				//if data
			}else{
					toastr.error('Algo salio mal 2','No se pudo duplicar el recibo.');
			}
			//fin del success
		 },
		 error:function (xhr, ajaxOptions, thrownError){
			 console.log(data);
			 //toastr.error(xhr.responseText,thrownError);
			
			//showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		 }
		});  
		
		return false;  
	}


	

	
	//mustra informaci�n de una obra social
	function showSocialWorkData(ID){
		var dataString = 'sAction=searchSocialWorkById&socialID=' + ID;
		$.ajax({  
		type: "post",  
		data: dataString, 	
		dataType: "json",  
		url: "socialWork.php",  
		success: function(data) {
			
			if(data){
				if(data.queryStatus == "OK"){

					$("#editCode").val(data.id);
					$("#editNombre").val(data.name);
					$("#editSigla").val(data.description);
					$("#iEditID").val(data.id);

				}else{
					showErrorToast(data.queryStatus);
				}
			}else{
				showErrorToast("NO DATA RETURNED");
			}
		 },
		 error:function (xhr, ajaxOptions, thrownError){
			showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		 }
		});  
		return false;  
	}	



		//mustra informaci�n de una persona 
		function showProductsData(ID){
			var dataString = 'sAction=searchProductsById&productsID=' + ID;
			$.ajax({  
			type: "post",  
			data: dataString, 	
			dataType: "json",  
			url: "productsABM.php",  
			success: function(data) {
				
				if(data){
					if(data.queryStatus == "OK"){
	
						$("#editDescription").val(data.description);
						$("#editMin").val(data.minStock);
						$("#iEditID").val(data.id);
						$("#editStock").val(data.stock);
						$("#editPrecio").val(data.totalValue);
						
						$("#editFecha").val(data.dateCase);
						$("#editPrecioPromedio").val(data.proValue);
						$("#editUbicacion").val(data.gps);

						$("textarea#editObser").val(data.memo);
						
						document.getElementById('editImage').src = 'img/products/signature/' + data.signature;
						//$("#editImg").val(data.logo);
						$("#editImgTest").val(data.signature);


							$("#editCategory option").each(function(){
							if($(this).val() == data.category){
								$(this).attr("selected","'selected'");
								$("#editCategory").val(data.category);
							}						
						});
						
						
							$("#editService option").each(function(){
							if($(this).val() == data.idService){
								$(this).attr("selected","'selected'");
								$("#editService").val(data.idService);
							}						
						});

						$("#editNivel option").each(function(){
							if($(this).val() == data.idLevel){
								$(this).attr("selected","'selected'");
								$("#editNivel").val(data.idLevel);
							}						
						});
	
					
					}else{
						showErrorToast(data.queryStatus);
					}
				}else{
					showErrorToast("NO DATA RETURNED");
				}
			 },
			 error:function (xhr, ajaxOptions, thrownError){
				showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
			 }
			});  
			return false;  
		}


function showProductsDataUser(ID){
			var dataString = 'sAction=searchProductsById&productsID=' + ID;
			$.ajax({  
			type: "post",  
			data: dataString, 	
			dataType: "json",  
			url: "productsABMUser.php",  
			success: function(data) {
				
				if(data){
					if(data.queryStatus == "OK"){
	
						$("#editDescription").val(data.description);
						$("#editMin").val(data.minStock);
						$("#iEditID").val(data.id);
						$("#editStock").val(data.stock);
						

						$("textarea#editObser").val(data.memo);
						
						document.getElementById('editImage').src = 'img/products/signature/' + data.signature;
						//$("#editImg").val(data.logo);
						$("#editImgTest").val(data.signature);


							$("#editCategory option").each(function(){
							if($(this).val() == data.category){
								$(this).attr("selected","'selected'");
								$("#editCategory").val(data.category);
							}						
						});
						
						
						

						$("#editNivel option").each(function(){
							if($(this).val() == data.idLevel){
								$(this).attr("selected","'selected'");
								$("#editNivel").val(data.idLevel);
							}						
						});
	
					
					}else{
						showErrorToast(data.queryStatus);
					}
				}else{
					showErrorToast("NO DATA RETURNED");
				}
			 },
			 error:function (xhr, ajaxOptions, thrownError){
				showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
			 }
			});  
			return false;  
		}


	//mustra informaci�n de una persona 
	 	function showPersonalByBillData(ID){
		var dataString = 'sAction=searchPersonalById&personalID=' + ID;
		$.ajax({  
		type: "post",  
		data: dataString, 	
		dataType: "json",  
		url: "personalByBill.php",  
		success: function(data) {
			
			if(data){
				if(data.queryStatus == "OK"){

					$("#editNombre").val(data.name);
					$("#editApellido").val(data.lastName);
					$("#iEditID").val(data.id);
					$("#editMonto").val(data.totalValue);
					$("#editCuit").val(data.cuit);
					$("textarea#editObser").val(data.memo);
					
					$("#editDocumento").val(data.document);
						$("#editTipoDocumento option").each(function(){
						if($(this).val() == data.typeDocument){
							$(this).attr("selected","'selected'");
							$("#editTipoDocumento").val(data.typeDocument);
						}						
					});
					
						$("#editCategoria option").each(function(){
						if($(this).val() == data.category){
							$(this).attr("selected","'selected'");
							$("#editCategoria").val(data.category);
						}						
					});

				
				}else{
					showErrorToast(data.queryStatus);
				}
			}else{
				showErrorToast("NO DATA RETURNED");
			}
		 },
		 error:function (xhr, ajaxOptions, thrownError){
			showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		 }
		});  
		return false;  
	}

	//mustra informaci�n de una persona 
	function showUserData(ID){
		var dataString = 'sAction=searchUserById&userID=' + ID;
		$.ajax({  
		type: "post",  
		data: dataString, 	
		dataType: "json",  
		url: "usersABM.php",  
		success: function(data) {
			
			if(data){
				if(data.queryStatus == "OK"){


					//elimino todo de la tabla de cargos				
var tableHeaderRowCount = 1;
var table = document.getElementById('editCargos');
var rowCount = table.rows.length;
for (var i = tableHeaderRowCount; i < rowCount; i++) {
    table.deleteRow(tableHeaderRowCount);
}
s=0;




					$("#editNombre").val(data.name);
					$("#editApellido").val(data.lastName);
					$("#iEditID").val(data.id);
					$("#editCuit").val(data.cuit);
					$("#editBox").val(data.box);
					$("#editDomicilio").val(data.address);
					$("#editTelefono").val(data.phone);
					$("#editUserAge").val(data.age);
					

					$("#editLocalidad").val(data.location);
					$("#editSocio").val(data.socio);

					$("#editTelefonoAdicional").val(data.cellphone);
					
					$("#editEmail").val(data.email);
					$("textarea#editObser").val(data.memo);
					
					$("#editDocumento").val(data.document);
					$("#editFechaNacimiento").val(data.birthDate);
					$("#editFechaAlta").val(data.dischargeDate);
					
				
					$("#editEstadoCivil option").each(function(){
						if($(this).val() == data.civil){
							$(this).attr("selected","'selected'");
							$("#editEstadoCivil").val(data.civil);
						}						
					});
					$("#editSexo option").each(function(){
						if($(this).val() == data.gen){
							$(this).attr("selected","'selected'");
							$("#editSexo").val(data.gen);
						}						
					});

	
					$("#editProvinciaUser option").each(function(){
						if($(this).val() == data.id_province){
							$(this).attr("selected","'selected'");
							$("#editProvinciaUser").val(data.id_province);
							//$("#editProvincia").val(data.id_province).trigger("change");
						}						
					});

					selectCityByIdUser(data.id_province,"editCiudadUser",data.id_city);
					
					
					$("#editType option").each(function(){
						if($(this).val() == data.type){
							$(this).attr("selected","'selected'");
							$("#editType").val(data.type);
						}						
					});
					
					
						$("#editEstado option").each(function(){
						if($(this).val() == data.status){
							$(this).attr("selected","'selected'");
							$("#editEstado").val(data.status).trigger("change");
						}						
					});
					
					$("#editJul0").prop('checked',false);
					$("#editJul1").prop('checked',false);
					
					
					if(data.retired == 1){
							  $("#editJul1").prop('checked',true); 
						} else {
								     $("#editJul0").prop('checked',true); 		 	
						}
					$("#editJul").val(data.retired);
					
					
						$("#editTipoDocumento option").each(function(){
						if($(this).val() == data.typeDocument){
							$(this).attr("selected","'selected'");
							$("#editTipoDocumento").val(data.typeDocument);
						}						
					});
					
						$("#editCategoria option").each(function(){
						if($(this).val() == data.category){
							$(this).attr("selected","'selected'");
							$("#editCategoria").val(data.category);
						}						
					});
					
					//getCiudades(data.id_province,2,data.id_city);
				
					$("#editCpUser").val(data.zipCode);


					if (data.charges.length>0){


						for (g = 0; g < data.charges.length; g++) {
							
						
							
						$( "#descripcionEdit" ).clone().attr('id', $( "#descripcionEdit" ).attr("id")+(++s)).appendTo( "#editCargos" );
						$( "#descripcionEdit"+s ).css("display","");
						
						$("#descripcionEdit"+s+" select" ).each(function(index){
							$(this).attr("name",$(this).attr("name")+s);
							$(this).val(data.charges[g]['idCharge']);
							$(this).attr("selected","'selected'");
							if($(this).val() == data.charges[g]['idCharge']){
													$(this).attr("selected","'selected'");
												}	
							
						});
						$("#descripcionEdit"+s+" input" ).each(function(index){
							//alert(this);
							$(this).attr("name",$(this).attr("name")+s);
							$('input[name=editCargoName-]'+s).val(data.charges[g]['name']);
							$("#editCargoName-"+s).val(data.charges[g]['name']);
							//$("#editCargoTelefono-".s).val(data.charges[g]['phone']);
							$("#editCargoEdad-"+s).val(data.charges[g]['age']);
							$("#editCargoFecha-"+s).val(data.charges[g]['dateBegin']);
							$("#editCargoDNI-"+s).val(data.charges[g]['dni']);
							//alert($(this).attr("name"));
						});
						
						//alert(data.charges[g]['dateBegin']);
						
						$("INPUT[name='editCargoName-"+s+"']").each
						(
							function(index, element)
							{
							$(this).val(data.charges[g]['name']);
							}
						);
						
						$("INPUT[name='editCargoDNI-"+s+"']").each
						(
							function(index, element)
							{
							$(this).val(data.charges[g]['dni']);
							}
						);
						
						$("INPUT[name='editCargoFecha-"+s+"']").each
						(
							function(index, element)
							{
							$(this).val(data.charges[g]['dateBegin']);
							}
						);
						
						$("INPUT[name='editCargoEdad-"+s+"']").each
						(
							function(index, element)
							{
							$(this).val(data.charges[g]['age']);
							}
						);
						
						
						
						$("INPUT[name='cantEditCargo]").each
						(
							function(index, element)
							{
							$(this).val(s);
							}
						);
						document.getElementById("cantEditCargo").value = s;
						
						
						
						
						}
											
						} 




					

				}else{
					showErrorToast(data.queryStatus);
				}
			}else{
				showErrorToast("NO DATA RETURNED");
			}
		 },
		 error:function (xhr, ajaxOptions, thrownError){
			showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		 }
		});  
		return false;  
	}

	
	//mustra informaci�n de una persona 
	 	function showPersonalData(ID){
		var dataString = 'sAction=searchPersonalById&personalID=' + ID;
		$.ajax({  
		type: "post",  
		data: dataString, 	
		dataType: "json",  
		url: "personalABM.php",  
		success: function(data) {
			
			if(data){
				if(data.queryStatus == "OK"){

//elimino todo de la tabla de cargos				
var tableHeaderRowCount = 1;
var table = document.getElementById('editCargos');
var rowCount = table.rows.length;
for (var i = tableHeaderRowCount; i < rowCount; i++) {
    table.deleteRow(tableHeaderRowCount);
}
s=0;

//elimino todo de la tabla de empleadores				
var tableHeaderRowCount = 1;
var table = document.getElementById('editEmpleadores');
var rowCount = table.rows.length;
for (var i = tableHeaderRowCount; i < rowCount; i++) {
    table.deleteRow(tableHeaderRowCount);
}
z=0;
				

					$("#editNombre").val(data.name);
					$("#editApellido").val(data.lastName);
					$("#iEditID").val(data.id);
					$("#editCuit").val(data.cuit);
					$("#editBox").val(data.box);
					$("#editDomicilio").val(data.address);
					$("#editTelefono").val(data.phone);
					
					$("#editLocalidad").val(data.location);

					$("#editTelefonoAdicional").val(data.cellphone);
					
					$("#editEmail").val(data.email);
					$("textarea#editObser").val(data.memo);
					
					$("#editDocumento").val(data.document);
					$("#editFechaNacimiento").val(data.birthDate);
					
				
					$("#editEstadoCivil option").each(function(){
						if($(this).val() == data.civil){
							$(this).attr("selected","'selected'");
							$("#editEstadoCivil").val(data.civil);
						}						
					});
					$("#editSexo option").each(function(){
						if($(this).val() == data.sex){
							$(this).attr("selected","'selected'");
							$("#editSexo").val(data.sex);
						}						
					});

	
					$("#editProvincia option").each(function(){
						if($(this).val() == data.id_province){
							$(this).attr("selected","'selected'");
							$("#editProvincia").val(data.id_province);
							//$("#editProvincia").val(data.id_province).trigger("change");
						}						
					});

					selectCityById(data.id_province,"editCiudad",data.id_city);
					
					
					$("#editSindicato option").each(function(){
						if($(this).val() == data.labor){
							$(this).attr("selected","'selected'");
							$("#editSindicato").val(data.labor);
						}						
					});
					
					
						$("#editObraSocial option").each(function(){
						if($(this).val() == data.social){
							$(this).attr("selected","'selected'");
							$("#editObraSocial").val(data.social).trigger("change");
						}						
					});
					
					$("#editJul0").prop('checked',false);
					$("#editJul1").prop('checked',false);
					
					
					if(data.retired == 1){
							  $("#editJul1").prop('checked',true); 
						} else {
								     $("#editJul0").prop('checked',true); 		 	
						}
					$("#editJul").val(data.retired);
					
					
						$("#editTipoDocumento option").each(function(){
						if($(this).val() == data.typeDocument){
							$(this).attr("selected","'selected'");
							$("#editTipoDocumento").val(data.typeDocument);
						}						
					});
					
						$("#editCategoria option").each(function(){
						if($(this).val() == data.category){
							$(this).attr("selected","'selected'");
							$("#editCategoria").val(data.category);
						}						
					});
					
					//getCiudades(data.id_province,2,data.id_city);
				
					$("#editCp").val(data.zipCode);
//si tiene cargos asignados vemos

if (data.charges.length>0){


for (g = 0; g < data.charges.length; g++) {
	

	
$( "#descripcionEdit" ).clone().attr('id', $( "#descripcionEdit" ).attr("id")+(++s)).appendTo( "#editCargos" );
$( "#descripcionEdit"+s ).css("display","");

$("#descripcionEdit"+s+" select" ).each(function(index){
	$(this).attr("name",$(this).attr("name")+s);
	$(this).val(data.charges[g]['idCharge']);
	$(this).attr("selected","'selected'");
	if($(this).val() == data.charges[g]['idCharge']){
							$(this).attr("selected","'selected'");
						}	
	
});
$("#descripcionEdit"+s+" input" ).each(function(index){
	//alert(this);
	$(this).attr("name",$(this).attr("name")+s);
	$('input[name=editCargoName-]'+s).val(data.charges[g]['name']);
	$("#editCargoName-"+s).val(data.charges[g]['name']);
	//$("#editCargoTelefono-".s).val(data.charges[g]['phone']);
	$("#editCargoEdad-"+s).val(data.charges[g]['age']);
	$("#editCargoFecha-"+s).val(data.charges[g]['dateBegin']);
	$("#editCargoDNI-"+s).val(data.charges[g]['dni']);
	//alert($(this).attr("name"));
});

//alert(data.charges[g]['dateBegin']);

$("INPUT[name='editCargoName-"+s+"']").each
(
    function(index, element)
    {
    $(this).val(data.charges[g]['name']);
    }
);

$("INPUT[name='editCargoDNI-"+s+"']").each
(
    function(index, element)
    {
    $(this).val(data.charges[g]['dni']);
    }
);

$("INPUT[name='editCargoFecha-"+s+"']").each
(
    function(index, element)
    {
    $(this).val(data.charges[g]['dateBegin']);
    }
);

$("INPUT[name='editCargoEdad-"+s+"']").each
(
    function(index, element)
    {
    $(this).val(data.charges[g]['age']);
    }
);



$("INPUT[name='cantEditCargo]").each
(
    function(index, element)
    {
    $(this).val(s);
    }
);
document.getElementById("cantEditCargo").value = s;




}
					
} 
					
//si tiene cargos asignados vemos

if (data.employed.length>0){

for (g = 0; g < data.employed.length; g++) {
	
$( "#descripcionEditEmpleador" ).clone().attr('id', $( "#descripcionEditEmpleador" ).attr("id")+(++z)).appendTo( "#editEmpleadores" );
$( "#descripcionEditEmpleador"+z ).css("display","");

$("#descripcionEditEmpleador"+z+" input" ).each(function(index){
	//alert(this);
	$(this).attr("name",$(this).attr("name")+z);
	$('input[name=editNameEmpleador-]'+z).val(data.employed[g]['school']);
	$("#editNameEmpleador-"+z).val(data.employed[g]['school']);
	//$("#editCargoTelefono-".z).val(data.employed[g]['phone']);
	$("#editCargoEmpleador-"+z).val(data.employed[g]['charge']);
	$("#editFechaEmpleador-"+z).val(data.employed[g]['dateBegin']);
	//alert($(this).attr("name"));
});

//alert(data.employed[g]['dateBegin']);

$("INPUT[name='editNameEmpleador-"+z+"']").each
(
    function(index, element)
    {
    $(this).val(data.employed[g]['school']);
    }
);

$("INPUT[name='editFechaEmpleador-"+z+"']").each
(
    function(index, element)
    {
    $(this).val(data.employed[g]['dateBegin']);
    }
);

$("INPUT[name='editCargoEmpleador-"+z+"']").each
(
    function(index, element)
    {
    $(this).val(data.employed[g]['charge']);
    }
);



$("INPUT[name='cantEditEmpleadores]").each
(
    function(index, element)
    {
    $(this).val(s);
    }
);
document.getElementById("cantEditEmpleadores").value = s;




}
					
} 					
					
					
					
					
					
					
					
					
						/*$("#editSubCategory option").each(function(){
						if($(this).val() == data.subcategory){
							$(this).attr("selected","'selected'");
						}						
					});*/
					
					/*	$("#editMarca option").each(function(){
						if($(this).val() == data.brand){
							$(this).attr("selected","'selected'");
						}						
					});
					
					$("#editStatus option").each(function(){
						if($(this).val() == data.status){
							$(this).attr("selected","'selected'");
						}						
					});
					
					$("#editOferta option").each(function(){
						if($(this).val() == data.offer){
							$(this).attr("selected","'selected'");
						}						
					});*/
				
				
				}else{
					showErrorToast(data.queryStatus);
				}
			}else{
				showErrorToast("NO DATA RETURNED");
			}
		 },
		 error:function (xhr, ajaxOptions, thrownError){
			showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		 }
		});  
		return false;  
	}
	
		//solamente para ver los conceptos si eligen formula en formulario conceptos
	function viewConceptData(valor,campo){
		if(valor == true){
			$(campo).removeAttr("style");
		}else{
			$(campo).attr("style","display:none");
		}	
	}
	
	//mostrar el concepto
	function showConceptData(conceptID){
		
		//openDialog("editConcept","Editar Concepto",0,0,true);
		var dataString = 'sAction=searchConceptById&conceptID=' + conceptID;
		$("#iConceptID").val(conceptID);
		$.ajax({  
		type: "post",  
		data: dataString,  
		dataType: "json",  
		url: "conceptABM.php",  
		success: function(data) {
			
			if(data){
				if(data.queryStatus == "OK"){
					
					
					/*$("#editConceptDataType1").removeAttr("checked");
					$("#editConceptDataType1").removeAttr("disabled");
					$("#editConceptDataType2").removeAttr("checked");
					$("#editConceptDataType2").removeAttr("disabled");
					$("#editConceptDataType3").removeAttr("checked");
					$("#editConceptDataType3").removeAttr("disabled");
					$("#editConceptDataType4").removeAttr("checked");
					$("#editConceptDataType4").removeAttr("disabled");
					$("#editConceptDataType5").removeAttr("checked");
					$("#editConceptDataType5").removeAttr("disabled");
					$("#editConceptRadio0").removeAttr("checked");
					$("#editConceptRadio0").removeAttr("disabled");
					$("#editConceptRadio1").removeAttr("checked");
					$("#editConceptRadio1").removeAttr("disabled");
					$("#editConceptRadio"+data.type).attr("checked","'checked'");*/
					
					$("#editConceptDataType1").prop('checked',false);
					$("#editConceptDataType2").prop('checked',false);
					$("#editConceptDataType3").prop('checked',false);
					$("#editConceptDataType4").prop('checked',false);
					$("#editConceptDataType5").prop('checked',false);
					$("#editConceptDataType6").prop('checked',false);
					$("#editConceptDataType7").prop('checked',false);
					$("#editConceptDataType9").prop('checked',false);
					$("#editConceptDataType10").prop('checked',false);
					$("#editConceptDataType11").prop('checked',false);

					$("#editConceptRadio0").prop('checked',false);
					$("#editConceptRadio1").prop('checked',false);
					$("#editConceptAnti").val(data.year);
					$("#editConceptAntiM").val(data.yearM);
					
				
					
					
					
					
					$("#editConceptName").val(data.name);
					$("#editConceptValue").val(data.formula);
	
					/*$("#editConceptType option").each(function(){
						if($(this).val() == data.type){
							$(this).attr("selected","'selected'");
						}						
					});*/

							
						$("#editConceptDataType8 option").each(function(){
						if($(this).val() == data.category){
							$(this).attr("selected","'selected'");
							$("#editConceptDataType8").val(data.category);
							
						}						
					});
					
						$("#editConvenio option").each(function(){
						if($(this).val() == data.idAgreement){
							$(this).attr("selected","'selected'");
							$("#editConvenio").val(data.idAgreement);
							
						}						
					});	
					
					
						$("#editSeeInvoice option").each(function(){
						if($(this).val() == data.typeShow){
							$(this).attr("selected","'selected'");
							$("#editSeeInvoice").val(data.typeShow);
							
						}						
					});	
					
							$("#editSumMeca option").each(function(){
						if($(this).val() == data.bruto){
							$(this).attr("selected","'selected'");
							$("#editSumMeca").val(data.bruto);
							
						}						
					});
					
				
					
				getConceptsByAgreement(data.idAgreement,2);
				getTypesLiquidationByConcept(data.service);
				
				
					switch(data.dataType) {
    case "0":
         //$("#editConceptDataType1").prop('checked',true);
		 $("#editConceptDataType1").prop('checked',true); 
		 //$("#editConceptDataType1").attr("checked","'checked'"); 
        break;
    case "1":
		$("#editConceptDataType2").prop('checked',true); 
		//$("#editConceptDataType2").attr("checked","'checked'");
         //$("#editConceptDataType2").prop('checked',true); 
        break; 
		case "2":
		$("#editConceptDataType3").prop('checked',true); 
		//$("#editConceptDataType3").attr("checked","'checked'");
         //$("#editConceptDataType3").prop('checked',true); 
        break; 
		case "3":
		$("#editConceptDataType4").prop('checked',true); 
		//$("#editConceptDataType4").attr("checked","'checked'");
         //$("#editConceptDataType4").prop('checked',true); 
        break; 
		case "4":
		//$("#editConceptDataType5").attr("checked","'checked'");
		$("#editConceptDataType5").prop('checked',true); 
         //$("#editConceptDataType5").prop('checked',true); 
        break;
} 


						
							if(data.type == 1){
							 
							  $("#editConceptRadio1").prop('checked',true); 
								   viewConceptData(true,'#editTableConcept');
							   
						} else {
								  
								     $("#editConceptRadio0").prop('checked',true); 
							    viewConceptData(false,'#editTableConcept');
								  
								 	
						}
						
						if((data.antiquity == 1) || (data.basic==2) || (data.basic=10) || (data.basic=11) || (data.basic==12)){
							
							    
								 $("#editConceptAnti").prop('disabled',false);								 
								 $("#editConceptAntiM").prop('disabled',false);								 
								 
							  
						} else {
								
								    
									 $("#editConceptAnti").prop('disabled',true);
									 $("#editConceptAntiM").prop('disabled',true);
								 	
						}
						
						
						
						if(data.fixedCharge==1){
						     
						      $("#editConceptDataType6").prop('checked',true);
							  
						} else {
						
						 $("#editConceptDataType6").prop('checked',false);
							  
						}
						
						if(data.news==1){
						     
						      $("#editConceptDataType9").prop('checked',true);
							  
						} else {
						
						 $("#editConceptDataType9").prop('checked',false);
							  
						}
						
					
						if(data.authorized==1){
						      
						      $("#editConceptDataType7").prop('checked',true);
							  
						} else {
						 
						 $("#editConceptDataType7").prop('checked',false);
							  
						}
						
						
						if(data.disability==1){
						     
						      $("#editConceptDataType10").prop('checked',true);
							  
						} else {
						
						 $("#editConceptDataType10").prop('checked',false);
							  
						}
						
						if(data.sum_total==1){
						     
						      $("#editConceptDataType11").prop('checked',true);
							  
						} else {
						
						 $("#editConceptDataType11").prop('checked',false);
							  
						}
						
						
						

				}else{
					showErrorToast(data.queryStatus);
				}
			}else{
				showErrorToast("NO DATA RETURNED");
			}
		 },
		 error:function (xhr, ajaxOptions, thrownError){
			showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		 }
		});  
		return false;  
	}
	
	function showConceptMappeoData(conceptMeID){
		
		//openDialog("editConcept","Editar Concepto",0,0,true);
		var dataString = 'sAction=searchConceptMappeoById&conceptMeID=' + conceptMeID;
		$("#iEditID").val(conceptMeID);
		$.ajax({  
		type: "post",  
		data: dataString,  
		dataType: "json",  
		url: "conceptAfipABM.php",  
		success: function(data) {
			
			if(data){
				if(data.queryStatus == "OK"){

					$("#editRepeticion").prop('checked',false);
					$("#editSipaAportes").prop('checked',false);
					$("#editSipaContribuciones").prop('checked',false);
					$("#editINSSJyPAportes").prop('checked',false);
					$("#editINSSJyPContribuciones").prop('checked',false);
					$("#editObraSocialAportes").prop('checked',false);
					$("#editObraSocialContribuciones").prop('checked',false);
					$("#editFSRAportes").prop('checked',false);
					$("#editFSRContribuciones").prop('checked',false);
					$("#editRenatreAportes").prop('checked',false);
					$("#editRenatreContribuciones").prop('checked',false);
					$("#editAFContribuciones").prop('checked',false);
					$("#editFondoNacionalContribuciones").prop('checked',false);
					$("#editLeyRiesgosContribuciones").prop('checked',false);
					$("#editRegimenesDifAportes").prop('checked',false);
					$("#editRegimenesEspecialesAportes").prop('checked',false);

				
	
						$("#editSearchConceptoAfip option").each(function(){
						if($(this).val() == data.conceptAfip_id){
							$(this).attr("selected","'selected'");
							$("#editSearchConceptoAfip").val(data.conceptAfip_id);
							
						}						
					});
					
						$("#editSearchConceptoMe option").each(function(){
						if($(this).val() == data.concept_id){
							$(this).attr("selected","'selected'");
							$("#editSearchConceptoMe").val(data.concept_id);
							
						}						
					});	


						if(data.repeticion==1){
						      $("#editRepeticion").prop('checked',true);
						} else {
						 $("#editRepeticion").prop('checked',false);	  
						}
						
						if(data.sipaAportes==1){
						      $("#editSipaAportes").prop('checked',true);
						} else {
						 $("#editSipaAportes").prop('checked',false);	  
						}
						if(data.sipaContribuciones==1){
						      $("#editSipaContribuciones").prop('checked',true);
						} else {
						 $("#editSipaContribuciones").prop('checked',false);	  
						}
						if(data.INSSJyPAportes==1){
						      $("#editINSSJyPAportes").prop('checked',true);
						} else {
						 $("#editINSSJyPAportes").prop('checked',false);	  
						}
						if(data.INSSJyPContribuciones==1){
						      $("#editINSSJyPContribuciones").prop('checked',true);
						} else {
						 $("#editINSSJyPContribuciones").prop('checked',false);	  
						}
						if(data.obraSocialAportes==1){
						      $("#editObraSocialAportes").prop('checked',true);
						} else {
						 $("#editObraSocialAportes").prop('checked',false);	  
						}
						if(data.obraSocialContribuciones==1){
						      $("#editObraSocialContribuciones").prop('checked',true);
						} else {
						 $("#editObraSocialContribuciones").prop('checked',false);	  
						}
						if(data.FSRAportes==1){
						      $("#editFSRAportes").prop('checked',true);
						} else {
						 $("#editFSRAportes").prop('checked',false);	  
						}
							if(data.FSRContribuciones==1){
						      $("#editFSRContribuciones").prop('checked',true);
						} else {
						 $("#editFSRContribuciones").prop('checked',false);	  
						}
							if(data.renatreAportes==1){
						      $("#editRenatreAportes").prop('checked',true);
						} else {
						 $("#editRenatreAportes").prop('checked',false);	  
						}
							if(data.renatreContribuciones==1){
						      $("#editRenatreContribuciones").prop('checked',true);
						} else {
						 $("#editRenatreContribuciones").prop('checked',false);	  
						}
					if(data.AFContribuciones==1){
						      $("#editAFContribuciones").prop('checked',true);
						} else {
						 $("#editAFContribuciones").prop('checked',false);	  
						}
					if(data.fondoNacionalContribuciones==1){
						      $("#editFondoNacionalContribuciones").prop('checked',true);
						} else {
						 $("#editFondoNacionalContribuciones").prop('checked',false);	  
						}
						if(data.leyRiesgosContribuciones==1){
						      $("#editLeyRiesgosContribuciones").prop('checked',true);
						} else {
						 $("#editLeyRiesgosContribuciones").prop('checked',false);	  
						}
						if(data.regimenesDifAportes==1){
						      $("#editRegimenesDifAportes").prop('checked',true);
						} else {
						 $("#editRegimenesDifAportes").prop('checked',false);	  
						}
				if(data.eregimenesEspecialesAportes==1){
						      $("#editRegimenesEspecialesAportes").prop('checked',true);
						} else {
						 $("#editRegimenesEspecialesAportes").prop('checked',false);	  
						}
					
				
						
						

				}else{
					showErrorToast(data.queryStatus);
				}
			}else{
				showErrorToast("NO DATA RETURNED");
			}
		 },
		 error:function (xhr, ajaxOptions, thrownError){
			showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		 }
		});  
		return false;  
	}
	
	//solamente para agregar los conceptos a la formula
	function copyValueOnNew(){
		var a =	'{'+document.getElementById("newCon").value +'}';
		var b= document.getElementById("newConceptValue").value;
		document.getElementById("newConceptValue").value = b+a;
	}
	
	function copyValueOnEdit(){
		var a =	'{'+document.getElementById("editCon").value +'}';
		var b= document.getElementById("editConceptValue").value;
		document.getElementById("editConceptValue").value = b+a;
	}
	
	
	//mustra informaci�n de un convenio
	function showAgreementData(ID){
		var dataString = 'sAction=searchAgreementById&convenioID=' + ID;
		$.ajax({  
		type: "post",  
		data: dataString, 	
		dataType: "json",  
		url: "agreementABM.php",  
		success: function(data) {
			
			if(data){
				if(data.queryStatus == "OK"){

					$("#editConvenio").val(data.name);
					$("#iEditID").val(data.id);

				}else{
					showErrorToast(data.queryStatus);
				}
			}else{
				showErrorToast("NO DATA RETURNED");
			}
		 },
		 error:function (xhr, ajaxOptions, thrownError){
			showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		 }
		});  
		return false;  
	}
	


		//mustra informaci�n de un convenio
		function showCategoryDataConcept(ID){
			var dataString = 'sAction=searchCategoryById&categoryID=' + ID;
			$.ajax({  
			type: "post",  
			data: dataString, 	
			dataType: "json",  
			url: "category_concept_ABM.php",  
			success: function(data) {
				
				if(data){
					if(data.queryStatus == "OK"){
						
						$("#editType0").prop('checked',false);
						$("#editType1").prop('checked',false);
						$("#editType2").prop('checked',false);
						$("#editType3").prop('checked',false);
						
						$("#editTypeO0").prop('checked',false);
						$("#editTypeO1").prop('checked',false);
						$("#editTypeO2").prop('checked',false);
						$("#editTypeO3").prop('checked',false);
						
						$("#editCategoria").val(data.name);
						$("#editVal").val(data.value);
						
						switch(data.type) {
		case "0":
			 $("#editType0").prop('checked',true); 
			break;
		case "1":
			$("#editType1").prop('checked',true); 
			   break; 
			case "2":
			$("#editType2").prop('checked',true); 
			break; 
			case "3":
			$("#editType3").prop('checked',true); 
			break; 
	} 
					switch(data.typeO) {
		case "0":
			 $("#editTypeO0").prop('checked',true); 
			break;
		case "1":
			$("#editTypeO1").prop('checked',true); 
			   break; 
			case "2":
			$("#editTypeO2").prop('checked',true); 
			break; 
			case "3":
			$("#editTypeO3").prop('checked',true); 
			break; 
	} 
						$("#iEditID").val(data.id);
	
					}else{
						showErrorToast(data.queryStatus);
					}
				}else{
					showErrorToast("NO DATA RETURNED");
				}
			 },
			 error:function (xhr, ajaxOptions, thrownError){
				showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
			 }
			});  
			return false;  
		}
	
	//mustra informaci�n de un convenio
	function showCategoryData(ID){
		var dataString = 'sAction=searchCategoryById&categoryID=' + ID;
		$.ajax({  
		type: "post",  
		data: dataString, 	
		dataType: "json",  
		url: "categoryABM.php",  
		success: function(data) {
			
			if(data){
				if(data.queryStatus == "OK"){
					
					$("#editType0").prop('checked',false);
					$("#editType1").prop('checked',false);
					$("#editType2").prop('checked',false);
					$("#editType3").prop('checked',false);
					
					$("#editTypeO0").prop('checked',false);
					$("#editTypeO1").prop('checked',false);
					$("#editTypeO2").prop('checked',false);
					$("#editTypeO3").prop('checked',false);
					
					$("#editCategoria").val(data.name);
					$("#editVal").val(data.value);
					
					switch(data.type) {
    case "0":
		 $("#editType0").prop('checked',true); 
        break;
    case "1":
		$("#editType1").prop('checked',true); 
		   break; 
		case "2":
		$("#editType2").prop('checked',true); 
        break; 
		case "3":
		$("#editType3").prop('checked',true); 
        break; 
} 
				switch(data.typeO) {
    case "0":
		 $("#editTypeO0").prop('checked',true); 
        break;
    case "1":
		$("#editTypeO1").prop('checked',true); 
		   break; 
		case "2":
		$("#editTypeO2").prop('checked',true); 
        break; 
		case "3":
		$("#editTypeO3").prop('checked',true); 
        break; 
} 
					$("#iEditID").val(data.id);

				}else{
					showErrorToast(data.queryStatus);
				}
			}else{
				showErrorToast("NO DATA RETURNED");
			}
		 },
		 error:function (xhr, ajaxOptions, thrownError){
			showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		 }
		});  
		return false;  
	}
	
	function showConceptAfipData(ID){
		var dataString = 'sAction=searchConceptAfipById&conceptAfipID=' + ID;
		$.ajax({  
		type: "post",  
		data: dataString, 	
		dataType: "json",  
		url: "conceptAfipSueldoABM.php",  
		success: function(data) {
			
			if(data){
				if(data.queryStatus == "OK"){
					
					$("#editConceptName").val(data.name);
					$("#editConceptDescription").val(data.description);
		
					$("#iEditID").val(data.id);

				}else{
					showErrorToast(data.queryStatus);
				}
			}else{
				showErrorToast("NO DATA RETURNED");
			}
		 },
		 error:function (xhr, ajaxOptions, thrownError){
			showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		 }
		});  
		return false;  
	}
	
	function showUserDataAdmin(ID){
		var dataString = 'sAction=searchUserById&userID=' + ID;
		$.ajax({  
		type: "post",  
		data: dataString,  
		dataType: "json",  
		url: "sysUserABM.php",  
		success: function(data) {
			if(data){
				if(data.queryStatus == "OK"){
					
					$("#editNombre").val(data.name);
					$("#editApellido").val(data.lastName);
					$("#editTelefono").val(data.phone);
					$("#editDomicilio").val(data.address);
					$("#editEmail").val(data.userName);
					$("#iEditID").val(data.id);
					$("#editPassword").val(data.password);
					 $("#editSysUsersType option").each(function(){
						if($(this).val() == data.sysUsersType){
							$(this).attr("selected","'selected'");
						}						
					});
					/*$("#editConceptName").val(data.name);
					$("#editConceptValue").val(data.value);
					$("#editConceptValue1").val(data.value1);
					$("#editConceptValue2").val(data.value2);
					$("#editConceptValue3").val(data.value3);
					$("#editConceptType option").each(function(){
						if($(this).val() == data.type){
							$(this).attr("selected","'selected'");
						}						
					});
                     $("#editConceptPropertyType option").each(function(){
						if($(this).val() == data.category){
							$(this).attr("selected","'selected'");
						}						
					});
                    $("#editConceptRadio"+data.typeValue).attr("checked","'checked'");
							$("#editConceptDataType1").removeAttr("checked");
							$("#editConceptDataType1").removeAttr("disabled");
							$("#editConceptDataType2").removeAttr("checked");
							$("#editConceptDataType2").removeAttr("disabled");
							$("#editConceptDataType3").removeAttr("checked");
							$("#editConceptDataType3").removeAttr("disabled");
							$("#editConceptDataType4").removeAttr("checked");
							$("#editConceptDataType4").removeAttr("disabled");
							
					
						if(data.dataType == 1){
							   $("#editConceptDataType1").attr("checked","'checked'");
							   $("#editConceptDataType2").attr("disabled","true");
						} else {
								   if(data.dataType == 2){
								   $("#editConceptDataType2").attr("checked","'checked'");
								   $("#editConceptDataType1").attr("disabled","true");
								   }
						}
						if(data.cargoFijo==1){
						      $("#editConceptDataType3").attr("checked","'checked'");
							  
						} else {
						 $("#editConceptDataType3").removeAttr("checked");
							  
						}
						if(data.authorized==1){
						      $("#editConceptDataType4").attr("checked","'checked'");
							  
						} else {
						 $("#editConceptDataType4").removeAttr("checked");
							  
						}
					$("#editConceptSalePoint option").each(function(){
						if($(this).val() == data.pointSale){
							$(this).attr("selected","'selected'");
						}						
					});
					var tmp = data.dischargeDate.split("-");
					$("#editConceptDischarge").val(tmp[2]+"/"+tmp[1]+"/"+tmp[0]);
					$("#editConceptStatus option").each(function(){
						if($(this).val() == data.status){
							$(this).attr("selected","'selected'");
						}						
					});*/
				}else{
					showErrorToast(data.queryStatus);
				}
			}else{
				showErrorToast("NO DATA RETURNED");
			}
		 },
		 error:function (xhr, ajaxOptions, thrownError){
			showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		 }
		});  
		return false;  
	}
	
	
function edad(Fecha,o){
var elem = o.name.split('-');
num = elem[1];
fecha = new Date(Fecha);
hoy = new Date();
ed = parseInt((hoy -fecha)/365/24/60/60/1000);

$("INPUT[name='editCargoEdad-"+num+"']").each
(
    function(index, element)
    {
    $(this).val(ed);
    }
);
}

function edadNew(Fecha,o){
var elem = o.name.split('-');
num = elem[1];
fecha = new Date(Fecha);
hoy = new Date();
ed = parseInt((hoy -fecha)/365/24/60/60/1000);

$("INPUT[name='newCargoEdad-"+num+"']").each
(
    function(index, element)
    {
    $(this).val(ed);
    }
);
}

function showServiceByUsersFiles(ID){
	var dataString = 'sAction=searchServicesByUserId&ID=' + ID;
	$.ajax({  
	type: "post",   
	data: dataString, 	
	dataType: "json",  
	url: "usersByServices.php",  
	success: function(data) {
		
		if(data){
			if(data.queryStatus == "OK"){


				$("#iEditIDC").val(data.id);
				
				$("#editCBU").val(data.cbu); 
				$("#editApellido").val(data.lastName);
				$("#editNombre").val(data.name);
				$("#editDocumento").val(data.document);
				$("#editTipoDocumento").val(data.typeDocument);
				$("#editNomenclatura").val(data.registryNumber);
				$("#editEmail").val(data.email);

				$("#editManzanaLote").val(data.apple+'.'+data.portion);
				$("#editDomicilioPostal").val(data.addressPostal);
				$("textarea#editMemo").val(data.memo);
				
				$("#editDomicilioC").val(data.address);
			
				
				
				$("#iUserIDC").val(data.idUser);
				$("#editCpe").val(data.cpe);
				$("#editAbonado").val(data.idUser);
				$("#newUserBusquedaC").val(data.Name);

				$("#editEstadoC option").each(function(){
					if($(this).val() == data.status){
						$(this).attr("selected","'selected'");
						$("#editEstadoC").val(data.status);
					}						
				});
				
				$("#editConvenioC option").each(function(){
					if($(this).val() == data.idAgreement){
						$(this).attr("selected","'selected'");
						$("#editConvenioC").val(data.idAgreement);
					}						
				});

			
			}else{
				showErrorToast(data.queryStatus);
			}
		}else{
			showErrorToast("NO DATA RETURNED");
		}
	 },
	 error:function (xhr, ajaxOptions, thrownError){
		showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
	 }
	});  
	return false;  
}

function showServiceByUsers(ID){
	var dataString = 'sAction=searchServicesByUserId&ID=' + ID;
	$.ajax({  
	type: "post",   
	data: dataString, 	
	dataType: "json",  
	url: "usersByServices.php",  
	success: function(data) {
		
		if(data){
			if(data.queryStatus == "OK"){

			//vacio las novedades asi cargo las correctas
				var tableHeaderRowCount = 1;
				var table = document.getElementById('editCargos');
				var rowCount = table.rows.length;
				for (var i = tableHeaderRowCount; i < rowCount; i++) {
					table.deleteRow(tableHeaderRowCount);
				}
				s=0;
			
//getServicesByAgreementIn(data.idAgreement,2,1);

				
				$("#iEditID").val(data.id);
				$("#editFechaBaja").val(data.fallDate);
				$("#editCuit").val(data.cuit);
				$("#editFechaAlta").val(data.activationDate);
				$("#editCBU").val(data.cbu); 
				 
				$("#editCredito").val(data.credito); 
				$("#editApellido").val(data.lastName);
				$("#editNombre").val(data.name);
				$("#editDocumento").val(data.document);
				$("#editTipoDocumento").val(data.typeDocument);
				$("#editNomenclatura").val(data.registryNumber);
				$("#editEmail").val(data.email);

				$("#editManzanaLote").val(data.apple+'.'+data.portion);
				$("#editDomicilioPostal").val(data.addressPostal);
				$("textarea#editMemo").val(data.memo);
				
				$("#editDomicilio").val(data.address);
		
				$("#iUserIDE").val(data.idUser);
				$("#editCpe").val(data.cpe);
				$("#editAbonado").val(data.idUser);
				$("#newUserBusquedaE").val(data.Name);

				$("#editEstado option").each(function(){
					if($(this).val() == data.status){
						$(this).attr("selected","'selected'");
						$("#editEstado").val(data.status);
					}						
				});
				
				$("#editConvenio option").each(function(){
					if($(this).val() == data.idAgreement){
						$(this).attr("selected","'selected'");
						$("#editConvenio").val(data.idAgreement);
					}						
				});
				
				$("#editTipoPropiedad option").each(function(){
					if($(this).val() == data.idProperty){
						$(this).attr("selected","'selected'");
						$("#editTipoPropiedad").val(data.idProperty);
					}						
				});
				
				$("#editMarca option").each(function(){
					if($(this).val() == data.marca){
						$(this).attr("selected","'selected'");
						$("#editMarca").val(data.marca);
					}						
				});
				
				$("#editBarrio option").each(function(){
					if($(this).val() == data.neighborhood){
						$(this).attr("selected","'selected'");
						$("#editBarrio").val(data.neighborhood);
					}						
				});
				
			//$test = getServicesByAgreementInAll(data.idAgreement,2,1);
			  var test = 1;
			
				if (data.services.length>0){
					data.services[g].push({'generation':1});
					if (test == 1){
						
						for (g = 0; g < data.services.length; g++) {
							
							nTab    = $("#editFolder-nav-tabs li")[0].clone().attr('class','');
							nCntTab = $("#cont-tab-").clone();
							nTbL = $("#editCargos").clone();
							nBox = $("#-box").clone().css("display","");
							nTr = $("#descripcionEdit").clone();
							nAM = $("#actaMatrimonioEdit-").clone().attr("id",$(this).attr("id")+s).css("display","");
							nAD = $("#actaDefuncionEdit-").clone().attr("id",$(this).attr("id")+s).css("display","");
							nAC = $("#actaCneEdit-").clone().attr("id",$(this).attr("id")+s).css("display","");
							++s;
						 	if (!$("#cont-tab-"+data.services[g]['generation']).length)
							 	nCntTab.attr( 'id', "cont-tab-"+data.services[g]['generation']).after( $(".tabcontent").last() );

              switch (data.services[g]['tipo']) {
							 case '1':
							 	if( !$("#"+data.services[g]['generation']).find("#born-box").length )
								 	nBox.append( "#cont-tab-"+data.services[g]['generation'] );

						 		nAN = $("#actaNacimientoEdit-").clone().attr("id",$(this).attr("id")+s).css("display","");
							 	nTbL.attr('id', 'born-'+nTbL.attr("id"));//typedoc-editCargos
              	nTr.attr('id', $("#descripcionEdit").attr("id")+s).append(nTbL);
              	nAN.append(nTr);
								
							 break;
							 case '2':
							 	ndTbL.attr('id', 'maried-'+ndTbL.attr("id"));
								$( "#actaMatrimonioEdit-"+s ).css("display","none");
							 break;
							 case '3':
							 	ndTbL.attr('id', 'dead-'+ndTbL.attr("id"));
								$( "#actaDefuncionEdit-"+s ).css("display","none");
							 break;
							case '4':
								ndTbL.attr('id', 'cne-'+ndTbL.attr("id"));
								$( "#actaCneEdit-"+s ).css("display","");
							 break;
							 default:
							 	/*
								 	$( "#actaNacimientoEdit-"+s ).css("display","none");
									$( "#actaMatrimonioEdit-"+s ).css("display","none");
									$( "#actaDefuncionEdit-"+s ).css("display","none");
									$( "#actaCneEdit-"+s ).css("display","none");
							 	*/
							 	/* funtion from backend interted in footer by js tags*/
 								//funtion();
							 break;	
							}
						
							$("#descripcionEdit").clone().attr('id', $("#descripcionEdit").attr("id")+(++s)).append("#editCargos");
							$("#descripcionEdit"+s).css("display","");

							$("#descripcionEdit"+s+" select" ).each(function(index){
								$(this).attr("name",$(this).attr("name")+s);
								$(this).val(data.services[g]['tipo']);
								
								$(this).attr("selected","'selected'");
								if($(this).val() == data.services[g]['tipo']){
									$(this).attr("selected","'selected'");
								}							
							});
							
							$("#descripcionEdit"+s+" div" ).each(function(index){
								$(this).attr("id",$(this).attr("id")+s);
								//alert($(this).attr("name"));
							});
							//default documents
							
							//default documents
							switch (data.services[g]['tipo']) {
							 case '1':
								$( "#actaNacimientoEdit-"+s ).css("display","");
								$( "#actaMatrimonioEdit-"+s ).css("display","none");
								$( "#actaDefuncionEdit-"+s ).css("display","none");								
							 break;
							 case '2':
								$( "#actaNacimientoEdit-"+s ).css("display","none");
								$( "#actaMatrimonioEdit-"+s ).css("display","");
								$( "#actaDefuncionEdit-"+s ).css("display","none");
							 break;
							 case '3':
								$( "#actaNacimientoEdit-"+s ).css("display","none");
								$( "#actaMatrimonioEdit-"+s ).css("display","none");
								$( "#actaDefuncionEdit-"+s ).css("display","");
							 break;
							 default:
								$( "#actaNacimientoEdit-"+s ).css("display","none");
								$( "#actaMatrimonioEdit-"+s ).css("display","none");
								$( "#actaDefuncionEdit-"+s ).css("display","none");
							 break;	
							}
							
							$("#descripcionEdit"+s+" input" ).each(function(index){
								//alert(this);
								$(this).attr("name",$(this).attr("name")+s);
								//$('input[name=editCargo-]'+s).val(data.services[g]['news']);
								//$('input[name=editCargoTipo-]'+s).val(data.services[g]['type']);
								$("#editServicio-"+s).val(data.services[g]['tipo']);
								//acta nacimiento
								$("#editNombreActa-"+s).val(data.services[g]['nombreActa']);
								$("#editApellidoActa-"+s).val(data.services[g]['apellidoActa']);
								$("#editLugarNacimientoActa-"+s).val(data.services[g]['lugarActa']);
								$("#editProvinciaActa-"+s).val(data.services[g]['provinciaActa']);
								$("#editPaisActa-"+s).val(data.services[g]['paisActa']);
								$("#editFechaActa-"+s).val(data.services[g]['fechaActa']);
								$("#editNacionalidadActa-"+s).val(data.services[g]['nacionalidadActa']);
								$("#editDniActa-"+s).val(data.services[g]['dniActa']);
								$("#editNomPadreActa-"+s).val(data.services[g]['padreActa']);
								$("#editApePadreActa-"+s).val(data.services[g]['padreApeActa']);
								$("#editNomMadreActa-"+s).val(data.services[g]['madreActa']);
								$("#editApeMadreActa-"+s).val(data.services[g]['madreApeActa']);
								$("#editNomAbueloActa-"+s).val(data.services[g]['abueloActa']);
								$("#editApeAbueloActa-"+s).val(data.services[g]['abueloApeActa']);
								$("#editNomAbuelaActa-"+s).val(data.services[g]['abuelaActa']);
								$("#editApeAbuelaActa-"+s).val(data.services[g]['abuelaApeActa']);							
							});
							
							
								$("INPUT[name='editNomAbueloActa-"+s+"']").each(function(index, element){
									$(this).val(data.services[g]['abueloActa']);
								});
								$("INPUT[name='editApeAbueloActa-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['abueloApeActa']);
								}
								);

								$("INPUT[name='editNomAbuelaActa-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['abuelaActa']);
								}
								);

								$("INPUT[name='editApeAbuelaActa-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['abuelaApeActa']);
								}
								);


								//acata nacimiento
								$("INPUT[name='editApeMadreActa-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['madreApeActa']);
								}
								);

								$("INPUT[name='editNomMadreActa-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['madreActa']);
								}
								);

								$("INPUT[name='editApePadreActa-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['padreApeActa']);
								}
								);


								$("INPUT[name='editNomPadreActa-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['padreActa']);
								}
								);

								$("INPUT[name='editDniActa-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['dniActa']);
								}
								);

								$("INPUT[name='editNacionalidadActa-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['nacionalidadActa']);
								}
								);

								$("INPUT[name='editFechaActa-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['fechaActa']);
								}
								);

								$("INPUT[name='editPaisActa-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['paisActa']);
								}
								);


								$("INPUT[name='editNombreActa-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['nombreActa']);
								}
								);

								$("INPUT[name='editApellidoActa-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['apellidoActa']);
								}
								);


								$("INPUT[name='editLugarNacimientoActa-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['lugarActa']);
								}
								);

								$("INPUT[name='editProvinciaActa-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['provinciaActa']);
								}
								);

								//fin acata

								//acta matrimonio

								$("INPUT[name='editNomMadreEsposoEsMatri-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['madreEsposoEsMatri']);
								}
								);


								$("INPUT[name='editApeMadreEsposoEsMatri-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['madreApeEsposoEsMatri']);
								}
								);



								$("INPUT[name='editNomPadreEsposoEsMatri-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['padreEsposoEsMatri']);
								}
								);



								$("INPUT[name='editApePadreEsposoEsMatri-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['padreApeEsposoEsMatri']);
								}
								);


								$("INPUT[name='editNacionalidadEsMatri-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['nacionalidadEsMatri']);
								}
								);


								$("INPUT[name='editDniEsMatri-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['dniEsMatri']);
								}
								);

								$("INPUT[name='editEdadEsMatri-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['edadEsMatri']);
								}
								);


								$("INPUT[name='editFechaNaciEsMatri-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['fechaNaciEsMatri']);
								}
								);


								$("INPUT[name='editLugarNacimientoEsMatri-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['lugarEsMatri']);
								}
								);



								$("INPUT[name='editNombreEsMatri-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['nomnombreEsMatri']);
								}
								);



								$("INPUT[name='editApellidoEsMatri-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['nomapellidoEsMatri']);
								}
								);



								$("INPUT[name='editNomMadreEsposoMatri-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['madreEsposoMatri']);
								}
								);




								$("INPUT[name='editApeMadreEsposoMatri-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['madreApeEsposoMatri']);
								}
								);


								$("INPUT[name='editNomPadreEsposoMatri-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['padreEsposoMatri']);
								}
								);


								$("INPUT[name='editApePadreEsposoMatri-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['padreApeEsposoMatri']);
								}
								);


								$("INPUT[name='editNacionalidadMatri-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['nacionalidadMatri']);
								}
								);



								$("INPUT[name='editDniMatri-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['dniMatri']);
								}
								);


								$("INPUT[name='editEdadMatri-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['edadMatri']);
								}
								);


								$("INPUT[name='editFechaNaciMatri-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['fechaNaciMatri']);
								}
								);


								$("INPUT[name='editLugarNacimientoMatri-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['lugarNaciMatri']);
								}
								);

								$("INPUT[name='editNombreMatri-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['nombreMatri']);
								}
								);

								$("INPUT[name='editApellidoMatri-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['nomapellidoMatri']);
								}
								);

								$("INPUT[name='editPaisMatri-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['paisMatri']);
								}
								);



								$("INPUT[name='editFechaMatri-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['fechaMatri']);
								}
								);


								$("INPUT[name='editLugarMatri-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['lugarMatri']);
								}
								);
								//fin matrimonio



								//acta defuncion
								$("INPUT[name='editNomMadreDefu-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['madreDefu']);
								}
								);

								$("INPUT[name='editApeMadreDefu-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['madreApeDefu']);
								}
								);


								$("INPUT[name='editNomPadreDefu-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['padreDefu']);
								}
								);


								$("INPUT[name='editApePadreDefu-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['padreApeDefu']);
								}
								);



								$("INPUT[name='editProfeDefu-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['profeDefu']);
								}
								);

								$("INPUT[name='editNacionalidadDefu-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['nacionalidadDefu']);
								}
								);

								$("INPUT[name='editDniDefu-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['dniDefu']);
								}
								);

								$("INPUT[name='editLugarNacimientoDefu-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['lugarNaciDefu']);
								}
								);



								$("INPUT[name='editFechaNaciDefu-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['fechaNaciDefu']);
								}
								);


								$("INPUT[name='editNombreApeDefu-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['nombreApellidoDefu']);
								}
								);


								$("SELECT[name='editEstadoDefu-"+s+"']").each
								(
								function(index, element)
								{
								$(this).select(data.services[g]['estadoDefu']);
								$(this).val(data.services[g]['estadoDefu']);
								}
								);

								$("INPUT[name='editEstadoDefu-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['estadoDefu']);
								}
								);





								$("INPUT[name='editFechaDefu-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['fechaDefu']);
								}
								);

								$("INPUT[name='editLugarDefuDefu-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['lugarDefu']);
								}
								);




								$("INPUT[name='editPaisDefu-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['paisDefu']);
								}
								);


								$("INPUT[name='editApellidoDefu-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['apellidoDefu']);
								}
								);


								$("INPUT[name='editNombreDefu-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['nombreDefu']);
								}
								);
								//fin acata defuncion



								$("SELECT[name='editServicio-"+s+"']").each
								(
								function(index, element)
								{
								$(this).select(data.services[g]['tipo']);
								$(this).val(data.services[g]['tipo']);
								}
								);

								$("INPUT[name='editServicio-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['tipo']);
								}
								);



								$("SELECT[name='editConceptDataType8-"+s+"']").each
								(
								function(index, element)
								{
								$(this).select(data.services[g]['category']);
								$(this).val(data.services[g]['category']);
								}
								);

								$("INPUT[name='editConceptDataType8-"+s+"']").each
								(
								function(index, element)
								{
								$(this).val(data.services[g]['category']);
								}
								);


								$("INPUT[name='cantEditCargo]").each
								(
								function(index, element)
								{
								$(this).val(s);
								}
								);

								document.getElementById("cantEditCargo").value = s;
							}
					}
				} 
			
			}else{
				showErrorToast(data.queryStatus);
			}
		}else{
			showErrorToast("NO DATA RETURNED");
		}
	 },
	 error:function (xhr, ajaxOptions, thrownError){
		showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
	 }
	});  
	return false;  
}	

	 
//mustra informaci�n de un colegio	 
	 	function showPersonalBySchool(ID){
		var dataString = 'sAction=searchPersonalBySchoolId&pBsID=' + ID;
		$.ajax({  
		type: "post",   
		data: dataString, 	
		dataType: "json",  
		url: "personalBySchool.php",  
		success: function(data) {
			
			if(data){
				if(data.queryStatus == "OK"){

				//vacio las novedades asi cargo las correctas
					var tableHeaderRowCount = 1;
					var table = document.getElementById('editCargos');
					var rowCount = table.rows.length;
					for (var i = tableHeaderRowCount; i < rowCount; i++) {
					    table.deleteRow(tableHeaderRowCount);
					}
					s=0;
				
					$("#editHoras").val(data.hour);
					$("#editCaja0").prop('checked',false);
					$("#editCaja1").prop('checked',false);
					$("#editSalario0").prop('checked',false);
					$("#editSalario1").prop('checked',false);
					
					$("#editPorcentaje").val(data.salaryValue);
					$("#iEditID").val(data.id);
					$("#editAntiguedad").val(data.antiquity);
					$("#editAntiguedadM").val(data.antiquityM);
					$("#editAntiguedadR").val(data.antiquityR);
					$("#editAntiguedadMR").val(data.antiquityMR); 
					$("#editFechaAlta").val(data.highDate);
					$("#editFechaBaja").val(data.endDate);
					$("#editFechaIPS").val(data.dateIPS);
					$("#editFechaDocencia").val(data.dateTeacher);
					$("#editInactivas").val(data.inactive);
					$("#editLegajo").val(data.leg);
					$("#editSecuencia").val(data.sequence);
					$("#editAfectacion").val(data.affectation);
					$("textarea#editMemo").val(data.memo);
					
					$("#iSchoolIDE").val(data.school);
					$("#iAgreementIDE").val(data.agreement);
					$("#iChargeIDE").val(data.charge);
					$("#iLevelIDE").val(data.activities);
					$("#iTypeLiquidationIDE").val(data.liquidationType);
				
					getConceptsByCharges(data.charge,data.agreement,data.school,data.activities,data.liquidationType,'2',data.personal,data.id);
					
					$("#iUserIDE").val(data.personal);
					$("#newEmpleadoBusquedaE").val(data.Name);

					$("#editTipo option").each(function(){
						if($(this).val() == data.type){
							$(this).attr("selected","'selected'");
						    $("#editTipo").val(data.type);
						}						
					});
					
					$("#editTipoAsign option").each(function(){
						if($(this).val() == data.typeAsign){
							$(this).attr("selected","'selected'");
						    $("#editTipoAsign").val(data.typeAsign);
						}						
					});
					
					$("#editConvenio option").each(function(){
						if($(this).val() == data.agreement){
							$(this).attr("selected","'selected'");
							$("#editConvenio").val(data.agreement);
						}						
					});
					
					getChargesByAgreementIn(data.agreement,2,data.charge);
					
					

						$("#editCargo option").each(function(){
						if($(this).val() == data.charge){
							$(this).attr("selected","'selected'");
							$("#editCargo").val(data.charge);
						}						
					});
					
					
					
					$("#editEmpleador option").each(function(){
						if($(this).val() == data.school){
							$(this).attr("selected","'selected'");
							$("#editEmpleador").val(data.school);
						}						
					});
					
					getNiveles(data.school,2,data.activities);
					
					$("#editStatus option").each(function(){
						if($(this).val() == data.status){
							$(this).attr("selected","'selected'");
							$("#editStatus").val(data.status);
						}						
					});
					
					$("#editTipoLiquidacion option").each(function(){
						if($(this).val() == data.liquidationType){
							$(this).attr("selected","'selected'");
							$("#editTipoLiquidacion").val(data.liquidationType);
						}						
					});
					
					if(data.salary == 1){
							  $("#editSalario1").prop('checked',true); 
						} else {
								     $("#editSalario0").prop('checked',true); 		 	
						}
						
					$("#editSalario").val(data.salary);
					if(data.box == 1){
							  $("#editCaja1").prop('checked',true); 
						} else {
								     $("#editCaja0").prop('checked',true); 		 	
						}
					$("#editCaja").val(data.box);
															
					if (data.charges.length>0){
					for (g = 0; g < data.charges.length; g++) {
					$( "#descripcionEdit" ).clone().attr('id', $( "#descripcionEdit" ).attr("id")+(++s)).appendTo( "#editCargos" );
					$( "#descripcionEdit"+s ).css("display","");

					$("#descripcionEdit"+s+" select" ).each(function(index){
					$(this).attr("name",$(this).attr("name")+s);
					$(this).val(data.charges[g]['news']);
					$(this).attr("selected","'selected'");
					if($(this).val() == data.charges[g]['news']){
					$(this).attr("selected","'selected'");
					}						

					});

					$("#descripcionEdit"+s+" input" ).each(function(index){
					//alert(this);
					$(this).attr("name",$(this).attr("name")+s);
					//$('input[name=editCargo-]'+s).val(data.charges[g]['news']);
					//$('input[name=editCargoTipo-]'+s).val(data.charges[g]['type']);
					$("#editCargo-"+s).val(data.charges[g]['news']);
					$("#editCargoTipo-"+s).val(data.charges[g]['news']);
					$("#editCargoDevo-"+s).val(data.charges[g]['typeUp']);
					$("#editCargoDesde-"+s).val(data.charges[g]['dateBegin']);
					$("#editCargoHasta-"+s).val(data.charges[g]['dateEnd']);
					$("#editCargoDias-"+s).val(data.charges[g]['value']);
					$("#editCargoMoti-"+s).val(data.charges[g]['reason']);

					});

					//alert(data.charges[g]['dateBegin']);

					$("SELECT[name='editCargoTipo-"+s+"']").each
					(
					function(index, element)
					{
					$(this).select(data.charges[g]['type']);
					$(this).val(data.charges[g]['type']);
					}
					);

					$("SELECT[name='editCargoDevo-"+s+"']").each
					(
					function(index, element)
					{
					$(this).select(data.charges[g]['typeUp']);
					$(this).val(data.charges[g]['typeUp']);
					}
					);

					$("INPUT[name='editCargoDesde-"+s+"']").each
					(
					function(index, element)
					{
					$(this).val(data.charges[g]['dateBegin']);
					}
					);

					$("INPUT[name='editCargoHasta-"+s+"']").each
					(
					function(index, element)
					{
					$(this).val(data.charges[g]['dateEnd']);
					}
					);


					$("INPUT[name='editCargoDias-"+s+"']").each
					(
					function(index, element)
					{
					$(this).val(data.charges[g]['value']);
					}
					);



					$("INPUT[name='editCargoMoti-"+s+"']").each
					(
					function(index, element)
					{
					$(this).val(data.charges[g]['reason']);
					}
					);

					$("INPUT[name='editCargo-"+s+"']").each
					(
					function(index, element)
					{
					$(this).val(data.charges[g]['news']);
					}
					);

					$("INPUT[name='editCargoDevo-"+s+"']").each
					(
					function(index, element)
					{
					$(this).val(data.charges[g]['typeUp']);
					}
					);


					$("INPUT[name='cantEditCargo]").each
					(
					function(index, element)
					{
					$(this).val(s);
					}
					);


					document.getElementById("cantEditCargo").value = s;




					}

					} 


					}else{
					showErrorToast(data.queryStatus);
					}
					}else{
					showErrorToast("NO DATA RETURNED");
					}
					},
					error:function (xhr, ajaxOptions, thrownError){
					showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
					}
					});  
					return false;  
	}	

//cargos por convenios

function showChargesByAgreementData(ID){
		var dataString = 'sAction=searchCAById&caID=' + ID;
		$.ajax({  
		type: "post",  
		data: dataString,  
		dataType: "json",  
		url: "chargesByAgreement.php",  
		success: function(data) {
			if(data){
				if(data.queryStatus == "OK"){
					
					$("#iEditID").val(data.id);
					
					$("#editCargo option").each(function(){
						if($(this).val() == data.charge){
							$(this).attr("selected","'selected'");
							$("#editCargo").val(data.charge).trigger("change");
							
						}						
					});
					
                     $("#editConvenio option").each(function(){
						if($(this).val() == data.agreement){
							$(this).attr("selected","'selected'");
							$("#editConvenio").val(data.agreement).trigger("change");
						}						
					});
					
					  $("#editCategoria option").each(function(){
						if($(this).val() == data.category){
							$(this).attr("selected","'selected'");
							$("#editCategoria").val(data.category).trigger("change");
						}						
					});
					
					getChargesByAgreement(data.agreement,2,data.concept);
					
                   /* $("#editConceptRadio"+data.typeValue).attr("checked","'checked'");
							$("#editConceptDataType1").removeAttr("checked");
							$("#editConceptDataType1").removeAttr("disabled");
							$("#editConceptDataType2").removeAttr("checked");
							$("#editConceptDataType2").removeAttr("disabled");
							$("#editConceptDataType3").removeAttr("checked");
							$("#editConceptDataType3").removeAttr("disabled");
							$("#editConceptDataType4").removeAttr("checked");
							$("#editConceptDataType4").removeAttr("disabled");
							
					
						if(data.dataType == 1){
							   $("#editConceptDataType1").attr("checked","'checked'");
							   $("#editConceptDataType2").attr("disabled","true");
						} else {
								   if(data.dataType == 2){
								   $("#editConceptDataType2").attr("checked","'checked'");
								   $("#editConceptDataType1").attr("disabled","true");
								   }
						}
						if(data.cargoFijo==1){
						      $("#editConceptDataType3").attr("checked","'checked'");
							  
						} else {
						 $("#editConceptDataType3").removeAttr("checked");
							  
						}
						if(data.authorized==1){
						      $("#editConceptDataType4").attr("checked","'checked'");
							  
						} else {
						 $("#editConceptDataType4").removeAttr("checked");
							  
						}
					$("#editConceptSalePoint option").each(function(){
						if($(this).val() == data.pointSale){
							$(this).attr("selected","'selected'");
						}						
					});
					var tmp = data.dischargeDate.split("-");
					$("#editConceptDischarge").val(tmp[2]+"/"+tmp[1]+"/"+tmp[0]);
					$("#editConceptStatus option").each(function(){
						if($(this).val() == data.status){
							$(this).attr("selected","'selected'");
						}						
					});*/
				}else{
					showErrorToast(data.queryStatus);
				}
			}else{
				showErrorToast("NO DATA RETURNED");
			}
		 },
		 error:function (xhr, ajaxOptions, thrownError){
			showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		 }
		});  
		return false;  
	}
	
	
		function searchPersonalByName(sName,phpFile,tableName,userIdField,userNameField){
		var dataString = 'sAction=searchPersonalByName&userName=' + sName;
		$.ajax({
		type: "post",  
		data: dataString,
		dataType: "json",  
		url: phpFile,  
		success: function(data) {
			var rows ="<tr><td class='headerResultados' colspan='3' style='background-color:#A0D37D;'><b style='color:black;'>Posibles Personas</b></td></tr>";
			if(data){
				if(data.status != "ERROR"){
					$("#"+tableName).css("display","");
					if(data.length == 1){
						$("#"+userIdField).val(data[0].userID);
						//$("#usersFounded").css("display","none");
					}
					for(var i in data){
						rows+="<tr style='cursor:pointer' onclick='setPersonalID("+data[i].userID+",\""+data[i].userLastName+" , "+data[i].userName+" CUIL: "+data[i].userCuit+"\",\""+tableName+"\",\""+userIdField+"\",\""+userNameField+"\",\""+data[i].userCuit+"\");'><td width='150'>"+data[i].userLastName+"</td><td width='150'>"+data[i].userName+"</td><td width='150'>"+" CUIL: "+data[i].userCuit+"</td></tr>";
//rows+="<tr style='cursor:pointer'><select onchange=onclick='setUserID("+data[i].userID+",\""+data[i].userLastName+" "+data[i].userName+"\",\""+tableName+"\",\""+userIdField+"\",\""+userNameField+"\",\""+data[i].userCategory+"\");'><option value="+data[i].userID+">"+data[i].userLastName+" "+data[i].userName+"</option></select></tr>";
					
					
					}
					$("#"+tableName).html(rows);
				}else{
					showErrorToast(data.message);
				}
			}else{
				$("#"+userIdField).val("-1");
				$("#"+tableName).css("display","none");
			}
		},
		error:function (xhr, ajaxOptions, thrownError){
				showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		}
		});				
	}
	
	
	function setPersonalID(iID,sUserName,tableName,userIdField,userNameField,userCategory){
		$("#"+userIdField).val(iID);
		$("#"+userNameField).val(sUserName);
		$("#"+tableName).css("display","none");
	}
	
	
	
	
 	$("#newEmpleadoBusqueda").focusout(function(){
		if($("#iUserID").val() == "" || parseInt($("#iUserID").val()) == 0){
			if($(this).val() != ""){
				searchPersonalByName($(this).val(),"personalABM.php","usersFounded","iUserID","newEmpleadoBusqueda");
			}else{
				$("#usersFounded").css("display","none");
			}
		}
	});

	$("#newEmpleadoBusqueda").keyup(function(){
		$("#iUserID").val("-1");
		if($(this).val() != ""){
			searchPersonalByName($(this).val(),"personalABM.php","usersFounded","iUserID","newEmpleadoBusqueda");
		}else{
			$("#usersFounded").css("display","none");
		}
	});
	
	
		$("#newEmpleadoBusquedaE").focusout(function(){
		if($("#iUserIDE").val() == "" || parseInt($("#iUserIDE").val()) == 0){
			if($(this).val() != ""){
				searchPersonalByName($(this).val(),"personalABM.php","usersFoundedE","iUserIDE","newEmpleadoBusquedaE");
			}else{
				$("#usersFoundedE").css("display","none");
			}
		}
	});

	$("#newEmpleadoBusquedaE").keyup(function(){
		$("#iUserIDE").val("-1");
		if($(this).val() != ""){
			searchPersonalByName($(this).val(),"personalABM.php","usersFoundedE","iUserIDE","newEmpleadoBusquedaE");
		}else{
			$("#usersFoundedE").css("display","none");
		}
	});



	function searchUsersByName(sName,phpFile,tableName,userIdField,userNameField){
		var dataString = 'sAction=searchUsersByName&userName=' + sName;
		$.ajax({
		type: "post",  
		data: dataString,
		dataType: "json",  
		url: phpFile,  
		success: function(data) {
			var rows ="<tr><td class='headerResultados' colspan='3' style='background-color:#A0D37D;'><b style='color:black;'>Posibles Usuarios</b></td></tr>";
			//console.log(data);
			if(data){
				if(data.status != "ERROR"){
					$("#"+tableName).css("display","");
					if(data.length == 1){
						$("#"+userIdField).val(data[0].userID);
						//$("#usersFounded").css("display","none");
					}
					for(var i in data){
						rows+="<tr style='cursor:pointer' onclick='setUserID("+data[i].userID+",\""+data[i].userLastName+" , "+data[i].userName+" DNI/CUIT: "+data[i].userDNI+"\",\""+tableName+"\",\""+userIdField+"\",\""+userNameField+"\",\""+data[i].userCuit+"\");'><td width='150'> CLIENTE: "+data[i].userID+"</td><td width='150'>"+data[i].userLastName+"</td><td width='150'>"+data[i].userName+"</td><td width='150'>"+" DNI/CUIT: "+data[i].userDNI+"</td></tr>";
//rows+="<tr style='cursor:pointer'><select onchange=onclick='setUserID("+data[i].userID+",\""+data[i].userLastName+" "+data[i].userName+"\",\""+tableName+"\",\""+userIdField+"\",\""+userNameField+"\",\""+data[i].userCategory+"\");'><option value="+data[i].userID+">"+data[i].userLastName+" "+data[i].userName+"</option></select></tr>";
					}
					$("#"+tableName).html(rows);
				}else{
					showErrorToast(data.message);
				}
			}else{
				$("#"+userIdField).val("-1");
				$("#"+tableName).css("display","none");
			}
		},
		error:function (xhr, ajaxOptions, thrownError){
				showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		}
		});				
	}
	
	
	
	function setUserID(iID,sUserName,tableName,userIdField,userNameField,userCategory){
		$("#"+userIdField).val(iID);
		$("#"+userNameField).val(sUserName);
		$("#"+tableName).css("display","none");
	}


/*
era de bill desde
*/


function mandar(na){
	var frm = document.getElementById("frmSearchSummary");
	
	frm.submit();
	setTimeOut(function(){
	var frms = document.getElementById("frmsearch");
	frms.submit();
	},1000);
	return false;
	}
	
	function debListSend(){
	
	$param=document.getElementById("param").value;
	$total=document.getElementById("total").value;
	window.open('debLetterList.php?total='+$total+'&+param='+$param,'_blank');
	}
	
	/*function debListEmail(){
	$param=document.getElementById("param").value;
	//$total=document.getElementById("total").value;
	window.open('copyPrintingByEmail.php?param='+$param,'_blank');
	}*/
	
	function debListWorkEmail(){
	$param=document.getElementById("param").value;
	//$total=document.getElementById("total").value;
	window.open('debtSendEmail.php?param='+$param+'&email=0','_blank');
	}
	
	function debListWorkEmailSend(){
	$param=document.getElementById("param").value;
	//$total=document.getElementById("total").value;
	window.open('debtSendEmail.php?param='+$param+'&email=1','_blank');
	}
	
	function debListDebitEmail(){
	$param=document.getElementById("param").value;
	//$total=document.getElementById("total").value;
	window.open('debtDebitSendEmail.php?param='+$param+'&email=0','_blank');
	}
	
	function debListDebitEmailSend(){
	$param=document.getElementById("param").value;
	//$total=document.getElementById("total").value;
	window.open('debtDebitSendEmail.php?param='+$param+'&email=1','_blank');
	}
	
	
	
	var c=0;
	var tra=0;
	function validar(obj){
	if(obj.value==5){
	
	$("#agregar").css("display","");
	
	
	} else {
	$("#agregar").css("display","none");
	for (i=1;i<=c;i++) {
						$( "#descripcion"+i ).remove();
						} 
	c=0;
	
	}
	
	
	
	
	}
	
	function agregar_Cheque(){



$( "#descripcion" ).clone().attr('id', $( "#descripcion" ).attr("id")+(++c)).appendTo( "#agregarCheques" );

$( "#descripcion"+c ).css("display","");

$("#descripcion"+c+" input" ).each(function(index){

	$(this).attr("name",$(this).attr("name")+c);

	//alert($(this).attr("name"));

});



$("#descripcion"+c+" select" ).each(function(index){

	$(this).attr("name",$(this).attr("name")+c);

});





$("INPUT[name='cantCheques]").each

(

    function(index, element)

    {

    $(this).val(c);

    }

);

document.getElementById("cantCheques").value = c;



}



	var pa=0;
	

	function agregar_ChequePago(){



$( "#descripcionNext" ).clone().attr('id', $( "#descripcionNext" ).attr("id")+(++pa)).appendTo( "#agregarChequesNext" );

$( "#descripcionNext"+pa ).css("display","");

$("#descripcionNext"+pa+" input" ).each(function(index){

	$(this).attr("name",$(this).attr("name")+pa);

	//alert($(this).attr("name"));

});



$("#descripcionNext"+pa+" select" ).each(function(index){

	$(this).attr("name",$(this).attr("name")+pa);

});





$("INPUT[name='cantChequesNext]").each

(

    function(index, element)

    {

    $(this).val(pa);

    }

);

document.getElementById("cantChequesNext").value = pa;



}








function agregar_ChequeT(){



$( "#descripcionT" ).clone().attr('id', $( "#descripcionT" ).attr("id")+(++tra)).appendTo( "#agregarChequesT" );

$( "#descripcionT"+tra ).css("display","");

$("#descripcionT"+tra+" input" ).each(function(index){

	$(this).attr("name",$(this).attr("name")+tra);

	//alert($(this).attr("name"));

});



$("#descripcionT"+tra+" select" ).each(function(index){

	$(this).attr("name",$(this).attr("name")+tra);

});







$("INPUT[name='cantChequesT]").each

(

    function(index, element)

    {

    $(this).val(tra);

    }

);

document.getElementById("cantChequesT").value = tra;



}



var d=0;

function validarNext(obj){

if(obj.value==5){



$("#agregarNext").css("display","");





} else {

$("#agregarNext").css("display","none");

for (i=1;i<=d;i++) {

                    $( "#descripcionNext"+i ).remove();

					} 

d=0;



}



}
	
	var d=0;
	function validarNext(obj){
	if(obj.value==5){
	
	$("#agregarNext").css("display","");
	
	
	} else {
	$("#agregarNext").css("display","none");
	for (i=1;i<=d;i++) {
						$( "#descripcionNext"+i ).remove();
						} 
	d=0;
	
	}
	
	}
	
	
	function agregar_ChequeNext(){
	
	$( "#descripcionNext" ).clone().attr('id', $( "#descripcionNext" ).attr("id")+(++d)).appendTo( "#agregarChequesNext" );
	$( "#descripcionNext"+d ).css("display","");
	$("#descripcionNext"+d+" input" ).each(function(index){
		$(this).attr("name",$(this).attr("name")+d);
		//alert($(this).attr("name"));
	});
	}
	
	
	
		
	function agregar_ChequeNextPago(){
	
	$( "#descripcionNextPago" ).clone().attr('id', $( "#descripcionNextPago" ).attr("id")+(++d)).appendTo( "#agregarChequesNextPago" );
	$( "#descripcionNextPago"+d ).css("display","");
	$("#descripcionNextPago"+d+" input" ).each(function(index){
		$(this).attr("name",$(this).attr("name")+d);
		//alert($(this).attr("name"));
	});
	}
	
	
	
	
	var lastValue ="";
		$("#newMovementsMeterSerial").focusout(function(){
			if(lastValue != $(this).val()){
				searchMovementsByMeter($(this).val());
				lastValue=$(this).val();
			}
		});
		$("#newMovementsMeterSerial").keyup(function(){
			if(lastValue != $(this).val()){
				searchMovementsByMeter($(this).val());
				lastValue=$(this).val();
			}
		});
	
	
	function enviar(){
	$("#sAction").val("search");
	$("#iMinLimit").val("0");
	$("#rPerPage").val("30");
	$("#iPage").val("1");
	
	}




	var paNext=0;



	function agregar_PagosNext(n){



$( "#pagosNext" ).clone().attr('id', $( "#pagosNext" ).attr("id")+(++paNext)).appendTo( "#pagosAltaNext" );

$( "#pagosNext"+paNext ).css("display","");



$("#pagosNext"+paNext+" select" ).each(function(index){

	$(this).attr("name",$(this).attr("name")+paNext);

	$(this).attr("selected","'selected'",n);

	$(this).val(n);

				

								

					

	

});

$("#pagosNext"+paNext+" input" ).each(function(index){

	$(this).attr("name",$(this).attr("name")+paNext);

	//alert($(this).attr("name"));

});





$("INPUT[name='cantPagosNext]").each

(

    function(index, element)

    {

    $(this).val(paNext);

    }

);

document.getElementById("cantPagosNext").value = paNext;



}



	
	
	
	var pa=0;



	function agregar_Pagos(n){



$( "#pagos" ).clone().attr('id', $( "#pagos" ).attr("id")+(++pa)).appendTo( "#pagosAlta" );

$( "#pagos"+pa ).css("display","");



$("#pagos"+pa+" select" ).each(function(index){

	$(this).attr("name",$(this).attr("name")+pa);

	$(this).attr("selected","'selected'",n);

	$(this).val(n);

				

								

					

	

});

$("#pagos"+pa+" input" ).each(function(index){

	$(this).attr("name",$(this).attr("name")+pa);

	//alert($(this).attr("name"));

});





$("INPUT[name='cantPagos]").each

(

    function(index, element)

    {

    $(this).val(pa);

    }

);

document.getElementById("cantPagos").value = pa;



}
	
	

	
	$("input[type=checkbox]").click(function(){
		var habilitar = -1;
		$("input[type=checkbox]").each(function(index){
			if (this.checked){
				habilitar = index;
			}
		});
		
		if(habilitar != -1){
			$("#btn_enviar").removeAttr("disabled");
		}else{
			$("#btn_enviar").attr("disabled","disabled");
		}
	});
	
	
function showSummaryPayment(){
	
				var checkBoxes = document.getElementsByTagName('input');
			var total=0;
					//alert(checkBoxes.length);
							var param="";
							for (var counter=0; counter < checkBoxes.length; counter++) {
											if (checkBoxes[counter].type.toUpperCase()=='CHECKBOX' && checkBoxes[counter].checked == true) {
															
															param += checkBoxes[counter].value + ",";
															total += parseFloat(document.getElementById("td_bill_"+checkBoxes[counter].value).innerHTML);
											}
							}
			param = param.substr(0,param.length-1);
			
			nombre =document.getElementById("userBillingUser").value;
			
				
		document.getElementById('nombre').value = nombre;
        document.getElementById('param').value = param;
        document.getElementById('total').value = Math.round(total*100)/100;	

	}
	
	
	
	
	
	function showSummaryPaymentNext(){
		nombre =document.getElementById("userBillingUser").value;
		iUserID =document.getElementById("iUserID").value;	
		document.getElementById('nombreNext').value = nombre;
        document.getElementById('iUserIDNext').value = iUserID;
      

	}



	function showSummaryNote(){
		nombre =document.getElementById("userBillingUser").value;
		iUserID =document.getElementById("iUserID").value;	
		document.getElementById('nombreNote').value = nombre;
        document.getElementById('iUserIDNote').value = iUserID;
      

	}


	
		
	
		
		$("#userBillingUser").focusout(function(){
			if($("#iUserID").val() == "" || parseInt($("#iUserID").val()) == 0){
				if($(this).val() != ""){
					searchUsersByName($(this).val(),"usersABM.php","usersFounded","iUserID","userBillingUser");
				}else{
					$("#usersFounded").css("display","none");
				}
			}
		});
	
		$("#userBillingUser").keyup(function(){
			$("#iUserID").val("-1");
			if($(this).val() != ""){
				searchUsersByName($(this).val(),"usersABM.php","usersFounded","iUserID","userBillingUser");
			}else{
				$("#usersFounded").css("display","none");
			}
		});
		
		$("#edituserBillingUser").focusout(function(){
			if($("#edituserBillingUserId").val() == "" || parseInt($("#edituserBillingUserId").val()) == 0){
				if($(this).val() != ""){
					searchUsersByName($(this).val(),"usersABM.php","editUsersFounded","edituserBillingUserId","edituserBillingUser");
				}else{
					$("#editUsersFounded").css("display","none");
				}
			}
		});
	
		$("#edituserBillingUser").keyup(function(){
			$("#edituserBillingUserId").val("-1");
			if($(this).val() != ""){
				searchUsersByName($(this).val(),"usersABM.php","editUsersFounded","edituserBillingUserId","edituserBillingUser");
			}else{
				$("#editUsersFounded").css("display","none");
			}
		});
		
		$("#newuserBillingUser").focusout(function(){
			if($("#newuserBillingUserId").val() == "" || parseInt($("#newuserBillingUserId").val()) == 0){
				if($(this).val() != ""){
					searchUsersByName($(this).val(),"usersABM.php","newUsersFounded","newuserBillingUserId","newuserBillingUser");
				}else{
					$("#newUsersFounded").css("display","none");
				}
			}
		});
	
		$("#newuserBillingUser").keyup(function(){
			$("#newuserBillingUserId").val("-1");
			if($(this).val() != ""){
				searchUsersByName($(this).val(),"usersABM.php","newUsersFounded","newuserBillingUserId","newuserBillingUser");
			}else{
				$("#newUsersFounded").css("display","none");
			}
		});
		
	/*	if($("#sError").val() != ""){
			showErrorToast($("#sError").val(),true);
		}*/
	
		$("#btnDeleteuserBilling").click(function(){
			if(confirm("Va a eliminar el medidor\n Est� seguro?")){
				$("#editUserAction").val("deleteuserBilling");
				//deleteUser();
			}
		});
		
				$("#btnRecipePaySubmit").click(function() {
			newRecipePay();
			return false;
		});
		
				$("#btnPayPaySubmit").click(function() {
			newPayPay();
			return false;
		});
		



	$("#newUserBusqueda").focusout(function(){
		if($("#iUserID").val() == "" || parseInt($("#iUserID").val()) == 0){
			if($(this).val() != ""){
				searchUsersByName($(this).val(),"usersABM.php","usersFounded","iUserID","newUserBusqueda");
			}else{
				$("#usersFounded").css("display","none");
			}
		}
	});

	$("#newUserBusqueda").keyup(function(){
		$("#iUserID").val("-1");
		if($(this).val() != ""){
			searchUsersByName($(this).val(),"usersABM.php","usersFounded","iUserID","newUserBusqueda");
		}else{
			$("#usersFounded").css("display","none");
		}
	});
	
	
		$("#newUserBusquedaE").focusout(function(){
		if($("#iUserIDE").val() == "" || parseInt($("#iUserIDE").val()) == 0){
			if($(this).val() != ""){
				searchUsersByName($(this).val(),"usersABM.php","usersFoundedE","iUserIDE","newUserBusquedaE");
			}else{
				$("#usersFoundedE").css("display","none");
			}
		}
	});

	$("#newUserBusquedaE").keyup(function(){
		$("#iUserIDE").val("-1");
		if($(this).val() != ""){
			searchUsersByName($(this).val(),"usersABM.php","usersFoundedE","iUserIDE","newUserBusquedaE");
		}else{
			$("#usersFoundedE").css("display","none");
		}
	});

//arriba personal, abajo empleador

function searchSchoolByName(sName,phpFile,tableName,userIdField,userNameField){
		var dataString = 'sAction=searchSchoolByName&schoolName=' + sName;
		$.ajax({
		type: "post",  
		data: dataString,
		dataType: "json",  
		url: phpFile,  
		success: function(data) {
			var rows ="<tr><td class='headerResultados' colspan='3' style='background-color:#A0D37D;'><b style='color:black;'>Posibles Empleadores</b></td></tr>";
			if(data){
				if(data.status != "ERROR"){
					$("#"+tableName).css("display","");
					if(data.length == 1){
						$("#"+userIdField).val(data[0].userID);
						//$("#usersFounded").css("display","none");
					}
					for(var i in data){
						rows+="<tr style='cursor:pointer' onclick='setSchoolID("+data[i].schoolID+",\""+data[i].schoolName+" CUIL: "+data[i].schoolCuit+"\",\""+tableName+"\",\""+userIdField+"\",\""+userNameField+"\",\""+data[i].schoolCuit+"\");'><td width='200'>"+data[i].schoolName+"</td><td width='150'>"+" CUIL: "+data[i].schoolCuit+"</td></tr>";
//rows+="<tr style='cursor:pointer'><select onchange=onclick='setUserID("+data[i].userID+",\""+data[i].userLastName+" "+data[i].userName+"\",\""+tableName+"\",\""+userIdField+"\",\""+userNameField+"\",\""+data[i].userCategory+"\");'><option value="+data[i].userID+">"+data[i].userLastName+" "+data[i].userName+"</option></select></tr>";
					
					
					}
					$("#"+tableName).html(rows);
				}else{
					showErrorToast(data.message);
				}
			}else{
				$("#"+userIdField).val("-1");
				$("#"+tableName).css("display","none");
			}
		},
		error:function (xhr, ajaxOptions, thrownError){
				showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		}
		});				
	}
	
	
	function setSchoolID(iID,sUserName,tableName,userIdField,userNameField,userCategory){
		$("#"+userIdField).val(iID);
		$("#"+userNameField).val(sUserName);
		$("#"+tableName).css("display","none");
		getNiveles(iID,1);
	}
	
	
	
	
 	$("#newEmpleadorBusqueda").focusout(function(){
		if($("#newEmpleador").val() == "" || parseInt($("#newEmpleador").val()) == 0){
			if($(this).val() != ""){
				searchSchoolByName($(this).val(),"schoolABM.php","schoolFounded","newEmpleador","newEmpleadorBusqueda");
			}else{
				$("#schoolFounded").css("display","none");
			}
		}
	});

	$("#newEmpleadorBusqueda").keyup(function(){
		$("#newEmpleador").val("-1");
		if($(this).val() != ""){
			searchSchoolByName($(this).val(),"schoolABM.php","schoolFounded","newEmpleador","newEmpleadorBusqueda");
		}else{
			$("#schoolFounded").css("display","none");
		}
	});	
	
	//fin de busqueda para empleadores
	
	
	
	//para el loading de algo
	function loading(){
document.getElementById('loading').style.display = 'block';
$.notific8('My notification message goes here.');

}



/*

$(document).on('keyup', "input.focusable",  function(event) {
    $(event.target).val(function(index, value) {
      return value.replace(/[^\d.]/g, '')  
        .replace(/(\..*)\./g, '$1')         // decimal can't exist more than once
      .replace(/(\.[\d]{2})./g, '$1')
        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
    });
    // do something here
}
);
$(document).on('keydown', "input.focusable",  function(e) {
    $(event.target).val(function(index, value) {
		
		var $clase= $('.focusable');
       if (e.which === 13 || e.keyCode == 9) {
		   e.preventDefault(); 
		   var value=$(this).val();
		   value=value.replace(/\,/g,'');
		   value=parseFloat(value).toFixed(2);
		   $(this).val(number_format(value));
        var ind = $clase.index(this);
        $clase.eq(ind + 1).focus();
	   }
      return value.replace(/[^\d.]/g, '')  
        .replace(/(\..*)\./g, '$1')         // decimal can't exist more than once
      .replace(/(\.[\d]{2})./g, '$1')
        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
    });
    // do something here
}
);

$(document).on('blur', "input.focusable", function(event) {
    $(event.target).val(function(index, value) {
    
	  
		   value=value.replace(/\,/g,'');
		   value=parseFloat(value).toFixed(2);
		   $(this).val(number_format(value));
	  
	return number_format(value);
    });
}
);*/

//nueva haber
$(document).on('keydown', "input.focusable",  function(e) {
    $(event.target).val(function(index, value) {
		
		var $clase= $('.focusable');
		var $clase2= $('.cantidad');
       if (e.which === 13 || e.keyCode == 9) {
		   e.preventDefault(); 
				   var value=$(this).val();
		   value=value.replace(/\,/g,'');
		  
			 try {
				 if (/^[\d-+/*().]+$/.test(value)) {
         			var val = 0;
			val= val + eval(value);
				value=val;
				value=parseFloat(value).toFixed(2);
		value=value.toString().replace(/[^\d.]/g, '')  
        .replace(/(\..*)\./g, '$1')         // decimal can't exist more than once
      .replace(/(\.[\d]{2})./g, '$1')
        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
    //
	
			// $(this).val(value);
        var ind = $clase.index(this);
        $clase2.eq(ind + 1).focus();
		
			} 
			else
			{
				
				 alert("El numero ingresado es invalido");
				 value=0;
				 
			}
	 
		 }
catch(err) {
 
				 alert("El numero ingresado es invalido");
				 val=0;
				  
}
		
	   }
      return value;
    });
    
}
);

//nuevas funciones
$(".cantidad").bind('keydown ', function (e) {
  	var $clase= $('.focusable');
		var $clase2= $('.cantidad');
	    if (e.which === 13 || e.keyCode == 9) {console.log("si");e.preventDefault();
		var ind = $clase2.index(this);
        $clase.eq(ind).focus();
		}
		else
			console.log("no");

});


$(":input.cantidad").bind('change keyup', function (e) {


   if (e.which != 13 && e.keyCode != 9) {
 if ($(this).is( ":focus" )==false) {
	
                 
      $(this).data("previousValue", $(this).val());
		
			var $clase= $('.cantidad');
			var $clasefocusable= $('.focusable');
		   e.preventDefault(); 
        var ind = $clase.index(this);
        $clasefocusable.eq(ind).focus();
		$clase.eq(ind).focus();
        }
	        
      $(this).data("previousValue", $(this).val());
		
			var $clase= $('.cantidad');
			var $clasefocusable= $('.focusable');
		   e.preventDefault(); 
		
        var ind = $clase.index(this);
        $clasefocusable.eq(ind).focus();
		$clase.eq(ind).focus();
      
		
   }
		
});


$(":input.cantidad").bind('click', function (e) {


	   console.log("click");           
    

});

//fin de nuevas funciones


/* anterior que estaba
$(document).on('keydown', "input.focusable",  function(e) {
    $(event.target).val(function(index, value) {
		
		var $clase= $('.focusable');
       if (e.which === 13 || e.keyCode == 9) {

		   e.preventDefault(); 
		   //
		   var value=$(this).val();
		   value=value.replace(/\,/g,'');
		 	 
			 try {
				 if (/^[\d-+/*().]+$/.test(value)) {
            // Evaluar el resultado
			
			


			
			var val = 0;
		
			
			val= val + eval(value);
				value=val;
				value=parseFloat(value).toFixed(2);
		value=value.toString().replace(/[^\d.]/g, '')  
        .replace(/(\..*)\./g, '$1')         // decimal can't exist more than once
      .replace(/(\.[\d]{2})./g, '$1')
        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
    //
	
			// $(this).val(value);
        var ind = $clase.index(this);
        $clase.eq(ind + 1).focus();
		
			//alert(value);
     
			} 
			else
			{
				
				 alert("El numero ingresado es invalido");
				 value=0;
			//return;
				 
			}
	 
		 }
catch(err) {
 
				 alert("El numero ingresado es invalido");
				 val=0;
				  
}
		 
		 
		 
		 
		  // value=parseFloat(value).toFixed(2);
		  
	   }
      return value;
    });
    // do something here
}
);*/




$(document).on('focus', "input.focusable",function(event) {
   // $(event.target).val(function(index, value) {
		// var value=$(this).val();
		 //  value=value.replace(/\,/g,'');
		// console.log(value);
	// });
	
	 var val=$(this).val();
	val=val.replace(/\,/g,'');
	$(event.target).val(val);
	$(event.target).select();
	 
	
  }
);
//todos los campos en , y punto
/*
$(".focusable").on({
  "focus": function(event) {
    $(event.target).select();
  },
  "keydown": function(event) {
    $(event.target).val(function(index, value) {
      return value.replace(/[^\d.]/g, '')  
        .replace(/(\..*)\./g, '$1')         // decimal can't exist more than once
      .replace(/(\.[\d]{2})./g, '$1')
        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
    });
  }
  ,
  "keyup": function(event) {
    $(event.target).val(function(index, value) {
      return value.replace(/[^\d.]/g, '')  
        .replace(/(\..*)\./g, '$1')         // decimal can't exist more than once
      .replace(/(\.[\d]{2})./g, '$1')
        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
    });
  }
  ,
  "blur": function(event) {
    $(event.target).val(function(index, value) {
    
	  
		   value=value.replace(/\,/g,'');
		   value=parseFloat(value).toFixed(2);
		   $(this).val(number_format(value));
	  
	return number_format(value);
    });
  }
  ,
   
  
  
  
  
  "keyup": function(event) {
    $(event.target).val(function(index, value) {
      return value.replace(/[^\d.]/g, '')  
        .replace(/(\..*)\./g, '$1')         // decimal can't exist more than once
      .replace(/(\.[\d]{2})./g, '$1')
        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
    });
  }
});

*/


//pensado para modificar los valores automaticos
//pensado para modificar los valores automaticos
function saveToDatabase(editableObj,column,ID,IDBill) {

var val = '';

switch(column) {
 
		case 2:
		
		resultado = document.getElementById(ID);
		

		
		 try {
			 if ((/^[\d-+/*().]+$/).test(editableObj.value)) {
            // Evaluar el resultado
			
			
			
			
 eval('('+editableObj.value+')');
 
 resultado.value = eval("(editableObj.value)");
			
			var val = 0;
		
			
			val= val + eval(resultado.value);
			
			
			editableObj.value = val;
					$(editableObj).val(function(index, value) {
      return value.replace(/[^\d.]/g, '')  
        .replace(/(\..*)\./g, '$1')         // decimal can't exist more than once
      .replace(/(\.[\d]{2})./g, '$1')
        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
    });	 
			
			var chec  = document.getElementById('byNeto'+ID).value;
			separador =',';
			arregloDeSubCadenas=chec.split(separador);
	
			
			
		    document.getElementById('byNeto'+ID).value = val+','+arregloDeSubCadenas[1];
			
		    sumNeto(document.getElementById('byNeto'+ID),IDBill);
			
			} 
			else
			{
			
				resultado.value = 0;
				 editableObj.value = 0;
				 alert("El numero ingresado es invalido");
				 totalRe(IDBill,0.00)
			
				 
			}
}
catch(err) {
	console.log(err);
  resultado.value = 0;
				// alert("El numero ingresado es invalido-4");
				 val=0;
				  totalRe(IDBill,0.00);
}
			
            
			
			
	
			//re('td.subtotal'+IDBill,IDBill);
        break;
		case 3:
		
        val = document.getElementById('cant'+ID).value;
		//console.log('emi hola');
		//console.log(val);
		if (val>0){
			//var resultado = saveToValueConcept(ID,val);
			var resul = 0;
			//resultado = document.getElementById(ID);
			/*setTimeout(function(){
         resultado=saveToValueConcept(ID,val);
    },10);*/
	
	var dataString = 'sAction=editConceptByCant&ID=' + ID +'&val='+ val;
	$.ajax({
		url: "movementsABM.php",
		type: "POST",
		data:dataString,
		async: false,
		success: function(data){
			var hola = JSON.parse(data);
		
			if(hola.queryStatus == "OK"){ 
               
                    resul = hola.val;
             }
		}        
   });
			

			//console.log(resultado);
			//resultado.value = eval(resul);
		/*	resul=resul.toString().replace(/[^\d.]/g, '')  
        .replace(/(\..*)\./g, '$1')         // decimal can't exist more than once
      .replace(/(\.[\d]{2})./g, '$1')
        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");*/
		
		document.getElementById(ID).value = resul;
			//$("#"+ID).val(resul);
			//$('#'+ID).attr('value',resul);
			//$('#'+ID).attr('placeholder',resul);
			//document.getElementById(ID).innerHTML= resul;
			/*$('#'+ID).val(resul);
			$('#'+ID).val(function(index, value) {
      return value.replace(/[^\d.]/g, '')  
        .replace(/(\..*)\./g, '$1')         // decimal can't exist more than once
      .replace(/(\.[\d]{2})./g, '$1')
        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
    });	*/
			
			//resultado = document.getElementById(ID).value;
			//resultado = resultado.toString().replace(/,/g, '');
			//console.log('miradno haber');
			//resultado = val * eval(resultado);
			//document.getElementById(ID).value = resultado;
			//console.log(resultado);
			//resultado.value = eval("(editableObj.value)");
		}
		

		

		
			
			/*val= val + eval(resultado.value);

			editableObj.value = val;
					$(editableObj).val(function(index, value) {
      return value.replace(/[^\d.]/g, '')  
        .replace(/(\..*)\./g, '$1')         // decimal can't exist more than once
      .replace(/(\.[\d]{2})./g, '$1')
        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
    });	 
			
			var chec  = document.getElementById('byNeto'+ID).value;
			separador =',';
			arregloDeSubCadenas=chec.split(separador);
	
			
			
		    document.getElementById('byNeto'+ID).value = val+','+arregloDeSubCadenas[1];
			
		    sumNeto(document.getElementById('byNeto'+ID),IDBill);*/
			
		//miro lo que hay en total
		
		
		
        break;
		  case 5:
        val = document.getElementById('periodoA'+ID).value;
        break;
		  case 6:
        val = document.getElementById('periodoM'+ID).value;
        break;
		case 7:
        val = document.getElementById('antiguedadA'+ID).value;
        break;
		case 8:
        val = document.getElementById('antiguedadM'+ID).value;
        break;
		case 9:
        val = document.getElementById('afectacion'+ID).value;
        break;
		case 10:
        val = document.getElementById('secuencia'+ID).value;
        break;
		case 11:
        val = document.getElementById('registro'+ID).value;
        break;
    default:
		val= editableObj.innerText;
} 


	var dataString = 'sAction=editConceptById&colum='+column+'&ID=' + ID +'&val='+ val;
	$(editableObj).css("background","#FFF url(img/loaderIcon.gif) no-repeat right");
	$.ajax({
		url: "movementsABM.php",
		type: "POST",
		data:dataString,
		success: function(data){
			$(editableObj).css("background","#FDFDFD");
			console.log(data);
			//$.notific8('Se modifico correctamente');
			//funciona bien, recarga antiguedad y todo
			if(column==2){
				//location.reload();
			}
			//location.reload();
		}        
   });
}

function saveToValueConcept(ID,val) {
	var dataString = 'sAction=editConceptByCant&ID=' + ID +'&val='+ val;
	$.ajax({
		url: "movementsABM.php",
		type: "POST",
		data:dataString,
		success: function(data){
			var hola = JSON.parse(data);
		
			if(hola.queryStatus == "OK"){ 
               
                    return hola.val;
                } else {
               
                    return 0;
                }

			//$.notific8('Se modifico correctamente');
			//funciona bien, recarga antiguedad y todo
			//location.reload();
		}        
   });
}


function saveToValueConceptPermanet(ID,type) {
	if (type==1){
	val = document.getElementById('cantPerma'+ID).value;
	var dataString = 'sAction=editConceptByCantPermanent&ID=' + ID +'&val='+ val;
	} else {
	val = document.getElementById('valPerma'+ID).value;
	var dataString = 'sAction=editConceptByValPermanent&ID=' + ID +'&val='+ val;
	}
	$.ajax({
		url: "movementsABM.php",
		type: "POST",
		data:dataString,
		success: function(data){
			
			
				if(data.queryStatus != "ERROR"){
			//var hola = JSON.parse(data);
                return 0;
                    //return hola.val;
                } else {
               
                    return 0;
                }

			//$.notific8('Se modifico correctamente');
			//funciona bien, recarga antiguedad y todo
			//location.reload();
		}        
   });
}
/*
document.querySelectorAll('.Total').forEach(function (total) {
        if (total.classList.length > 1) {
            var letra = total.classList[1];
            var suma = 0;
            document.querySelectorAll('.Columna' + letra).forEach(function (celda) {
                var valor = parseInt(celda.innerHTML);
                suma += valor;
            });
            total.innerHTML = suma;
        }
    });*/

function totalRe(IDBill) {
 	var suma = 0;
  var x = document.querySelectorAll("[id='"+IDBill+"'] input[name='cantRe"+IDBill+"[]']"); //tomo todos los input con name='cantProd[]'

  var i;
  for (i = 0; i < x.length; i++) {
	  
	  var res = eval(x[i].value.replace(/\,/g,''));


	  
	  var val = 0;
	  if (/^[\d-+/*()]+$/.test(res)) {
            // Evaluar el resultado
            val = eval(eval(res));
			} else {
				val = res;
				
			}

	
    suma += parseFloat(0+ val); //ac� hago 0+x[i].value para evitar problemas cuando el input est� vac�o, si no tira NaN
  
  }

// ni idea d�nde lo vas a mostrar ese dato, yo puse un input, pero puede ser cualquier otro elemento
 
 //suma=parseFloat(suma).toFixed(2);
  suma=suma.toFixed(2).toString().replace(/[^\d.]/g, '')  
        .replace(/(\..*)\./g, '$1')         // decimal can't exist more than once
      .replace(/(\.[\d]{2})./g, '$1')
        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
 //suma=parseFloat(suma).toFixed(2);
  document.getElementById('totalRe'+IDBill).value = suma;
  
  
   totalT(IDBill);
}	

function totalRet(IDBill) {
 	var suma = 0;
  var x = document.querySelectorAll("[id='"+IDBill+"'] input[name='cantRet"+IDBill+"[]']"); //tomo todos los input con name='cantProd[]'
  var i;
  for (i = 0; i < x.length; i++) {
	  
	  var res = eval(x[i].value.replace(/\,/g,''));


	  
	  var val = 0;
	  if (/^[\d-+/*()]+$/.test(res)) {
            // Evaluar el resultado
            val = eval(eval(res));
			} else {
				val = res;
				
			}

	
    suma += parseFloat(0+ val); //ac� hago 0+x[i].value para evitar problemas cuando el input est� vac�o, si no tira NaN
  
  }

// ni idea d�nde lo vas a mostrar ese dato, yo puse un input, pero puede ser cualquier otro elemento
 
 //suma=parseFloat(suma).toFixed(2);
  suma=suma.toFixed(2).toString().replace(/[^\d.]/g, '')  
        .replace(/(\..*)\./g, '$1')         // decimal can't exist more than once
      .replace(/(\.[\d]{2})./g, '$1')
        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
 //suma=parseFloat(suma).toFixed(2);
  document.getElementById('totalRet'+IDBill).value = suma;
   totalT(IDBill);
}

function totalRen(IDBill) {
 	var suma = 0;
  var x = document.querySelectorAll("[id='"+IDBill+"'] input[name='cantRen"+IDBill+"[]']"); //tomo todos los input con name='cantProd[]'

 var i;
  for (i = 0; i < x.length; i++) {
	  
	  var res = eval(x[i].value.replace(/\,/g,''));

//alert(res);
	  
	  var val = 0;
	  if (/^[\d-+/*()]+$/.test(res)) {
            // Evaluar el resultado
            val = eval(eval(res));
			} else {
				val = res;
				
			}

	
    suma += parseFloat(0+ val); //ac� hago 0+x[i].value para evitar problemas cuando el input est� vac�o, si no tira NaN
  
  }

// ni idea d�nde lo vas a mostrar ese dato, yo puse un input, pero puede ser cualquier otro elemento
 
 //suma=parseFloat(suma).toFixed(2);
  suma=suma.toFixed(2).toString().replace(/[^\d.]/g, '')  
        .replace(/(\..*)\./g, '$1')         // decimal can't exist more than once
      .replace(/(\.[\d]{2})./g, '$1')
        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
 //suma=parseFloat(suma).toFixed(2);
  document.getElementById('totalRen'+IDBill).value = suma;
  
  
   totalT(IDBill);
}	

/*
function totalRen(IDBill) {
 	var suma = 0;
  var x = document.querySelectorAll("[id='"+IDBill+"'] input[name='cantRen"+IDBill+"[]']"); //tomo todos los input con name='cantProd[]'

   var i;
  for (i = 0; i < x.length; i++) {
	  
	  var res = x[i].value.replace(/,/g, '');


	  
	  var val = 0;
	  if (/^[\d-+/*()]+$/.test(res)) {
            // Evaluar el resultado
            val = eval(eval(res));
			} else {
				val = res;
				
			}

    suma += parseFloat(0+ val); //ac� hago 0+x[i].value para evitar problemas cuando el input est� vac�o, si no tira NaN
  
  }

// ni idea d�nde lo vas a mostrar ese dato, yo puse un input, pero puede ser cualquier otro elemento
  
  
  suma=suma.toString().replace(/[^\d.]/g, '')  
        .replace(/(\..*)\./g, '$1')         // decimal can't exist more than once
      .replace(/(\.[\d]{2})./g, '$1')
        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
 document.getElementById('totalRen'+IDBill).value = suma;
  totalT(IDBill);
}
*/



function sumNeto(valor,IDBill) {
  
  var netoN = 0;
  var netoNoN = 0;
  var retN = 0;
  var suma = 0;
  var sumaN = 0;
  //suma = document.getElementById('totalTN'+IDBill).value;
  //separador = ",",
  //arregloDeSubCadenas = suma.split(separador);
//console.log('holas 4');

    var selected = new Array();
	
     $('input:checkbox[name=sumNeto'+IDBill+']:checked').each(function(){
         selected.push($(this).val());
		 var res = $(this).val().slice(0,-2).replace(",","");
		 
		 var paso = eval(res.replace(/\,/g,''));
		  
		 var vale = 0;
	  if (/^[\d-+/*()]+$/.test(paso)) {
            // Evaluar el resultado
            vale = eval(eval(paso));
			} else {
				vale = paso;
				
			}
			
		var type = $(this).val().slice(-1);
		switch(type) {
    case "1":
    netoNoN += parseFloat(0+ vale);
    break;
    case "2":
    netoN += parseFloat(0+ vale); 
    break;
	case "3":
    netoN += parseFloat(0+ vale); 
    break;
	case "4":
    retN -= parseFloat(0+ vale);
    break;
    default:
    netoN += parseFloat(0+ 0);   
}
      });
	  
//console.log(selected);
//console.log(netoN);
//console.log(netoNoN);
//console.log(netoN*0.81);
var sumas= 0;
sumas = netoN*0.81;
sumas = sumas + netoNoN;


sumas=sumas.toFixed(2).toString().replace(/[^\d.]/g, '')  
        .replace(/(\..*)\./g, '$1')         // decimal can't exist more than once
      .replace(/(\.[\d]{2})./g, '$1')
        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
 //;
 
 
 
  document.getElementById('totalTN'+IDBill).value = sumas;

//alert(suma);
 
/*
 var i;
  for (i = 0; i < x.length; i++) {
	  
	  var res = eval(x[i].value.replace(/\,/g,''));

console.log(res);
	  
	  var val = 0;
	  if (/^[\d-+/*()]+$/.test(res)) {
            // Evaluar el resultado
            val = eval(eval(res));
			} else {
				val = res;
				
			}

	
    suma += parseFloat(0+ val); //ac� hago 0+x[i].value para evitar problemas cuando el input est� vac�o, si no tira NaN
  
  }
  
  console.log(suma);
   $('input:checkbox[name=sumNeto'+IDBill+']:checked').each(function(){
//  arregloDeSubCadenas=$(this).val().split(separador);
var vale = $(this).val().slice(0,-2).replace(",","");
var type = $(this).val().slice(-1);

		switch(type) {
    case "1":
    netoNoN += parseFloat(0+ vale);
    break;
    case "2":
    netoN += parseFloat(0+ vale); 
    break;
	case "3":
    netoN += parseFloat(0+ vale); 
    break;
	case "4":
    retN -= parseFloat(0+ vale);
    break;
    default:
    netoN += parseFloat(0+ 0);   
} 
			
   });

sumaN = ($netoN*0.81)
suma= sumaN+ $netoNoN;

console.log('hola primera suma');
console.log(suma);
//alert(suma);
suma=parseFloat(suma).toFixed(2)
 suma=suma.toFixed(2).toString().replace(/[^\d.]/g, '')  
        .replace(/(\..*)\./g, '$1')         // decimal can't exist more than once
      .replace(/(\.[\d]{2})./g, '$1')
        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
 //;
 
 console.log(suma);
 
  document.getElementById('totalTN'+IDBill).value = suma;*/




  
}			

function totalT(IDBill) {
  var suma = 0;
  var r = eval(document.getElementById('totalRe'+IDBill).value.replace(/[^\d.]/g, '') ); 
  var re = eval(document.getElementById('totalRen'+IDBill).value.replace(/[^\d.]/g, '') ); 
  var ret = eval(document.getElementById('totalRet'+IDBill).value.replace(/[^\d.]/g, '') );
 
	suma =  (r+re)-ret;
	//alert(suma);
	suma=suma.toFixed(2).toString().replace(/[^\d.]/g, '')  
        .replace(/(\..*)\./g, '$1')         // decimal can't exist more than once
      .replace(/(\.[\d]{2})./g, '$1')
        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");

  document.getElementById('totalT'+IDBill).value = suma;
}	
	
function re (selector,IDBill) {	

$(selector).each(function () {
			var total = 0,
			column = $(this).siblings(selector).andSelf().index(this);
			$(this).parents().prevUntil(':has(' + selector + ')').each(function () {
				total += parseFloat($('td.sum'+IDBill+':eq(' + column + ')', this).html()) || 0;
			})
			$(this).html(total);
		});

		
			
/*
let totalRe = 0;
let totalRet = 0;
let totalRen = 0;
// Seleccionar todos los `tr` que tengan el atributo `valorNumerico1`
$("td[sumRe1046]").each(function() {
  // A�adir el valor de ese atributo al total
  totalRe += parseFloat($(this).attr("sumRe1046"));
});
$("td[sumRet"+IDBill+"]").each(function() {
  // A�adir el valor de ese atributo al total
  totalRet += parseFloat($(this).attr("sumRet"+IDBill));
});
$("td[sumRen"+IDBill+"]").each(function() {
  // A�adir el valor de ese atributo al total
  totalRen += parseFloat($(this).attr("sumRen"+IDBill));
});
// Seleccionar el `td` con atributo `sumaTotal` y asignarle el valor de total a ese atributo (ya de paso tambi�n escribirlo)
$("td[subtotalRe"+IDBill+"]").attr("subtotalRe"+IDBill, totalRe).text(totalRe);
$("td[subtotalRet"+IDBill+"]").attr("subtotalRet"+IDBill, totalRet).text(totalRet);
$("td[subtotalRen"+IDBill+"]").attr("subtotalRen"+IDBill, totalRen).text(totalRen);
var todo = (totalRe+totalRen)-totalRet;
$("td[total"+IDBill+"]").attr("total"+IDBill, todo).text(todo);
*/

	}
 


function showEdit(editableObj) {
			$(editableObj).css("background","#FFF");
		var str =	$(editableObj).val();
			//var newStr=$(editableObj).replace(/,/g, '');
		//	alert("esta"+$(editableObj).val(newStr));
	
var res = str.toString().replace(/,/g, '');

$(editableObj).val(res);
} 


  $(document).ready(function () {
 
            (function ($) {
 
                $('#filtrar').keyup(function () {
 
                    var rex = new RegExp($(this).val(), 'i');
                    $('.buscar tr').hide();
                    $('.buscar tr').filter(function () {
                        return rex.test($(this).text());
                    }).show();
 
                })
 
            }(jQuery));
 
        }); 
		
		  $(document).ready(function () {
 
            (function ($) {
 
                $('#filtrarConcept').keyup(function () {
 
                    var rex = new RegExp($(this).val(), 'i');
                    $('.buscarConcept tr').hide();
                    $('.buscarConcept tr').filter(function () {
                        return rex.test($(this).text());
                    }).show();
 
                })
 
            }(jQuery));
 
        }); 
		
			  $(document).ready(function () {
 
            (function ($) {
 
                $('#filtrarPersonalByBill').keyup(function () {
 
                    var rex = new RegExp($(this).val(), 'i');
                    $('.buscarPersonal tr').hide();
                    $('.buscarPersonal tr').filter(function () {
                        return rex.test($(this).text());
                    }).show();
 
                })
 
            }(jQuery));
 
        }); 

function number_format(valor){
	
	return valor.replace(/[^\d.]/g, '')  
        .replace(/(\..*)\./g, '$1')         // decimal can't exist more than once
      .replace(/(\.[\d]{2})./g, '$1')
        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
}


function newFile(action){
	 switch (action) {
    case 'newConceptById':
		direccion='movementsABM.php';
		form='#newConceptById';  
		aswer=($('#iBill').val().length > 0);
		succ='Ha Agregado Nuevos Conceptos. , Concepto/s agregados correctamente.';
        break;
		case 'editNewsByBillDevo':
		direccion='movementsABM.php';
		form='#editNewsByBillDevo';  
		aswer=($('#iBillDevo').val().length > 0);
		succ='Ha Agregado Nuevos Conceptos. , Concepto/s agregados correctamente.';
        break;
		case 'editNewsByBill':
		direccion='movementsABM.php';
		form='#editNewsByBill';  
		aswer=($('#iBillDes').val().length > 0);
		succ='Ha Agregado Nuevos Conceptos. , Concepto/s agregados correctamente.';
		//&& ($('#newConvenio').val().length > 0) && ($('#newConceptValue').val().length >= 1) && ($('#newConceptDataType').val().length >= 1)
        break;
		 case 'newPersonalByBillId':
		direccion='movementsABM.php';
		form='#newPersonalByBillId';  
		aswer=($('#iBill').val().length > 0);
		succ='Ha Agregado Nuevos Recibos. , Recibo/s agregados correctamente.';
        break;
		
		
		
    
}
//$( ".container" ).remove();

	if( aswer ){

		toastr.info("<br><button type='button' id='confirmationRevertYes' class='btn green'>Si</button><button type='button' id='confirmationRevertNo' class='btn default'>No</button>",'Desea Agregar conceptos?',
  {
      closeButton: false,
      allowHtml: true,
      onShown: function (toast) {
		  
          $("#confirmationRevertYes").click(function(){
            	$("#sAction").val(action);
				$.ajax({  
				type: "post",  
				data: $(form).serialize(),
				dataType: "json",  
				url: direccion,  
				success: function(data) {
					if(data.queryStatus == "OK"){
						toastr.success(succ);
						
						
						$('#full-width').modal('hide');
						$('#full-widthDescuentos').modal('hide');
						$('#full-widthEmployed').modal('hide');
						$('#full-widthDevoluciones').modal('hide');
						location.reload();
						//agrego los conceptos

						      for (x=0;x<data.query.length;x++){
							  
var row = $("<tr>");		

if (typeof data.query[x].tipo !== 'undefined'){
	
	var chequeado = "";
	if (data.query[x].bruto==1) {
		chequeado="checked";
	}
	
							switch(data.query[x].tipo) {
    case "1":
        row.append($("<td>"+data.query[x].idConcept+"</td>"))
				.append($('<td contenteditable="true" onBlur="saveToDatabase(this,1 ,'+ data.query[x].movement + ','+data.query[x].idBill+')" onClick="showEdit(this);">'+ data.query[x].name +' </td>'))
				.append($('<td contenteditable="true" onBlur="saveToDatabase(this,3 ,'+ data.query[x].movement + ','+data.query[x].idBill+')" onClick="showEdit(this);">1</td>'))
				.append($('<td></td>'))
				.append($('<td contenteditable="true" onBlur="saveToDatabase(this, 2 , '+ data.query[x].movement + ','+data.query[x].idBill+')" onClick="showEdit(this);"> <input type="text" class="focusable" name="cantRen'+ data.query[x].idBill + '[]" id="'+ data.query[x].movement + '" onBlur="saveToDatabase(this, 2 , '+ data.query[x].movement + ','+data.query[x].idBill+')" onClick="showEdit(this);" onChange="totalRen('+ data.query[x].idBill + ','+ data.query[x].movement + ')" value="'+ number_format(data.query[x].value+"") +'"/>	</td>'))
				.append($('<td></td>'))
				.append($('<td><input data-index="1" class="focusable" name="sumNeto'+data.query[x].idBill+'" id="byNeto'+ data.query[x].movement + '" onclick="sumNeto(this,'+data.query[x].idBill+');" type="checkbox" '+chequeado+' value="'+data.query[x].value+','+data.query[x].tipo+'"> </td>'))
				.append($('<td><a  href="javascript:;" onclick="deleteConcept(this,'+ data.query[x].movement + ','+ data.query[x].idBill + ');" class="btn red-mint"><span class="glyphicon glyphicon-trash"></span></a></td>'));
				$("#"+data.query[x].idBill+" tbody").append(row);
        break;
    case "2":
       row.append($("<td>"+data.query[x].idConcept+"</td>"))
				.append($('<td contenteditable="true" onBlur="saveToDatabase(this,1 ,'+ data.query[x].movement + ','+data.query[x].idBill+')" onClick="showEdit(this);">'+ data.query[x].name +' </td>'))
				.append($('<td contenteditable="true" onBlur="saveToDatabase(this,3 ,'+ data.query[x].movement + ','+data.query[x].idBill+')" onClick="showEdit(this);">1</td>'))
				.append($('<td contenteditable="true" onBlur="saveToDatabase(this, 2 , '+ data.query[x].movement + ','+data.query[x].idBill+')" onClick="showEdit(this);"> <input type="text" class="focusable" name="cantRe'+ data.query[x].idBill + '[]" id="'+ data.query[x].movement + '" onBlur="saveToDatabase(this, 2 , '+ data.query[x].movement + ','+data.query[x].idBill+')" onClick="showEdit(this);" onChange="totalRe('+ data.query[x].idBill + ','+ data.query[x].movement + ')" value="'+number_format(data.query[x].value+"") +'"/>	</td>'))
				.append($('<td></td>'))
				.append($('<td></td>'))
				.append($('<td><input data-index="1" class="focusable" name="sumNeto'+data.query[x].idBill+'" id="byNeto'+ data.query[x].movement + '" onclick="sumNeto(this,'+data.query[x].idBill+');" type="checkbox" '+chequeado+' value="'+data.query[x].value+','+data.query[x].tipo+'"> </td>'))
				.append($('<td><a  href="javascript:;" onclick="deleteConcept(this,'+ data.query[x].movement + ','+ data.query[x].idBill + ');" class="btn red-mint"><span class="glyphicon glyphicon-trash"></span></a></td>'));
				$("#"+data.query[x].idBill+" tbody").append(row);
        break;
		  case "3":
       row.append($("<td>"+data.query[x].idConcept+"</td>"))
				.append($('<td contenteditable="true" onBlur="saveToDatabase(this,1 ,'+ data.query[x].movement + ','+data.query[x].idBill+')" onClick="showEdit(this);">'+ data.query[x].name +' </td>'))
				.append($('<td contenteditable="true" onBlur="saveToDatabase(this,3 ,'+ data.query[x].movement + ','+data.query[x].idBill+')" onClick="showEdit(this);">1</td>'))
				.append($('<td contenteditable="true" onBlur="saveToDatabase(this, 2 , '+ data.query[x].movement + ','+data.query[x].idBill+')" onClick="showEdit(this);"> <input type="text" class="focusable" name="cantRe'+ data.query[x].idBill + '[]" id="'+ data.query[x].movement + '" onBlur="saveToDatabase(this, 2 , '+ data.query[x].movement + ','+data.query[x].idBill+')" onClick="showEdit(this);" onChange="totalRe('+ data.query[x].idBill + ','+ data.query[x].movement + ')" value="'+ number_format(data.query[x].value+"") +'"/>	</td>'))
				.append($('<td></td>'))
				.append($('<td></td>'))
				.append($('<td><input data-index="1" class="focusable" name="sumNeto'+data.query[x].idBill+'" id="byNeto'+ data.query[x].movement + '" onclick="sumNeto(this,'+data.query[x].idBill+');" type="checkbox" '+chequeado+' value="'+data.query[x].value+','+data.query[x].tipo+'"> </td>'))
				.append($('<td><a  href="javascript:;" onclick="deleteConcept(this,'+ data.query[x].movement + ','+ data.query[x].idBill + ');" class="btn red-mint"><span class="glyphicon glyphicon-trash"></span></a></td>'));
				$("#"+data.query[x].idBill+" tbody").append(row);
        break;
		  case "4":
       row.append($("<td>"+data.query[x].idConcept+"</td>"))
				.append($('<td contenteditable="true" onBlur="saveToDatabase(this,1 ,'+ data.query[x].movement + ','+data.query[x].idBill+')" onClick="showEdit(this);">'+ data.query[x].name +' </td>'))
				.append($('<td contenteditable="true" onBlur="saveToDatabase(this,3 ,'+ data.query[x].movement + ','+data.query[x].idBill+')" onClick="showEdit(this);">1</td>'))
				.append($('<td></td>'))
				.append($('<td></td>'))
				.append($('<td contenteditable="true" onBlur="saveToDatabase(this, 2 , '+ data.query[x].movement + ','+data.query[x].idBill+')" onClick="showEdit(this);"> <input type="text" class="focusable" name="cantRet'+ data.query[x].idBill + '[]" id="'+ data.query[x].movement + '" onBlur="saveToDatabase(this, 2 , '+ data.query[x].movement + ','+data.query[x].idBill+')" onClick="showEdit(this);" onChange="totalRet('+ data.query[x].idBill + ','+ data.query[x].movement + ')" value="'+ number_format(data.query[x].value+"") +'"/>	</td>'))
				.append($('<td><input data-index="1" class="focusable" name="sumNeto'+data.query[x].idBill+'" id="byNeto'+ data.query[x].movement + '" onclick="sumNeto(this,'+data.query[x].idBill+');" type="checkbox" '+chequeado+' value="'+data.query[x].value+','+data.query[x].tipo+'"> </td>'))
				.append($('<td><a  href="javascript:;" onclick="deleteConcept(this,'+ data.query[x].movement + ','+ data.query[x].idBill + ');" class="btn red-mint"><span class="glyphicon glyphicon-trash"></span></a></td>'));
				$("#"+data.query[x].idBill+" tbody").append(row);
        break;
	
    default:
       toastr.error('Hubo un error en agregar conceptos tipo','Datos No agregados');
} 

totalRe(data.query[x].idBill);
totalRen(data.query[x].idBill);
totalRet(data.query[x].idBill);
totalT(data.query[x].idBill);
sumNeto('',data.query[x].idBill);

}	 else {
	toastr.error('Hubo un error en agregar conceptos fuera','Datos No agregados');
}						
				
							
							
							
							
						}
						
					}else{
						
						toastr.error('Completo todos los Datos?','Datos No generados');
					}
				},
				error:function (xhr, ajaxOptions, thrownError){
					toastr.error('Completo todos los Datos?','Datos No generados');
					//$.notific8(data.status);
						//toastr.error(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
				}
				});
          });
		  
		   $("#confirmationRevertNo").click(function(){
						toastr.info('Cancelado por Usuario.', 'Datos No Generados.');
						//toastr.success('Datos No guardados', 'Editar Cargo');
						 //$('#full-width').modal('hide');
						
				});

        }
  });
		

	
		
		}else{
			toastr.warning('Completo todos los datos?','Datos No guardados');
			//$.notific8(data.status);
			//showErrorToast("El usuario no existe. Modifique los datos y reintente");
		}
	}	
	
	
function newFiledos(tabla) {   

var dataString = 'sAction=newConceptById&ID=' + tabla;
	$.ajax({
		url: "movementsABM.php",
		type: "POST",
		data:dataString,
		success: function(data){
			if(data){
				var obj = JSON.parse(data);
				var row = $("<tr>");
				row.append($("<td>"+obj.lastId+"</td>"))
				.append($('<td contenteditable="true" onBlur="saveToDatabase(this,1 ,'+ obj.lastId + ')" onClick="showEdit(this);"> </td>'))
				.append($('<td contenteditable="true" onBlur="saveToDatabase(this, 2 , '+ obj.lastId + ')" onClick="showEdit(this);"> </td>'))
				.append($('<td><a  href="javascript:;" onclick="deleteConcept(this,'+ obj.lastId + ');" class="btn red-mint"><span class="glyphicon glyphicon-trash"></span></a></td>'));
				$("#"+tabla+" tbody").append(row);
			} else {
				toastr.error('No se pudo agregar la fila','Error en Agregar');
			}
			
		}        
   });
  
}


function newDevoluciones(action) {   

 switch (action) {
   
		case 'editNewsByBillDevo':
		direccion='movementsABM.php';
		form='#editNewsByBillDevo';  
		aswer=($('#iBillDevo').val().length > 0);
		succ='Ha Agregado Nuevas Devoluciones. , Devoluciones agregadas correctamente.';
        break;
 
}


	if( aswer ){

		toastr.info("<br><button type='button' id='confirmationRevertYes' class='btn green'>Si</button><button type='button' id='confirmationRevertNo' class='btn default'>No</button>",'Desea Agregar conceptos?',
  {
      closeButton: false,
      allowHtml: true,
      onShown: function (toast) {
		  
          $("#confirmationRevertYes").click(function(){
            	$("#sAction").val(action);
				$.ajax({  
				type: "post",  
				data: $(form).serialize(),
				dataType: "json",  
				url: direccion,  
				success: function(data) {
					if(data.queryStatus == "OK"){
						toastr.success(succ);

						$('#full-widthDescuentos').modal('hide');
						
						      for (x=0;x<data.query.length;x++){
							  
var row = $("<tr>");		

if (typeof data.query[x].news !== 'undefined'){
	
				row.append($("<td>"+data.query[x].news+"</td>"))
				.append($('<td>'+ data.query[x].cant +' </td>'))
				.append($('<td>'+ data.query[x].total +'</td>'))
				.append($('<td>'+ data.query[x].reason +'</td>'))
				.append($('<td><a  href="javascript:;" onclick="deleteDevolucion(this,'+ data.query[x].id + ','+ data.query[x].bill + ');" class="btn red-mint"><span class="glyphicon glyphicon-trash"></span></a></td>'));
				$("#devolucion_"+data.query[x].bill+" tbody").append(row);
}	

 else {
	toastr.error('Hubo un error en agregar devoluciones','Datos No agregados');
}						
				
							
							
							
							
						}
						
					}else{
						
						toastr.error('Completo todos los Datos?','Datos No generados');
					}
				},
				error:function (xhr, ajaxOptions, thrownError){
					toastr.error('Completo todos los Datos?','Datos No generados');
					//$.notific8(data.status);
						//toastr.error(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
				}
				});
          });
		  
		   $("#confirmationRevertNo").click(function(){
						toastr.info('Cancelado por Usuario.', 'Datos No Generados.');
						//toastr.success('Datos No guardados', 'Editar Cargo');
						 //$('#full-width').modal('hide');
						
				});

        }
  });
		

	
		
		}else{
			toastr.warning('Completo todos los datos?','Datos No guardados');
			//$.notific8(data.status);
			//showErrorToast("El usuario no existe. Modifique los datos y reintente");
		}
	}


//actualizar precios de todos


function updateCostoPromedio(){

		var dataString = 'sAction=updateCostoPromedio';
		direccion='productsABM.php'; 

		toastr.info("<br><button type='button' id='confirmationRevertYes' class='btn green'>Si</button><button type='button' id='confirmationRevertNo' class='btn default'>No</button>",'Desea actualizar todos los costos?',
  {
      closeButton: false,
      allowHtml: true,
      onShown: function (toast) {
		  
          $("#confirmationRevertYes").click(function(){
				$.ajax({  
				type: "post",  
				data:dataString,
				dataType: "json",  
				url: direccion,  
				success: function(data) {
					if(data.status == "OK"){
						toastr.success('Ha actualizado los costos.', 'Costos Promedios actualizados correctamente.');
					}else{
						toastr.error('Completo todos los datos?','Costos No Actualizado');
					}
				},
				error:function (xhr, ajaxOptions, thrownError){
					toastr.error('Completo todos los datos?','Costos No Actualizado');
					
				}
				});
          });
		  
		   $("#confirmationRevertNo").click(function(){
						toastr.info('Cancelado por Usuario.', 'Costos No Actualizado.');
						
						
				});

        }
  });
		


	}






//eliminar recibo entero
function deleteInvoice(IDBILL){

		var dataString = 'sAction=deleteInvoiceById&ID=' + IDBILL;
		direccion='movementsABM.php'; 

		toastr.info("<br><button type='button' id='confirmationRevertYes' class='btn green'>Si</button><button type='button' id='confirmationRevertNo' class='btn default'>No</button>",'Desea Eliminar el Recibo?',
  {
      closeButton: false,
      allowHtml: true,
      onShown: function (toast) {
		  
          $("#confirmationRevertYes").click(function(){
				$.ajax({  
				type: "post",  
				data:dataString,
				dataType: "json",  
				url: direccion,  
				success: function(data) {
					if(data){
						$("#"+IDBILL).remove();
						/*var p=o.parentNode.parentNode;
						p.parentNode.removeChild(p);*/
						toastr.success('Ha eliminado el Recibo.', 'Recibo eliminado correctamente.');
					}else{
						toastr.error('Completo todos los datos?','Recibo No Eliminado');
					}
				},
				error:function (xhr, ajaxOptions, thrownError){
					toastr.error('Completo todos los datos?','Recibo No Eliminado');
					
				}
				});
          });
		  
		   $("#confirmationRevertNo").click(function(){
						toastr.info('Cancelado por Usuario.', 'Concepto No Eliminado.');
						
						
				});

        }
  });
		


	}

	//eliminar conceptos permanentes
function editConceptPermanent(editConceptByIdPermanent){
	var valores = new Array();
	var inputs = document.querySelectorAll("[name='destinoPermanent[]']:checked");
// atravesamos el array de inputs
for (var x = 0; x < inputs.length; x++) {
    // insertando su valores en el array de valores
    valores.push(inputs[x].value);
}
		var dataString = 'sAction=editConceptByIdPermanent&destinoPermanent='+valores.join(",");
		direccion='movementsABM.php'; 

		toastr.info("<br><button type='button' id='confirmationRevertYes' class='btn green'>Si</button><button type='button' id='confirmationRevertNo' class='btn default'>No</button>",'Desea Eliminar el/los conceptos seleccionados?',
  {
      closeButton: false,
      allowHtml: true,
      onShown: function (toast) {
		  
          $("#confirmationRevertYes").click(function(){
				$.ajax({  
				type: "post",  
				data:dataString,
				dataType: "json",  
				url: direccion,  
				success: function(data) {
					if(data){
						$('#full-widthEdit').modal('hide');
						toastr.success('Ha eliminado el/los conceptos seleccionados.', 'Concepto/s eliminados correctamente.');
					}else{
						toastr.error('Completo todos los datos?','Concepto/s No Eliminados');
					}
				},
				error:function (xhr, ajaxOptions, thrownError){
					toastr.error('Completo todos los datos?','Concepto/s No Eliminados');
					
				}
				});
          });
		  
		   $("#confirmationRevertNo").click(function(){
						toastr.info('Cancelado por Usuario.', 'Concepto No Eliminado.');
						
						
				});

        }
  });
		


	}


//eliminar conceptos dentro del recibo
function deleteConcept(o,id,IDBILL){

		var dataString = 'sAction=deleteConceptById&ID=' + id;
		direccion='movementsABM.php'; 

		toastr.info("<br><button type='button' id='confirmationRevertYes' class='btn green'>Si</button><button type='button' id='confirmationRevertNo' class='btn default'>No</button>",'Desea Eliminar el Concepto?',
  {
      closeButton: false,
      allowHtml: true,
      onShown: function (toast) {
		  
          $("#confirmationRevertYes").click(function(){
				$.ajax({  
				type: "post",  
				data:dataString,
				dataType: "json",  
				url: direccion,  
				success: function(data) {
					if(data){
						var p=o.parentNode.parentNode;
						p.parentNode.removeChild(p);
						toastr.success('Ha eliminado el Concepto.', 'Concepto eliminado correctamente.');
						location.reload();
						totalRe(IDBILL);
						totalRen(IDBILL);
						totalRet(IDBILL);
						totalT(IDBILL);
						
					}else{
						toastr.error('Completo todos los datos?','Concepto No Eliminado');
					}
				},
				error:function (xhr, ajaxOptions, thrownError){
					toastr.error('Completo todos los datos?','Concepto No Eliminado');
					
				}
				});
          });
		  
		   $("#confirmationRevertNo").click(function(){
						toastr.info('Cancelado por Usuario.', 'Concepto No Eliminado.');
						
						
				});

        }
  });
		


	}


//eliminar conceptos dentro del recibo
function deleteDevolucion(o,id,IDBILL){

		var dataString = 'sAction=deleteDevolucionById&ID=' + id;
		direccion='movementsABM.php'; 

		toastr.info("<br><button type='button' id='confirmationRevertYes' class='btn green'>Si</button><button type='button' id='confirmationRevertNo' class='btn default'>No</button>",'Desea Eliminar la Devoluci�n?',
  {
      closeButton: false,
      allowHtml: true,
      onShown: function (toast) {
		  
          $("#confirmationRevertYes").click(function(){
				$.ajax({  
				type: "post",  
				data:dataString,
				dataType: "json",  
				url: direccion,  
				success: function(data) {
					if(data){
						var p=o.parentNode.parentNode;
						p.parentNode.removeChild(p);
						toastr.success('Ha eliminado la Devoluci�n.', 'Devoluci�n eliminada correctamente.');
						//location.reload();

					}else{
						toastr.error('Completo todos los datos?','Devoluci�n No Eliminada');
					}
				},
				error:function (xhr, ajaxOptions, thrownError){
					toastr.error('Completo todos los datos?','Devoluci�n No Eliminada');
					
				}
				});
          });
		  
		   $("#confirmationRevertNo").click(function(){
						toastr.info('Cancelado por Usuario.', 'Concepto No Eliminado.');
						
						
				});

        }
  });
		


	}


	
	//agregar conceptos para asignaci�n de colegios
function addConceptBySchool(addConceptBySchool){
		direccion='movementsABM.php'; 
		form='#addConceptBySchool';  
		toastr.info("<br><button type='button' id='confirmationRevertYes' class='btn green'>Si</button><button type='button' id='confirmationRevertNo' class='btn default'>No</button>",'Desea Agregar Concepto?',
  {
      closeButton: false,
      allowHtml: true,
      onShown: function (toast) {
		  
          $("#confirmationRevertYes").click(function(){
				$.ajax({  
				type: "post",  
				data: $(form).serialize(),
				dataType: "json",  
				url: direccion,  
				success: function(data) {
					if(data.status == "OK"){
						$('#full-widthConceptos').modal('hide');
							$("#formSearch").submit();
						toastr.success('Ha agregado el Concepto.', 'Concepto Agregado correctamente.');
					}else{
						toastr.error('Completo todos los datos?','Concepto No Agregado');
					}
				},
				error:function (xhr, ajaxOptions, thrownError){
					toastr.error('Completo todos los datos?','Concepto No Agregado');
					
				}
				});
          });
		  
		   $("#confirmationRevertNo").click(function(){
						toastr.info('Cancelado por Usuario.', 'Concepto No Eliminado.');
						
						
				});

        }
  });
		


	}

		//quitar conceptos para asignaci�n de colegios
function delConceptBySchool(delConceptBySchool){
		direccion='movementsABM.php'; 
		form='#delConceptBySchool';  
		toastr.info("<br><button type='button' id='confirmationRevertYes' class='btn green'>Si</button><button type='button' id='confirmationRevertNo' class='btn default'>No</button>",'Desea Quitar ese/os Concepto/s?',
  {
      closeButton: false,
      allowHtml: true,
      onShown: function (toast) {
		  
          $("#confirmationRevertYes").click(function(){
				$.ajax({  
				type: "post",  
				data: $(form).serialize(),
				dataType: "json",  
				url: direccion,  
				success: function(data) {
					if(data.status == "OK"){
						$('#full-widthNoConceptos').modal('hide');
							$("#formSearch").submit();
						toastr.success('Ha quitado el Concepto.', 'Concepto Quitado correctamente.');
					}else{
						toastr.error('Completo todos los datos?','Concepto No Quitado');
					}
				},
				error:function (xhr, ajaxOptions, thrownError){
					toastr.error('Completo todos los datos?','Concepto No Quitado');
					
				}
				});
          });
		  
		   $("#confirmationRevertNo").click(function(){
						toastr.info('Cancelado por Usuario.', 'Concepto No Eliminado.');
						
						
				});

        }
  });
		


	}


	 function deleteConceptAnteriorver(o,id){

var r = confirm("Desea Eliminar el Concepto!");
if (r == true) {
   var dataString = 'sAction=deleteConceptById&ID=' + id;
	$.ajax({
		url: "movementsABM.php",
		type: "POST",
		data:dataString,
		success: function(data){
			if(data){
				var p=o.parentNode.parentNode;
				p.parentNode.removeChild(p);
	
			} else {
				$.notific8('No se pudo eliminar la fila');
			}
			
		}        
   });
} else {
    $.notific8('Cancelada por Usuario!');
}		 
		 
		 
	 }
	 
	 
function openNewPage (page,data){
	   var obj = JSON.parse(data);
	   //alert(data);
	  
		window.open(page +'?data='+obj,'_blank'); 
	 }
	
	

function marcar(source) 
	{
		checkboxes=document.getElementsByTagName('input'); //obtenemos todos los controles del tipo Input
		for(i=0;i<checkboxes.length;i++) //recoremos todos los controles
		{
			if(checkboxes[i].type == "checkbox") //solo si es un checkbox entramos
			{
				checkboxes[i].checked=source.checked; //si es un checkbox le damos el valor del checkbox que lo llam� (Marcar/Desmarcar Todos)
			}
		}
	}


function validarCuit(cuit) {
 
        if(cuit.length != 11) {
            return false;
        }
 
        var acumulado   = 0;
        var digitos     = cuit.split("");
        var digito      = digitos.pop();
 
        for(var i = 0; i < digitos.length; i++) {
            acumulado += digitos[9 - i] * (2 + (i % 6));
        }
 
        var verif = 11 - (acumulado % 11);
        if(verif == 11) {
            verif = 0;
        } else if(verif == 10) {
            verif = 9;
        }
 
        return digito == verif;
}
	
	function validarCUIT (val) {
var valor = document.getElementById(val).value;
	
if (!validarCuit(valor)){
	alert('CUIT NO VALIDO');
	$("#"+val).val('');
}	
		
		
	}
	
	
//limpiar formularios
	function formReset(name)
{
document.getElementById(name).reset();
}


//se va a pasar todo nuevo y editar a js

function editParameters(action){

switch (action) {
	case 'editParameters':
		direccion='parameters.php';
		form='#editParameters';  
		item='#name';
		aswer=($('#name').val().length > 0);
        break;
}

	if( aswer ){

		toastr.info("<br><button type='button' id='confirmationRevertYes' class='btn green'>Si</button><button type='button' id='confirmationRevertNo' class='btn default'>No</button>",'Desea Guardar los Datos?',
  {
      closeButton: false,
      allowHtml: true,
      onShown: function (toast) {
		  
          $("#confirmationRevertYes").click(function(){
            	$("#sAction").val(action);
				$.ajax({  
				type: "post",  
				data: $(form).serialize(),
				dataType: "json",  
				url: direccion,  
				success: function(data) {
					if(data.status == "OK"){
						toastr.success('Ha modificado el elemento.', 'Datos Guardados correctamente.');
						// $('#full-widthEdit').modal('hide');
						//$("#formSearch").submit();
					}else{
						toastr.error('Completo todos los datos?','Datos No guardados');
					}
				},
				error:function (xhr, ajaxOptions, thrownError){
					toastr.error('Completo todos los datos?','Datos No guardados');
					//$.notific8(data.status);
						//showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
				}
				});
          });
		  
		   $("#confirmationRevertNo").click(function(){
						toastr.info('Cancelado por Usuario.', 'Datos No Guardados.');
						//toastr.success('Datos No guardados', 'Editar Cargo');
						 $('#full-widthEdit').modal('hide');
						
				});

        }
  });
		

	
		
		}else{
			toastr.warning('Completo todos los datos?','Datos No guardados');
			//$.notific8(data.status);
			//showErrorToast("El usuario no existe. Modifique los datos y reintente");
		}
	}




//edicion
function editOneItem(action){

switch (action) {
    case 'editSchoolCharges':
		direccion='chargesABM.php';
		form='#editSchoolCharges';  
		item='#editCargo';
		aswer=parseInt($("#iEditID").val()) !=0 && parseInt($("#iEditID").val()) !=-1 && ($(item).val().length > 3);
        break;
	case 'editProviderLevels':
       direccion='levelsABM.php';
		form='#editProviderLevels';  
		item='#editNivel';
		aswer=parseInt($("#iEditID").val()) !=0 && parseInt($("#iEditID").val()) !=-1 && ($(item).val().length > 3);
        break;
		case 'editProviderLevelsUser':
       direccion='levelsABMUser.php';
		form='#editProviderLevelsUser';  
		item='#editNivel';
		aswer=parseInt($("#iEditID").val()) !=0 && parseInt($("#iEditID").val()) !=-1 && ($(item).val().length > 3);
        break;
		case 'editProductsCategory':
       direccion='categoryABM.php';
		form='#editProductsCategory';  
		item='#editNivel';
		aswer=parseInt($("#iEditID").val()) !=0 && parseInt($("#iEditID").val()) !=-1 && ($(item).val().length > 3);
		break;
		case 'editProductsCategoryUser':
       direccion='categoryABMUser.php';
		form='#editProductsCategoryUser';  
		item='#editNivel';
		aswer=parseInt($("#iEditID").val()) !=0 && parseInt($("#iEditID").val()) !=-1 && ($(item).val().length > 3);
		break;
		case 'editProducts':
       direccion='productsABM.php';
		form='#editProducts';  
		item='#editProducts';
		aswer=parseInt($("#iEditID").val()) !=0 && parseInt($("#iEditID").val()) !=-1 && ($(item).val().length > 3);
		break;
		case 'editWorks':
       direccion='works.php';
		form='#editWorks';  
		item='#editNivel';
		aswer=parseInt($("#iEditID").val()) !=0 && parseInt($("#iEditID").val()) !=-1 && ($(item).val().length > 3);
		break;
		case 'editWorksUser':
       direccion='worksUser.php';
		form='#editWorksUser';  
		item='#editNivel';
		aswer=parseInt($("#iEditID").val()) !=0 && parseInt($("#iEditID").val()) !=-1 && ($(item).val().length > 3);
		break;
		case 'editServices':
       direccion='services.php';
		form='#editServices';  
		item='#editNivel';
		aswer=parseInt($("#iEditID").val()) !=0 && parseInt($("#iEditID").val()) !=-1 && ($(item).val().length > 3);
        break;
		case 'editSchoolLabor':
       direccion='laborABM.php';
		form='#editSchoolLabor';  
		item='#editSindicato';
		aswer=parseInt($("#iEditID").val()) !=0 && parseInt($("#iEditID").val()) !=-1 && ($(item).val().length > 3);
        break;
		case 'editAgreement':
       direccion='agreementABM.php';
		form='#editAgreement';  
		item='#editConvenio';
		aswer=parseInt($("#iEditID").val()) !=0 && parseInt($("#iEditID").val()) !=-1 && ($(item).val().length > 3);
        break;
		case 'editSocialWork':
		direccion='socialWork.php';
		form='#editSocialWork';  
		item='#editCode';
		aswer= parseInt($("#iEditID").val()) !=0 && parseInt($("#iEditID").val()) !=-1 && ($('#editCode').val().length > 2) && ($('#editCode').val().length < 7) && ($('#editNombre').val().length > 2);
        break;
		case 'editConceptoAfipMappeo':
		direccion='conceptAfipABM.php';
		form='#editConceptoAfipMappeo';  
		aswer= parseInt($("#iEditID").val()) !=0 && parseInt($("#iEditID").val()) !=-1;
        break;
			case 'editPersonalByBill':
		direccion='personalByBill.php';
		form='#editPersonalByBill';  
		aswer=parseInt($("#iEditID").val()) !=0 && parseInt($("#iEditID").val()) !=-1 &&($('#editApellido').val().length > 2) && ($('#editNombre').val().length > 2) && ($('#editTipoDocumento').val().length >= 1)&& ($('#editCuit').val().length >= 11)&& ($('#editDocumento').val().length >= 1)&& ($('#editCategoria').val().length >= 1);
        break;
		
		
		case 'editPersonal':
		direccion='personalABM.php';
		form='#editPersonal';  
		aswer=parseInt($("#iEditID").val()) !=0 && parseInt($("#iEditID").val()) !=-1 &&($('#editApellido').val().length > 2) && ($('#editNombre').val().length > 2) && ($('#editObraSocial').val().length >= 1) && ($('#editTipoDocumento').val().length >= 1)&& ($('#editCuit').val().length >= 11)&& ($('#editDocumento').val().length >= 1)&& ($('#editCategoria').val().length >= 1)&& ($('#editEstadoCivil').val().length >= 1);
        break;
		case 'editUser':
			direccion='usersABM.php';
			form='#editUser';  
			aswer=parseInt($("#iEditID").val()) !=0 && parseInt($("#iEditID").val()) !=-1 &&($('#editApellido').val().length > 2) && ($('#editNombre').val().length > 2) &&  ($('#editTipoDocumento').val().length >= 1)&& ($('#editDocumento').val().length >= 1)&& ($('#editCategoria').val().length >= 1)&& ($('#editEstadoCivil').val().length >= 1);
			break;
		case 'editProvider':
		direccion='providerABM.php';
		form='#editProvider';  
		aswer=($('#editProveedor').val().length > 0) && ($('#editRazon').val().length > 0) && ($('#editLocalidad').val().length >= 1) && ($('#editDomicilio').val().length >= 1)&& ($('#editCuit').val().length >= 1);
        break;
		case 'editConcept':
		direccion='conceptABM.php';
		form='#editConcept';  
		aswer=($('#editConceptName').val().length > 0);
		//&& ($('#editConvenio').val().length > 0) && ($('#editConceptValue').val().length >= 1) && ($('#editConceptDataType').val().length >= 1)
        break;
		case 'editConceptAfip':
		direccion='conceptAfipSueldoABM.php';
		form='#editConceptAfip';  
		aswer=($('#editConceptName').val().length > 0);
		//&& ($('#editConvenio').val().length > 0) && ($('#editConceptValue').val().length >= 1) && ($('#editConceptDataType').val().length >= 1)
        break;
		case 'editCategory':
		direccion='categoryABM.php';
		form='#editCategory';  
		aswer=($('#editCategoria').val().length > 0);
		//&& ($('#newConvenio').val().length > 0) && ($('#newConceptValue').val().length >= 1) && ($('#newConceptDataType').val().length >= 1)
        break;
		case 'editCategoryConcept':
			direccion='category_concept_ABM.php';
			form='#editCategoryConcept';  
			aswer=($('#editCategoria').val().length > 0);
			//&& ($('#newConvenio').val().length > 0) && ($('#newConceptValue').val().length >= 1) && ($('#newConceptDataType').val().length >= 1)
			break;
		case 'editCA':
		direccion='chargesByAgreement.php';
		form='#editCA';  
		aswer=($('#editCargo').val().length > 0);
		//&& ($('#newConvenio').val().length > 0) && ($('#newConceptValue').val().length >= 1) && ($('#newConceptDataType').val().length >= 1)
        break;
		case 'editpersonalBySchool':
		direccion='personalBySchool.php';
		form='#editpersonalBySchool';  
		aswer=($('#iUserIDE').val().length > 0);
		//&& ($('#newConvenio').val().length > 0) && ($('#newConceptValue').val().length >= 1) && ($('#newConceptDataType').val().length >= 1)
        break;
		case 'editServiceByUser':
		direccion='usersByServices.php';
		form='#editServiceByUser';  
		aswer=($('#iUserIDE').val().length > 0);
		//&& ($('#newConvenio').val().length > 0) && ($('#newConceptValue').val().length >= 1) && ($('#newConceptDataType').val().length >= 1)
        break;
		
		case 'editNewsByBill':
		direccion='personalBySchool.php';
		form='#editNewsByBill';  
		aswer=($('#iUserID').val().length > 0);
		//&& ($('#newConvenio').val().length > 0) && ($('#newConceptValue').val().length >= 1) && ($('#newConceptDataType').val().length >= 1)
        break;
		case 'editNewsByBillDevo':
		direccion='personalBySchool.php';
		form='#editNewsByBillDevo';  
		aswer=($('#iUserIDDevo').val().length > 0);
		//&& ($('#newConvenio').val().length > 0) && ($('#newConceptValue').val().length >= 1) && ($('#newConceptDataType').val().length >= 1)
        break;
}

	if( aswer ){

		toastr.info("<br><button type='button' id='confirmationRevertYes' class='btn green'>Si</button><button type='button' id='confirmationRevertNo' class='btn default'>No</button>",'Desea Guardar los Datos?',
  {
      closeButton: false,
      allowHtml: true,
      onShown: function (toast) {
		  
          $("#confirmationRevertYes").click(function(){
            	$("#sAction").val(action);
				$.ajax({  
				type: "post",  
				data: $(form).serialize(),
				dataType: "json",  
				url: direccion,  
				success: function(data) {
					if(data.status == "OK"){
						toastr.success('Ha modificado el elemento.', 'Datos Guardados correctamente.');
						 $('#full-widthEdit').modal('hide');
if(action!='editServiceByUser'){
	$("#formSearch").submit();
}
						//$("#formSearch").submit();
					}else{
						toastr.error('Completo todos los datos?','Datos No guardados');
					}
				},
				error:function (xhr, ajaxOptions, thrownError){
					toastr.error('Completo todos los datos?','Datos No guardados');
					//$.notific8(data.status);
						//showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
				}
				});
          });
		  
		   $("#confirmationRevertNo").click(function(){
						toastr.info('Cancelado por Usuario.', 'Datos No Guardados.');
						//toastr.success('Datos No guardados', 'Editar Cargo');
						 $('#full-widthEdit').modal('hide');
						
				});

        }
  });
		

	
		
		}else{
			toastr.warning('Completo todos los datos?','Datos No guardados');
			//$.notific8(data.status);
			//showErrorToast("El usuario no existe. Modifique los datos y reintente");
		}
	}
	


  

function activeTypeValue(type){
		var desde = new Date(document.getElementsByName('editCargoDesde-'+type.substr(-1))[0].value);
		var hasta= new Date(document.getElementsByName('editCargoHasta-'+type.substr(-1))[0].value);
		var diasdif= hasta.getTime()-desde.getTime();
		document.getElementsByName('editCargoDias-'+type.substr(-1))[0].value=Math.round(diasdif/(1000*60*60*24))+1;	
} 

function showMeca(id){
element = document.getElementById("showMeca");
        if (id==1) {
            element.style.display='block';
        }
        else {
            element.style.display='none';
			document.getElementById("newMsAporte").value=0;
			document.getElementById("newMcAporte").value=0;
			document.getElementById("newMfamiliar").value=0;
        }	
}


function editOneItemFiles(action){
var formdata = false;
switch (action) {
		case 'editProvider':
		direccion='providerABM.php';
		form='#editProvider';
		var form = $('#editProvider')[0];
		// Create an FormData object
        formdata = new FormData(form);	
		aswer=($('#editProveedor').val().length > 0) && ($('#editRazon').val().length > 0) && ($('#editLocalidad').val().length >= 1) && ($('#editDomicilio').val().length >= 1)&& ($('#editCuit').val().length >= 1);
		break;
		
		case 'editMovementsById':
		direccion='inABM.php';
		form='#editMovementsById';
		var form = $('#editMovementsById')[0];
		// Create an FormData object
        formdata = new FormData(form);	
		aswer=($('#editProvider').val().length > 0) && ($('#editNumero').val().length > 0) && ($('#editCantidad').val().length >= 1) && ($('#editPrecio').val().length >= 0)&& ($('#editTotal').val().length >= 0);
		break;
		
		case 'newMovementsById':
		direccion='inABM.php';
		form='#newMovementsById';
		var form = $('#newMovementsById')[0];
		// Create an FormData object
        formdata = new FormData(form);	
		aswer=($('#editProviderFC').val().length > 0) && ($('#editNumeroFC').val().length > 0) && ($('#editCantidadFC').val().length >= 1) && ($('#editPrecioFC').val().length >= 0)&& ($('#editTotalFC').val().length >= 0);
		break;
		
		case 'editProducts':
		direccion='productsABM.php';
		form='#editProducts';
		var form = $('#editProducts')[0];
		// Create an FormData object
        formdata = new FormData(form);	
		aswer=($('#editDescription').val().length > 0) && ($('#editStock').val().length > 0) && ($('#editPrecio').val().length >= 1) && ($('#editPrecioPromedio').val().length >= 1);
        break;
		case 'editProductsUser':
		direccion='productsABMUser.php';
		form='#editProductsUser';
		var form = $('#editProductsUser')[0];
		// Create an FormData object
        formdata = new FormData(form);	
		aswer=($('#editDescription').val().length > 0);
        break;
		case 'editSchoolBilling':
		direccion='facturacionABM.php';
		form='#editSchoolBilling';
		var form = $('#editSchoolBilling')[0];
		// Create an FormData object
        formdata = new FormData(form);	
		aswer=($('#editTotalValue').val().length >= 0);
        break;
		case 'editActivitie':
		direccion='activitiesABM.php';
		form='#editActivitie';
		var form = $('#editActivitie')[0];
		// Create an FormData object
        formdata = new FormData(form);			
		aswer=($('#editEmpleador').val().length > 0) && ($('#editNivel').val().length > 0) && ($('#editDipregep').val().length >= 1) && ($('#editSub').val().length >= 1)&& ($('#editCuit').val().length >= 1);
        break;
		case 'editMove':
		direccion='outABM.php';
		form='#editMove';
		var form = $('#editMove')[0];
		// Create an FormData object
        formdata = new FormData(form);			
		aswer=($('#editStock').val().length > 0) && ($('#editPrecio').val().length > 0) && ($('#editPrecioTotal').val().length >= 1);
        break;
}

	if( aswer ){

		toastr.info("<br><button type='button' id='confirmationRevertYes' class='btn green'>Si</button><button type='button' id='confirmationRevertNo' class='btn default'>No</button>",'Desea Guardar los Datos?',
  {
      closeButton: false,
      allowHtml: true,
      onShown: function (toast) {
		  
          $("#confirmationRevertYes").click(function(){
            	$("#sAction").val(action);
				$.ajax({  
				type: "post",  
				 data        : formdata ? formdata : $(form).serialize(),
				 enctype: 'multipart/form-data',				
				 processData: false,
				contentType: false,
				dataType: "json",  
				url: direccion,  
				success: function(data) {
					if(data.status == "OK"){
						toastr.success('Ha modificado el elemento.', 'Datos Guardados correctamente.');
						 $('#full-widthEdit').modal('hide');
						 $('#full-widthEditForNote').modal('hide');
						$("#formSearch").submit();
					}else{
						toastr.error('Completo todos los datos?','Datos No guardados');
					}
				},
				error:function (xhr, ajaxOptions, thrownError){
					toastr.error('Completo todos los datos?','Datos No guardados');
					//$.notific8(data.status);
						//showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
				}
				});
          });
		  
		   $("#confirmationRevertNo").click(function(){
						toastr.info('Cancelado por Usuario.', 'Datos No Guardados.');
						//toastr.success('Datos No guardados', 'Editar Cargo');
						 $('#full-widthEdit').modal('hide');
						
				});

        }
  });
		

	
		
		}else{
			toastr.warning('Completo todos los datos?','Datos No guardados');
			//$.notific8(data.status);
			//showErrorToast("El usuario no existe. Modifique los datos y reintente");
		}
	}	
	
//new

function newOneItem(action){

	 switch (action) {
    case 'newSchoolCharges':
		direccion='chargesABM.php';
		form='#newSchoolCharges';  
		aswer=($('#newCargo').val().length > 3);
        break;
	case 'newProviderLevels':
		direccion='levelsABM.php';
		form='#newProviderLevels';  
		aswer=($('#newNivel').val().length > 3);
        break;
		case 'newProviderLevelsUser':
		direccion='levelsABMUser.php';
		form='#newProviderLevelsUser';  
		aswer=($('#newNivel').val().length > 3);
        break;
		case 'newProductsCategory':
		direccion='categoryABM.php';
		form='#newProductsCategory';  
		aswer=($('#newNivel').val().length > 3);
		break;
		case 'newProductsCategoryUser':
		direccion='categoryABMUser.php';
		form='#newProductsCategoryUser';  
		aswer=($('#newNivel').val().length > 3);
		break;
		case 'newWorks':
		direccion='works.php';
		form='#newWorks';  
		aswer=($('#newNivel').val().length > 3);
		break;
		case 'newWorksUser':
		direccion='worksUser.php';
		form='#newWorksUser';  
		aswer=($('#newNivel').val().length > 3);
		break;
		case 'newServices':
		direccion='services.php';
		form='#newServices';  
		aswer=($('#newNivel').val().length > 3);
        break;
		case 'newClose':
		direccion='close.php';
		form='#newClose';  
		aswer=($('#newYear').val().length > 3);
        break;
		case 'newSchoolLabor':
		direccion='laborABM.php';
		form='#newSchoolLabor';  
		aswer=($('#newSindicato').val().length > 3);
        break;
		case 'newAgreement':
		direccion='agreementABM.php';
		form='#newAgreement';  
		aswer=($('#newConvenio').val().length > 3);
        break;
		case 'newSocialWork':
		direccion='socialWork.php';
		form='#newSocialWork';  
		aswer=($('#newCode').val().length > 0) && ($('#newNombre').val().length > 2);
        break;
		case 'newPersonal':
		direccion='personalABM.php';
		form='#newPersonal';  
		aswer=($('#newApellido').val().length > 2) && ($('#newNombre').val().length > 2) && ($('#newObraSocial').val().length >= 1) && ($('#newTipoDocumento').val().length >= 1)&& ($('#newCuit').val().length >= 1)&& ($('#newDocumento').val().length >= 1)&& ($('#newCategoria').val().length >= 1)&& ($('#newEstadoCivil').val().length >= 1);
        break;
		case 'newUser':
			direccion='usersABM.php';
			form='#newUser';  
			aswer=($('#newApellido').val().length > 2) && ($('#newNombre').val().length > 2) && ($('#newTipoDocumento').val().length >= 1) && ($('#newDocumento').val().length >= 1)&& ($('#newCategoria').val().length >= 1)&& ($('#newEstadoCivil').val().length >= 1);
			break;
		case 'newPersonalByBill':
		direccion='personalByBill.php';
		form='#newPersonalByBill';  
		aswer=($('#newApellido').val().length > 2) && ($('#newNombre').val().length > 2) && ($('#newTipoDocumento').val().length >= 1)&& ($('#newCuit').val().length >= 1)&& ($('#newDocumento').val().length >= 1)&& ($('#newCategoria').val().length >= 1);
        break;
		case 'newSchool':
		direccion='schoolABM.php';
		form='#newSchool';  
		aswer=($('#newEmpleador').val().length > 0) && ($('#newRazon').val().length > 0) && ($('#newLocalidad').val().length >= 1) && ($('#newDomicilio').val().length >= 1)&& ($('#newCuit').val().length >= 1);
        break;
		case 'insertNewConcept':
		direccion='conceptABM.php';
		form='#insertNewConcept';  
		aswer=($('#newConceptName').val().length > 0);
		//&& ($('#newConvenio').val().length > 0) && ($('#newConceptValue').val().length >= 1) && ($('#newConceptDataType').val().length >= 1)
        break;
		case 'newCategory':
		direccion='categoryABM.php';
		form='#newCategory';  
		aswer=($('#newCategoria').val().length > 0);
		//&& ($('#newConvenio').val().length > 0) && ($('#newConceptValue').val().length >= 1) && ($('#newConceptDataType').val().length >= 1)
        break;
		case 'newCategoryConcept':
		direccion='category_concept_ABM.php';
		form='#newCategoryConcept';  
		aswer=($('#newCategoria').val().length > 0);
		//&& ($('#newConvenio').val().length > 0) && ($('#newConceptValue').val().length >= 1) && ($('#newConceptDataType').val().length >= 1)
        break;
		case 'newCA':
		direccion='chargesByAgreement.php';
		form='#newCA';  
		aswer=($('#newCargo').val().length > 0);
		//&& ($('#newConvenio').val().length > 0) && ($('#newConceptValue').val().length >= 1) && ($('#newConceptDataType').val().length >= 1)
        break;
		case 'newPersonalBySchool':
		direccion='personalBySchool.php';
		form='#newPersonalBySchool';  
		aswer=($('#iUserID').val().length > 0);
		//&& ($('#newConvenio').val().length > 0) && ($('#newConceptValue').val().length >= 1) && ($('#newConceptDataType').val().length >= 1)
        break;
		case 'newMappeoConcept':
		direccion='conceptAfipABM.php';
		form='#newMappeoConcept';  
		aswer=($('#newConceptoAfip').val().length > 0);
		//&& ($('#newConvenio').val().length > 0) && ($('#newConceptValue').val().length >= 1) && ($('#newConceptDataType').val().length >= 1)
        break;
		case 'newPersonalBySchool':
		direccion='personalBySchool.php';
		form='#newPersonalBySchool';  
		aswer=($('#iUserID').val().length > 0);
		//&& ($('#newConvenio').val().length > 0) && ($('#newConceptValue').val().length >= 1) && ($('#newConceptDataType').val().length >= 1)
        break;
		case 'newServiceByUser':
		direccion='usersByServices.php';
		form='#newServiceByUser';  
		aswer=($('#iUserID').val().length > 0);
		//&& ($('#newConvenio').val().length > 0) && ($('#newConceptValue').val().length >= 1) && ($('#newConceptDataType').val().length >= 1)
        break;
		case 'newConceptAfip':
		direccion='conceptAfipSueldoABM.php';
		form='#newConceptAfip';  
		aswer=($('#newConceptName').val().length > 0);
		//&& ($('#newConvenio').val().length > 0) && ($('#newConceptValue').val().length >= 1) && ($('#newConceptDataType').val().length >= 1)
        break;
    
}

	if( aswer ){

		toastr.info("<br><button type='button' id='confirmationRevertYes' class='btn green'>Si</button><button type='button' id='confirmationRevertNo' class='btn default'>No</button>",'Desea Guardar los Datos?',
  {
      closeButton: false,
      allowHtml: true,
      onShown: function (toast) {
		  
       $("#confirmationRevertYes").click(function(){
        $("#sAction").val(action);
				$.ajax({
					type: "post",
					data: $(form).serialize(),
					dataType: "json",  
					url: direccion,  
					success: function(data) {
						console.log(data);
						if(data.status == "OK"){
							toastr.success('Ha Agregado un nuevo elemento.', 'Datos Guardados correctamente.');
							 $('#full-width').modal('hide');
							$("#formSearch").submit();
						}else{
							toastr.error(data.status,'Datos No guardados');
						}
					},
					error:function (xhr, ajaxOptions, thrownError){
						toastr.error(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",'Datos No guardados');
						//$.notific8(data.status);
							//showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
					}
				});
       });
		  
		   $("#confirmationRevertNo").click(function(){
						toastr.info('Cancelado por Usuario.', 'Datos No Guardados.');
						//toastr.success('Datos No guardados', 'Editar Cargo');
						 $('#full-width').modal('hide');
						
				});

        }
  });
		

	
		
		}	else{
			toastr.warning('Completo todos los datos?','Datos No guardados');
			//$.notific8(data.status);
			//showErrorToast("El usuario no existe. Modifique los datos y reintente");
		}
	}	



		
	function edadNewUser(Fecha,o, n){
		var elem = o.name.split('-');
		num = elem[1];
		fecha = new Date(Fecha);
		hoy = new Date();
		ed = parseInt((hoy -fecha)/365/24/60/60/1000);
		
		if (n==1) {
			$("INPUT[name='newUserAge']").each
		(
			function(index, element)
			{
			$(this).val(ed);
			}
		);
		} else {
				$("INPUT[name='editUserAge']").each
		(
			function(index, element)
			{
			$(this).val(ed);
			}
		);
		}
		
		}




function newOneItemFiles(action){
var formdata = false;
	
	 switch (action) {
   
	
	
		case 'newProvider':
		direccion='providerABM.php';
		form='#newProvider';  
		var form = $('#newProvider')[0];
		// Create an FormData object
        formdata = new FormData(form);
		aswer=($('#newProveedor').val().length > 0) && ($('#newRazon').val().length > 0) && ($('#newLocalidad').val().length >= 1) && ($('#newDomicilio').val().length >= 1)&& ($('#newCuit').val().length >= 1);
        break;
		case 'newProducts':
		direccion='productsABM.php';
		form='#newProducts';
		var form = $('#newProducts')[0];
		// Create an FormData object
        formdata = new FormData(form);		
		aswer=($('#newDescription').val().length > 0) && ($('#newNivel').val().length > 0) && ($('#newCategory').val().length >= 1) && ($('#newStock').val().length >= 1)&& ($('#newPrecio').val().length >= 1);
        break;
		
		case 'extraetxt':
		direccion='activitiesABM.php';
		form='#extraetxt';
		var form = $('#extraetxt')[0];
		// Create an FormData object
        formdata = new FormData(form);		
		aswer=($('#myfile').val().length > 0);
        break;
		
		case 'closeMonth':
		direccion='activitiesABM.php';
		form='#closeMonth';
		var form = $('#closeMonth')[0];
		// Create an FormData object
        formdata = new FormData(form);		
		aswer=($('#cerrarMes').val().length > 0);
        break;
		
		
    
}

	if( aswer ){

		toastr.info("<br><button type='button' id='confirmationRevertYes' class='btn green'>Si</button><button type='button' id='confirmationRevertNo' class='btn default'>No</button>",'Desea Guardar los Datos?',
  {
      closeButton: false,
      allowHtml: true,
      onShown: function (toast) {
		  
          $("#confirmationRevertYes").click(function(){
            	$("#sAction").val(action);
				div = document.getElementById('flotante');
				div.style.display = '';
		
		
				$.ajax({  
				type: "post",  
				data        : formdata ? formdata : $(form).serialize(),
				 enctype: 'multipart/form-data',				
				 processData: false,
				contentType: false,
				dataType: "json",  
				url: direccion,  
				success: function(data) {
					if(data.status == "OK"){
						toastr.success('Ha Agregado un nuevo elemento.', 'Datos Guardados correctamente.');
						 div = document.getElementById('flotante');
						 div.style.display = 'none';
						 $('#full-width').modal('hide');
						 $("#formSearch").submit();
					}else{
						toastr.error('Completo todos los datos?','Datos No guardados');
					}
				},
				error:function (xhr, ajaxOptions, thrownError){
					toastr.error('Completo todos los datos?','Datos No guardados');
					//$.notific8(data.status);
						//showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
				}
				});
          });
		  
		   $("#confirmationRevertNo").click(function(){
						toastr.info('Cancelado por Usuario.', 'Datos No Guardados.');
						//toastr.success('Datos No guardados', 'Editar Cargo');
						 $('#full-width').modal('hide');
						
				});

        }
  });
		

	
		
		}else{
			toastr.warning('Completo todos los datos?','Datos No guardados');
			//$.notific8(data.status);
			//showErrorToast("El usuario no existe. Modifique los datos y reintente");
		}
	}



//agregar productos o quitar
function newOneItemProducts(action){
	var formdata = false;
		
		 switch (action) {
			case 'newBill':
			direccion='movementsInProviderABM.php';
			form='#newBill';
			var form = $('#newBill')[0];
			// Create an FormData object
			formdata = new FormData(form);		
			aswer=($('#newProvider').val().length > 0) && ($('#newType').val().length > 0) && ($('#newPoint').val().length >= 1) && ($('#newNume').val().length >= 1)&& ($('#newDate').val().length >= 1);
			break;

			case 'newBillUpdate':
				direccion='movementsOutProviderABM.php';
				form='#newBillUpdate';
				var form = $('#newBillUpdate')[0];
				// Create an FormData object
				formdata = new FormData(form);		
				aswer=($('#newWork').val().length > 0)  &&  ($('#newDate').val().length >= 1);
				break;
				
				case 'newBillUpdateUser':
				direccion='movementsOutProviderABMUser.php';
				form='#newBillUpdateUser';
				var form = $('#newBillUpdateUser')[0];
				// Create an FormData object
				formdata = new FormData(form);		
				aswer=($('#newWork').val().length > 0)  &&  ($('#newDate').val().length >= 1);
				break;

	}
	
		if( aswer ){
	
			toastr.info("<br><button type='button' id='confirmationRevertYes' class='btn green'>Si</button><button type='button' id='confirmationRevertNo' class='btn default'>No</button>",'Desea Guardar los Datos?',
	  {
		  closeButton: false,
		  allowHtml: true,
		  onShown: function (toast) {
			  
			  $("#confirmationRevertYes").click(function(){
					$("#sAction").val(action);
					$.ajax({  
					type: "post",  
					data        : formdata ? formdata : $(form).serialize(),
					 enctype: 'multipart/form-data',				
					 processData: false,
					contentType: false,
					dataType: "json",  
					url: direccion,  
					success: function(data) {
						if(data.status == "OK"){
							
							var prueba = '';
							if (action=='newBill'){
								prueba = 'Factura Agregada: ' + data.lastId;
							}
								
							toastr.options = {
               "showDuration": "600000"
           };
							
							toastr.success('Ha Agregado un nuevo elemento.' + prueba, 'Datos Guardados correctamente.');
							setTimeout(function() { window.location=window.location;},4000);
						
							

						}else{
							toastr.error('Completo todos los datos?','Datos No guardados');
						}
					},
					error:function (xhr, ajaxOptions, thrownError){
						toastr.error('Completo todos los datos?','Datos No guardados');
						//$.notific8(data.status);
							//showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
					}
					});
			  });
			  
			   $("#confirmationRevertNo").click(function(){
							toastr.info('Cancelado por Usuario.', 'Datos No Guardados.');
							//toastr.success('Datos No guardados', 'Editar Cargo');
							 $('#full-width').modal('hide');
							
					});
	
			}
	  });
			
	
		
			
			}else{
				toastr.warning('Completo todos los datos?','Datos No guardados');
				//$.notific8(data.status);
				//showErrorToast("El usuario no existe. Modifique los datos y reintente");
			}
		}






function pidamosCAEA(action){
	var formdata = false;
		
		 switch (action) {
			case 'newSolicitarCaea':
			direccion='https://siwca.com.ar/gestion/includes/afip_new/solicitar.php';
			form='#newSolicitarCaea';
			var form = $('#newSolicitarCaea')[0];
			// Create an FormData object
			formdata = new FormData(form);		
			aswer=($('#newYear').val().length > 0);
			break;
			
			case 'newConsultarCaea':
			direccion='https://siwca.com.ar/gestion/includes/afip_new/consultar.php';
			form='#newConsultarCaea';
			var form = $('#newConsultarCaea')[0];
			// Create an FormData object
			formdata = new FormData(form);		
			aswer=($('#newYear').val().length > 0);
			break;
	}
	
		if( aswer ){
	
			toastr.info("<br><button type='button' id='confirmationRevertYes' class='btn green'>Si</button><button type='button' id='confirmationRevertNo' class='btn default'>No</button>",'Desea solicitar/consultar el CAEA?',
	  {
		  closeButton: false,
		  allowHtml: true,
		  onShown: function (toast) {
			  
			  $("#confirmationRevertYes").click(function(){
					$("#sAction").val(action);
					$.ajax({  
					type: "post",  
					data        : formdata ? formdata : $(form).serialize(),
					 enctype: 'multipart/form-data',				
					 processData: false,
					contentType: false,
					dataType: "json",  
					url: direccion,  
					success: function(data) {
						if(data.status == "OK"){
							
							
							toastr.options = {
               "showDuration": "600000"
           };
						
							
							toastr.success(data.Error, 'Solicitud correctamente.');
						
						
							

						}else{
							toastr.error(data.Error,'Datos No solicitados');
						}
					},
					error:function (xhr, ajaxOptions, thrownError){
						toastr.error('Completo todos los datos?','Datos No solicitados');
						//$.notific8(data.status);
							//showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
					}
					});
			  });
			  
			   $("#confirmationRevertNo").click(function(){
							toastr.info('Cancelado por Usuario.', 'Datos No solicitados.');
							//toastr.success('Datos No solicitados', 'CAEA no solicitado');
							 $('#full-width').modal('hide');
							
					});
	
			}
	  });
			
	
		
			
			}else{
				toastr.warning('Completo todos los datos?','Datos No solicitados');
				
			}
		}

/*

function pidamosCAEA(action){
	
		direccion='https://siwca.com.ar/gestion/includes/afip_new/solicitar.php';
		 $.ajax({                        
           type: "POST",
		   dataType: "json",  		   
           url: direccion,                     
           data: $("#newSolicitarCaea").serialize(), 
           success: function(data)             
           {
           // alert(data); 
	$( ".container" ).empty().append( 
	$(data) ); 			
           }
       });
				
}	*/










	
function searchLiquidation(action){
	
		direccion='settlement.php';
		 $.ajax({                        
           type: "POST",
		   dataType: "json",  		   
           url: direccion,                     
           data: $("#newLiquidation").serialize(), 
           success: function(data)             
           {
            alert(data.mensaje);  
			
		direccion='settlement.php';
		form='#newLiquidation';  
		aswer=($('#newEmpleador').val().length > 0);
		succ='Ha Agregado un Nueva Liquidaci\xf3n. , Liquidaci\xf3n generada correctamente.';
		toShow='1';
		textToShow='Desea Generar la Liquidaci\xf3n?';			
           }
       });
				
}	
	

function enviar_ajax(action){	

    $("#sAction").val('searchLiquidation');
	$.ajax({
	type: 'POST',	
	url: 'settlement.php',
	data: $('#newLiquidation').serialize(),
	success: function(respuesta) {
		
		direccion='settlement.php';
		form='#newLiquidation';  
		aswer=($('#newYear').val().length > 0);
		succ='Ha Agregado un Nueva Liquidaci\xf3n. , Liquidaci\xf3n generada correctamente.';
		toShow='1';
		textToShow=respuesta;
			if( aswer ){

		toastr.info("<br><button type='button' id='confirmationRevertYes' class='btn green'>Si</button><button type='button' id='confirmationRevertNo' class='btn default'>No</button>",textToShow,
  {
      closeButton: false,
      allowHtml: true,
      onShown: function (toast) {
		  
          $("#confirmationRevertYes").click(function(){
				//document.getElementById('container').innerHTML="";
            	$("#sAction").val(action);
				$.ajax({  
				type: "post",  
				data: $(form).serialize(),
				dataType: "json",  
				url: direccion,  
				success: function(data) {
					if(data.status == "OK"){
						toastr.success(succ);
						argumento = data.argumento;
						/*$tpl->set_var("sDataEdit",'<a target="_blank" href="printing.php?data='.$argumento.'"><button type="button" class="btn green-meadow">Ver Recibos</button></a>');
						$tpl->set_var("sDataSee",'<a target="_blank" href="printingEdit.php?data='.$argumento.'"><button type="button"  class="btn yellow-crusta">Editar Liquidacion</button></a>');
						$tpl->set_var("sDataExport",'<a target="_blank" href="printingExport.php?data='.$argumento.'"><button type="button"  class="btn grey-cascade">Unir</button></a>');
						$tpl->set_var("sDataExportAfip",'<a target="_blank" href="printingExportAfip.php?data='.$argumento.'"><button type="button"  class="btn btn-default">AFIP</button></a>');
						$tpl->set_var("sDataTools",'<a target="_blank" href="printing.php?data='.$argumento.'&email=1"><button type="button"  class="btn red">Enviar Por Email</button></a>');
						*/
	

	 switch (toShow) {
    case '1':
		$( ".container" ).empty().append( 
	$('<a target="_blank" href="printingPrint.php?data='+argumento+'"><button type="button" class="btn green-meadow">Para imprimir</button></a>'+
	'<a target="_blank" href="printingEmail.php?data='+argumento+'"><button type="button"  class="btn yellow-crusta">Salen por Email</button></a>') );
        break;
	case '2':
	$( ".container" ).empty().append( 
	$('<a target="_blank" href="printing.php?data='+argumento+'"><button type="button" class="btn green-meadow">Recibos</button></a>'+
	'<a target="_blank" href="realPay.php?data='+argumento+'"><button type="button"  class="btn yellow-crusta">Planilla de Pagos</button></a>'
	+'<a target="_blank" href="sadop.php?data='+argumento+'&type=1"><button type="button"  class="btn grey-cascade">SADOP</button></a>'
	+'<a target="_blank" href="box.php?data='+argumento+'&type=1"><button type="button"  class="btn btn-default">Caja Complementaria</button></a>'
	+'<a target="_blank" href="printing.php?data='+argumento+'&email=1"><button type="button"  class="btn red">Enviar Por Email</button></a>') );
        break;	
		case '3':
		$( ".container" ).empty().append( 
	$('<a target="_blank" href="controlMeca.php?data='+argumento+'&type=1"><button type="button" class="btn green-meadow">Planilla de Control</button></a>'+
	'<a target="_blank" href="controlMeca.php?data='+argumento+'&type=2"><button type="button" class="btn green-meadow">Trabajada</button></a>'+
	'<a target="_blank" href="controlSocialWork.php?data='+argumento+'"><button type="button"  class="btn yellow-crusta">Planilla de Obra Social</button></a>'
	+'<a target="_blank" href="returns.php?data='+argumento+'"><button type="button"  class="btn grey-cascade">Devoluciones DIPREGEP</button></a>'
	+'<a target="_blank" href="printing.php?data='+argumento+'&email=1"><button type="button"  class="btn red">Enviar Por Email</button></a>') );
        break;	
		case '4':
		$( ".container" ).empty().append( 
	$('<a target="_blank" href="printing.php?data='+argumento+'"><button type="button" class="btn green-meadow">Unir</button></a>'+
	'<a target="_blank" href="printingEdit.php?data='+argumento+'"><button type="button"  class="btn yellow-crusta">AFIP</button></a>'
	+'<a target="_blank" href="sadop.php?data='+argumento+'&type=2"><button type="button"  class="btn grey-cascade">SADOP</button></a>'
	+'<a target="_blank" href="box.php?data='+argumento+'&type=2"><button type="button"  class="btn btn-default">Caja Complementaria</button></a>'
	+'<a target="_blank" href="printing.php?data='+argumento+'&email=1"><button type="button"  class="btn red">Enviar Por Email</button></a>') );
        break;	
    
}
	
						
					}else{
						toastr.error('Completo todos los datos?','Datos No generados');
					}
				},
				error:function (xhr, ajaxOptions, thrownError){
					toastr.error('Completo todos los datos?','Datos No generados');
					//$.notific8(data.status);
						//showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
				}
				});
          });
		  
		   $("#confirmationRevertNo").click(function(){
						toastr.info('Cancelado por Usuario.', 'Datos No Generados.');
						//toastr.success('Datos No guardados', 'Editar Cargo');
						 //$('#full-width').modal('hide');
						
				});

        }
  });
		

	
		
		}else{
			toastr.warning('Completo todos los datos?','Datos No guardados');
			//$.notific8(data.status);
			//showErrorToast("El usuario no existe. Modifique los datos y reintente");
		}
		
		
	}
	});
}	
	
	
	
//separo tema liquidaciones y movimientos
function newLiquidation(action){

	 switch (action) {
    case 'newLiquidation':
		direccion='settlement.php';
		form='#newLiquidation';  
		aswer=($('#newEmpleador').val().length > 0);
		succ='Ha Agregado un Nueva Liquidaci\xf3n. , Liquidaci\xf3n generada correctamente.';
		toShow='1';
		textToShow='Desea Generar la Liquidaci\xf3n?';
        break;
	case 'newSearch':
		direccion='movementsABM.php';
		form='#newSearch';  
		aswer=($('#newYear').val().length > 0);
		succ='Ha Generado Resultados. , Liquidaci\xf3n generada correctamente.';
		toShow='1';
		textToShow='Desea Mostrar la Liquidaci\xf3n?';	
        break;
	case 'newSearchResumen':
		direccion='summaryQuery.php';
		form='#newSearchResumen';  
		aswer=($('#detail').val().length > 0);
		succ='Ha Generado Resultados. , Resumenes generados correctamente.';
		toShow='1';
		textToShow='Desea Mostrar los Resumenes?';	
        break;	
	case 'newSearchDelinquent':
		direccion='delinquent.php';
		form='#newSearchDelinquent';		
		aswer=($('#desdeCuota').val() > 0);
		succ='Ha Generado Resultados. , Morosos generados correctamente.';
		toShow='1';
		textToShow='Desea Mostrar los Morosos?';	
        break;		
	case 'newCalculation':
		direccion='calculation.php';
		form='#newCalculation';  
		aswer=($('#newEmpleador').val().length > 0);
		succ='Ha Calculado los Resultados. , Calculos generados correctamente.';
		toShow='2';
		textToShow='Desea Calcular y Emitir la Liquidaci\xf3n?';	
        break;
	case 'newCloseMeca':
		direccion='closeMeca.php';
		form='#newCloseMeca';  
		aswer=($('#newEmpleador').val().length > 0);
		succ='Ha Cerrado la Mecanizada. , Cierre de Mecanizada realizado.';
		toShow='3';
		textToShow='Desea Cerrar la Mecanizada?';	
        break;
	case 'newInfoGroup':
		direccion='infoGroup.php';
		form='#newInfoGroup';  
		aswer=($('#newEmpleador').val().length > 0);
		succ='Se han Generado los Informes. , Informes Generados.';
		toShow='4';
		textToShow='Desea Generar la Informaci\xf3n?';	
        break;				
    
}
//$( ".container" ).remove();

	if( aswer ){

		toastr.info("<br><button type='button' autofocus='true' id='confirmationRevertYes' class='btn green'>Si</button><button type='button' id='confirmationRevertNo' class='btn default'>No</button>",textToShow,
  {
      closeButton: false,
      allowHtml: true,
      onShown: function (toast) {
		  
          $("#confirmationRevertYes").click(function(){
				//document.getElementById('container').innerHTML="";
            	$("#sAction").val(action);
				$.ajax({  
				type: "post",  
				data: $(form).serialize(),
				dataType: "json",  
				url: direccion,  
				success: function(data) {
					if(data.status == "OK"){
						toastr.success(succ);
						argumento = data.argumento;
						type = data.type;
						
						/*$tpl->set_var("sDataEdit",'<a target="_blank" href="printing.php?data='.$argumento.'"><button type="button" class="btn green-meadow">Ver Recibos</button></a>');
						$tpl->set_var("sDataSee",'<a target="_blank" href="printingEdit.php?data='.$argumento.'"><button type="button"  class="btn yellow-crusta">Editar Liquidacion</button></a>');
						$tpl->set_var("sDataExport",'<a target="_blank" href="printingExport.php?data='.$argumento.'"><button type="button"  class="btn grey-cascade">Unir</button></a>');
						$tpl->set_var("sDataExportAfip",'<a target="_blank" href="printingExportAfip.php?data='.$argumento.'"><button type="button"  class="btn btn-default">AFIP</button></a>');
						$tpl->set_var("sDataTools",'<a target="_blank" href="printing.php?data='.$argumento.'&email=1"><button type="button"  class="btn red">Enviar Por Email</button></a>');
						*/

	 switch (toShow) {
    case '1':
	switch (type){
		case 1:
				$( ".container" ).empty().append( 
	$('<a target="_blank" href="printingPrintOut.php?data='+argumento+'"><button type="button" class="btn green-meadow">Imprimir</button></a>'+
	'<a target="_blank" href="printingEmailOut.php?data='+argumento+'"><button type="button"  class="btn yellow-crusta">Por Email</button></a>'
	+'<a target="_blank" href="printingByApp.php?data='+argumento+'&type=1"><button type="button" class="btn green-meadow">APP</button></a>'
	+'<a target="_blank" href="printingByAppEmailWhatsapp.php?data='+argumento+'"><button type="button"  class="btn yellow-crusta">Archivo Email</button></a>' 
+'<a target="_blank" href="liquidationCBU.php?data='+argumento+'&type=1"><button type="button" class="btn green-meadow">Debitos</button></a>'	
+'<a target="_blank" href="liquidationCPE.php?data='+argumento+'&type=1"><button type="button" class="btn yellow-crusta">Pagos Directos</button></a>'	
	+'<a target="_blank" href="summaryReport.php?data='+argumento+'"><button type="button"  class="btn green-meadow">Reporte</button></a>' 
+'<a target="_blank" href="monthsBillingAfip.php?data='+argumento+'&type=1"><button type="button" class="btn yellow-crusta">AFIP otro</button></a>'
+'<a target="_blank" href="newspaper.php?data='+argumento+'"><button type="button"  class="btn green-meadow">IVA-Ventas</button></a>' 
+'<a target="_blank" href="liquidationRipsa.php?data='+argumento+'&type=1"><button type="button" class="btn yellow-crusta">RIPSA</button></a>'	
	) );
        
		break;
		case 2:
				$( ".container" ).empty().append( 
	$('<a target="_blank" href="printing.php?data='+argumento+'"><button type="button" class="btn green-meadow">Recibos</button></a>'+
	'<a target="_blank" href="printingEdit.php?data='+argumento+'"><button type="button"  class="btn yellow-crusta">Editar</button></a>'
	+'<a target="_blank" href="printingExport.php?data='+argumento+'"><button type="button"  class="btn grey-cascade">Exportar Deno 20</button></a>'
	+'<a target="_blank" href="controlDeno.php?data='+argumento+'"><button type="button"  class="btn grey-cascade">Planilla Control</button></a>'
	+'<a target="_blank" href="printingExport.php?data='+argumento+'"><button type="button"  class="btn grey-cascade">Unir</button></a>'
	+'<a target="_blank" href="printingExportAfip.php?data='+argumento+'"><button type="button"  class="btn btn-default">AFIP</button></a>'
	+'<a target="_blank" href="printing.php?data='+argumento+'&email=1"><button type="button"  class="btn red">Enviar Por Email</button></a>') );
        
		break;
		case 3:
				$( ".container" ).empty().append( 
	$('<a target="_blank" href="printing.php?data='+argumento+'"><button type="button" class="btn green-meadow">Recibos</button></a>'+
	'<a target="_blank" href="printingEdit.php?data='+argumento+'"><button type="button"  class="btn yellow-crusta">Editar</button></a>'
	+'<a target="_blank" href="printingExport.php?data='+argumento+'"><button type="button"  class="btn grey-cascade">Exportar Deno 20</button></a>'
	+'<a target="_blank" href="printingExportAfip.php?data='+argumento+'"><button type="button"  class="btn btn-default">AFIP</button></a>'
	+'<a target="_blank" href="printing.php?data='+argumento+'&email=1"><button type="button"  class="btn red">Enviar Por Email</button></a>') );
        
		break;
		case 4:
				$( ".container" ).empty().append( 
	$('<a target="_blank" href="printing.php?data='+argumento+'"><button type="button" class="btn green-meadow">Recibos</button></a>'+
	'<a target="_blank" href="printingEdit.php?data='+argumento+'"><button type="button"  class="btn yellow-crusta">Editar</button></a>'
	+'<a target="_blank" href="printingExport.php?data='+argumento+'"><button type="button"  class="btn grey-cascade">Exportar Deno 20</button></a>'
	+'<a target="_blank" href="printingExportAfip.php?data='+argumento+'"><button type="button"  class="btn btn-default">AFIP</button></a>'
	+'<a target="_blank" href="printing.php?data='+argumento+'&email=1"><button type="button"  class="btn red">Enviar Por Email</button></a>') );
        
		break;
			case 5:
				$( ".container" ).empty().append( 
	$('<a target="_blank" href="summaryPayment.php?data='+argumento+'"><button type="button" class="btn green-meadow">Resumen Cobranzas</button></a>'+
	'<a target="_blank" href="detailPayment.php?data='+argumento+'"><button type="button"  class="btn yellow-crusta">Detalle de Cobranzas</button></a>'
	+'<a target="_blank" href="detailInvoices.php?data='+argumento+'"><button type="button"  class="btn grey-cascade">Detalle de Recibos</button></a>') );
        
		break;
		case 6:
				$( ".container" ).empty().append( 
	$('<a target="_blank" href="badList.php?data='+argumento+'"><button type="button" class="btn green-meadow">Listado de Morosos</button></a>'+
	'<a target="_blank" href="debt.php?data='+argumento+'"><button type="button"  class="btn yellow-crusta">Reclamo de Deuda</button></a>'+
	'<a target="_blank" href="debt.php?data='+argumento+'&email=1"><button type="button"  class="btn grey-cascade">Reclamo de Deuda x Email</button></a>') );
        
		break;
	}
break;
	case '2':
	$( ".container" ).empty().append( 
	$('<a target="_blank" href="printing.php?data='+argumento+'"><button type="button" class="btn green-meadow">Recibos</button></a>'+
	'<a target="_blank" href="realPay.php?data='+argumento+'"><button type="button"  class="btn yellow-crusta">Planilla de Pagos</button></a>'
	+'<a target="_blank" href="sadop.php?data='+argumento+'&type=1"><button type="button"  class="btn grey-cascade">SADOP</button></a>'
	+'<a target="_blank" href="box.php?data='+argumento+'&type=1"><button type="button"  class="btn btn-default">Caja Complementaria</button></a>'
	+'<a target="_blank" href="printing.php?data='+argumento+'&email=1"><button type="button"  class="btn red">Enviar Por Email</button></a>') );
        break;	
		case '3':
		$( ".container" ).empty().append( 
	$('<a target="_blank" href="controlMeca.php?data='+argumento+'&type=1"><button type="button" class="btn green-meadow">Planilla de Control</button></a>'+
	'<a target="_blank" href="controlMeca.php?data='+argumento+'&type=2"><button type="button" class="btn green-meadow">Trabajada</button></a>'+
	'<a target="_blank" href="controlSocialWork.php?data='+argumento+'"><button type="button"  class="btn yellow-crusta">Planilla de Obra Social</button></a>'
	+'<a target="_blank" href="returns.php?data='+argumento+'"><button type="button"  class="btn grey-cascade">Devoluciones DIEGEP</button></a>'
	+'<a target="_blank" href="printing.php?data='+argumento+'&email=1"><button type="button"  class="btn red">Enviar Por Email</button></a>') );
        break;	
		case '4':
		$( ".container" ).empty().append( 
	$('<a target="_blank" href="printingExportLink.php?data='+argumento+'"><button type="button" class="btn green-meadow">Unir</button></a>'+
	'<a target="_blank" href="printingExportLinkSecction.php?data='+argumento+'"><button type="button" class="btn green-meadow">Unir Nivel</button></a>'+
	'<a target="_blank" href="printingExportLinkAlta.php?data='+argumento+'"><button type="button" class="btn green-meadow">Unir Alta</button></a>'+
	'<a target="_blank" href="printingExportLinkFuturos.php?data='+argumento+'"><button type="button" class="btn green-meadow">Unir Futuros</button></a>'+
	'<a target="_blank" href="printingExportAfipLink.php?data='+argumento+'"><button type="button"  class="btn yellow-crusta">AFIP</button></a>'
	+'<a target="_blank" href="sadop.php?data='+argumento+'&type=2"><button type="button"  class="btn grey-cascade">SADOP</button></a>'
	+'<a target="_blank" href="box.php?data='+argumento+'&type=2"><button type="button"  class="btn btn-default">Caja Complementaria</button></a>'
	+'<a target="_blank" href="printing.php?data='+argumento+'&email=1"><button type="button"  class="btn red">Enviar Por Email</button></a>') );
        break;	
}
	
						
					}else{
						toastr.error('Completo todos los datos?','Datos No generados');
					}
				},
				error:function (xhr, ajaxOptions, thrownError){
					toastr.error('Completo todos los datos?','Datos No generados');
					//$.notific8(data.status);
						//showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
				}
				});
          });
		  
		   $("#confirmationRevertNo").click(function(){
						toastr.info('Cancelado por Usuario.', 'Datos No Generados.');
						//toastr.success('Datos No guardados', 'Editar Cargo');
						 //$('#full-width').modal('hide');
						
				});

        }
  });
		

	
		
		}else{
			toastr.warning('Completo todos los datos?','Datos No guardados');
			//$.notific8(data.status);
			//showErrorToast("El usuario no existe. Modifique los datos y reintente");
		}
	}	
	

//adjuntar notas por email y novedades

function newNote(action,type){
alert(type);
	 switch (action) {
    case 'upload':
		direccion='noteEmail.php';
		form='#upload';  
		aswer=($('#title').val().length > 0);
		var form = $('#upload')[0];
		// Create an FormData object
        var data = new FormData(form);
		//succ='Ha Agregado un Nueva Liquidaci�n. , Liquidaci\xf3n generada correctamente.';
        break;
	case 'newSearch':
		direccion='movementsABM.php';
		form='#newSearch';  
		aswer=($('#title').val().length > 0);
		//succ='Ha Generado Resultados. , Liquidaci\xf3n generada correctamente.';
        break;	
    
}
//$( ".container" ).remove();

	if( aswer ){

		toastr.info("<br><button type='button' id='confirmationRevertYes' class='btn green'>Si</button><button type='button' id='confirmationRevertNo' class='btn default'>No</button>",'Desea enviar el archivo por email?',
  {
      closeButton: false,
      allowHtml: true,
      onShown: function (toast) {
		  
          $("#confirmationRevertYes").click(function(){
            	$("#sAction").val(action);
				$.ajax({
				type: "post", 
				enctype: 'multipart/form-data',				
				data: data,
				 processData: false,
				contentType: false,
				dataType: "json",  
				url: direccion,  
				success: function(data) {
					if(data.queryStatus == "OK"){
						toastr.success(data.enviados+'<br>'+data.errores);
						if(type==1){
							$('#full-widthEmail').modal('hide');
						}else {
							$('#full-width').modal('hide');
						}
						
						$("#formSearch").submit();
					}else{
						toastr.error(data.queryStatus,'Datos No Enviados');
					}
				},
				error:function (xhr, ajaxOptions, thrownError){
					toastr.error('Completo todos los datos?','Datos No Enviados');
					//$.notific8(data.status);
						//showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
				}
				});
          });
		  
		   $("#confirmationRevertNo").click(function(){
						toastr.info('Cancelado por Usuario.', 'Datos No Enviados.');
						//toastr.success('Datos No guardados', 'Editar Cargo');
						 //$('#full-width').modal('hide');
						
				});

        }
  });
		}else{
			toastr.warning('Completo todos los datos?','Datos No Enviados');
			//$.notific8(data.status);
			//showErrorToast("El usuario no existe. Modifique los datos y reintente");
		}
	}	
	
//cargar los meses y a�o, antiguedad y receso
function carg(elemento,entrada,entradaM) {
 
  var input = document.getElementById(entrada);
  var inputM = document.getElementById(entradaM);
  d = elemento.value;
 
  
  if(d == "2" || d=="10" || d=="11" || d=="12"){
    input.disabled = false;
    inputM.disabled = false;
  }else{
	input.disabled = true;
    inputM.disabled = true;
  }
  
}


 	function showPersonalBySchoolEditBill(ID,iID,iMonth,iYear){
		document.getElementById("iBillDes").value = iID;
		document.getElementById("iMonthDes").value = iMonth;
		document.getElementById("iYearDes").value = iYear;
		var dataString = 'sAction=searchPersonalBySchoolId&pBsID=' + ID +'&iMonth='+iMonth+'&iYear='+iYear;
		$.ajax({  
		type: "post",   
		data: dataString, 	
		dataType: "json",  
		url: "personalBySchool.php",  
		success: function(data) {

		if(data){
		if(data.queryStatus == "OK"){
		document.getElementById("iUserID").value = ID;
		//vacio las novedades asi cargo las correctas
		var tableHeaderRowCount = 1;
		var table = document.getElementById('editCargos');
		var rowCount = table.rows.length;
		for (var i = tableHeaderRowCount; i < rowCount; i++) {
		table.deleteRow(tableHeaderRowCount);
		}
		s=0;

		//getChargesByAgreementIn(data.agreement,2,data.charge);

		if (!!data  && !!data.charges && data.charges.length>0){
		for (g = 0; g < data.charges.length; g++) {
		$( "#descripcionEdit" ).clone().attr('id', $( "#descripcionEdit" ).attr("id")+(++s)).appendTo( "#editCargos" );
		$( "#descripcionEdit"+s ).css("display","");

		$("#descripcionEdit"+s+" select" ).each(function(index){
		$(this).attr("name",$(this).attr("name")+s);
		$(this).val(data.charges[g]['news']);
		$(this).attr("selected","'selected'");
		if($(this).val() == data.charges[g]['news']){
		$(this).attr("selected","'selected'");
		}						
		});

		$("#descripcionEdit"+s+" input" ).each(function(index){
		//alert(this);
		$(this).attr("name",$(this).attr("name")+s);
		//$('input[name=editCargo-]'+s).val(data.charges[g]['news']);
		//$('input[name=editCargoTipo-]'+s).val(data.charges[g]['type']);
		$("#editCargo-"+s).val(data.charges[g]['news']);
		$("#editCargoTipo-"+s).val(data.charges[g]['type']);
		$("#editCargoDevo-"+s).val(data.charges[g]['typeUp']);
		$("#editCargoDesde-"+s).val(data.charges[g]['dateBegin']);
		$("#editCargoHasta-"+s).val(data.charges[g]['dateEnd']);
		$("#editCargoDias-"+s).val(data.charges[g]['value']);
		$("#editCargoMoti-"+s).val(data.charges[g]['reason']);

		});

		$("SELECT[name='editCargoTipo-"+s+"']").each
		(
		function(index, element)
		{
		$(this).select(data.charges[g]['type']);
		$(this).val(data.charges[g]['type']);
		}
		);

		$("SELECT[name='editCargoDevo-"+s+"']").each
		(
		function(index, element)
		{
		$(this).select(data.charges[g]['typeUp']);
		$(this).val(data.charges[g]['typeUp']);
		}
		);

		$("INPUT[name='editCargoDesde-"+s+"']").each
		(
		function(index, element)
		{
		$(this).val(data.charges[g]['dateBegin']);
		}
		);

		$("INPUT[name='editCargoHasta-"+s+"']").each
		(
		function(index, element)
		{
		$(this).val(data.charges[g]['dateEnd']);
		}
		);


		$("INPUT[name='editCargoDias-"+s+"']").each
		(
		function(index, element)
		{
		$(this).val(data.charges[g]['value']);
		}
		);

		$("INPUT[name='editCargoMoti-"+s+"']").each
		(
		function(index, element)
		{
		$(this).val(data.charges[g]['reason']);
		}
		);

		$("INPUT[name='editCargo-"+s+"']").each
		(
		function(index, element)
		{
		$(this).val(data.charges[g]['news']);
		}
		);

		$("INPUT[name='editCargoDevo-"+s+"']").each
		(
		function(index, element)
		{
		$(this).val(data.charges[g]['typeUp']);
		}
		);


		$("INPUT[name='cantEditCargo]").each
		(
		function(index, element)
		{
		$(this).val(s);
		}
		);

		document.getElementById("cantEditCargo").value = s;

		}

		} 
		}else{
		showErrorToast(data.queryStatus);
		}
		}else{
		showErrorToast("NO DATA RETURNED");
		}
		},
		error:function (xhr, ajaxOptions, thrownError){
		showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		}
		});  
		return false;  
	}		
		
		//devoluciones
		
	function showPersonalBySchoolEditBillDes(ID,iID,iMonth,iYear){
		document.getElementById("iBillDevo").value = iID;
		document.getElementById("iMonthDevo").value = iMonth;
		document.getElementById("iYearDevo").value = iYear;
		var dataString = 'sAction=searchPersonalBySchoolIdDevo&pBsID=' + ID+'&iMonth='+iMonth+'&iYear='+iYear;
		$.ajax({  
		type: "post",   
		data: dataString, 	
		dataType: "json",  
		url: "personalBySchool.php",  
		success: function(data) {

		if(data){
		if(data.queryStatus == "OK"){
		document.getElementById("iUserIDDevo").value = ID;
		//vacio las novedades asi cargo las correctas
		var tableHeaderRowCount = 1;
		var table = document.getElementById('editCargosDevo');
		var rowCount = table.rows.length;
		for (var i = tableHeaderRowCount; i < rowCount; i++) {
		table.deleteRow(tableHeaderRowCount);
		}
		s=0;

		//getChargesByAgreementIn(data.agreement,2,data.charge);
			
		if (!!data  && !!data.charges && data.charges.length>0){
		for (g = 0; g < data.charges.length; g++) {
		$( "#descripcionEditDevo" ).clone().attr('id', $( "#descripcionEditDevo" ).attr("id")+(++s)).appendTo( "#editCargosDevo" );
		$( "#descripcionEditDevo"+s ).css("display","");

		$("#descripcionEditDevo"+s+" select" ).each(function(index){
		$(this).attr("name",$(this).attr("name")+s);
		$(this).val(data.charges[g]['news']);
		$(this).attr("selected","'selected'");
		if($(this).val() == data.charges[g]['news']){
		$(this).attr("selected","'selected'");
			}						
		});

		$("#descripcionEditDevo"+s+" input" ).each(function(index){
		//alert(this);
		$(this).attr("name",$(this).attr("name")+s);
		//$('input[name=editCargo-]'+s).val(data.charges[g]['news']);
		//$('input[name=editCargoTipo-]'+s).val(data.charges[g]['type']);
		$("#editCargoDevo-"+s).val(data.charges[g]['news']);
		$("#editCargoDesdeDevo-"+s).val(data.charges[g]['cant']);
		$("#editCargoHastaDevo-"+s).val(data.charges[g]['total']);
		$("#editCargoMotiDevo-"+s).val(data.charges[g]['reason']);

		});


		$("SELECT[name='editCargoDevo-"+s+"']").each
		(
		function(index, element)
		{
		$(this).select(data.charges[g]['news']);
		$(this).val(data.charges[g]['news']);
		}
		);

		$("INPUT[name='editCargoDesdeDevo-"+s+"']").each
		(
		function(index, element)
		{
		$(this).val(data.charges[g]['cant']);
		}
		);

		$("INPUT[name='editCargoHastaDevo-"+s+"']").each
		(
		function(index, element)
		{
		$(this).val(data.charges[g]['total']);
		}
		);



		$("INPUT[name='editCargoMotiDevo-"+s+"']").each
		(
		function(index, element)
		{
		$(this).val(data.charges[g]['reason']);
		}
		);


		document.getElementById("cantEditCargoDevo").value = s;

		}

		} 
		}else{
		showErrorToast(data.queryStatus);
		}
		}else{
		showErrorToast("NO DATA RETURNED");
		}
		},
		error:function (xhr, ajaxOptions, thrownError){
		showErrorToast(xhr.responseText+"\n\n"+thrownError+"\n\nCONTACTE AL ADMINISTRADOR",true);
		}
		});  
		return false;  
	}		
	
	

