<?php
include_once 'config.php';


if(true){ //(!empty($_SESSION['sysUser']) && isset($_SESSION["sysUser"]) && SYSTEM=="COOPERATIVAS&ASOCIADOS"){
	if(true){ //($user->can(IS_ADMIN,$_SESSION['sysUser'],$cnx) or $user->can(IS_USER,$_SESSION['sysUser'],$cnx) or $user->can(IS_USER_BASIC,$_SESSION['sysUser'],$cnx)) {
		$tpl->set_var("sDisplayUsers","active open selected");
		
		if (true){//($user->can(IS_ADMIN,$_SESSION['sysUser'],$cnx)){
			$tpl->load_file("admin/menu.html","menu");
		} else {
				if ($user->can(IS_USER,$_SESSION['sysUser'],$cnx)){
			$tpl->load_file("user/menu.html","menu");
			} else {
					$tpl->load_file("user_basic/menu.html","menu");
			}
		}
		
		$tpl->load_file("users/usersByServices.html", "bodyContent");
		
		$tpl->set_var("sDisplayError","display:none;");
		$tpl->set_var("sDisplayOK","display:none;");
		$tpl->set_var("sResult","");
		$tpl->set_var("sSysUserName","Administrador");
		
		$sAction = isset($_POST['sAction'])? $_POST['sAction']: null;
		$tpl->set_var("searchCuit", (isset($_POST['searchCuit'])? $_POST['searchCuit']: null));
		$tpl->set_var("searchAbonado",(isset($_POST['searchAbonado'])? $_POST['searchAbonado']: null));
		$tpl->set_var("searchLast",(isset($_POST['searchLast'])? $_POST['searchLast']: null));
		$tpl->set_var("searchName", (isset($_POST['searchName'])? $_POST['searchName']: null) );
		$tpl->set_var("searchCuil",(isset($_POST['searchCuil'])? $_POST['searchCuil']: null) );
		$tpl->set_var("searchSerial",(isset($_POST['searchSerial'])? $_POST['searchSerial']: null) );
		$tpl->set_var("searchEmail",(isset($_POST['searchEmail'])? $_POST['searchEmail']: null));
		$tpl->set_var("searchApellido",(isset($_POST['searchApellido'])? $_POST['searchApellido']: null));
		$tpl->set_var("searchDNI",(isset($_POST['searchDNI'])? $_POST['searchDNI']: null));
		
		
		
		
		switch ($sAction){
			case "editServiceByUser":
			// var_dump($_POST);exit;
			
			  
					$oData					= new stdClass();
					$oData->status 			= (int)($_POST['editEstado']);
					$oData->idAgreement 			= (int)($_POST['editConvenio']);
					$oData->idUser 			= (int)($_POST['iUserIDE']);
					$oData->idProperty 			= (int)($_POST['editTipoPropiedad']);
					$oData->neighborhood 			= (int)($_POST['editBarrio']);
					$oData->address 			= ($_POST['editDomicilio']);
					$oData->addressPostal 			= ($_POST['editDomicilioPostal']);
					$pieces = explode(".", $_POST['editManzanaLote']);
					$oData->apple 			= $pieces[0];
					$oData->portion 			= $pieces[1];
					$oData->email 			= ($_POST['editEmail']);
					$oData->registryNumber 			= ($_POST['editNomenclatura']);

				
					$oData->typeDocument 			= (int)($_POST['editTipoDocumento']);
					
					

					$oData->document 			= ($_POST['editDocumento']);
					$oData->name 			= ($_POST['editNombre']);
					$oData->lastName 			= ($_POST['editApellido']);
					$oData->cbu 			= ($_POST['editCBU']);
					$oData->id 			= ($_POST['iEditID']);
					
					$oData->marca 			= ($_POST['editMarca']);
					$oData->credito 			= ($_POST['editCredito']);

					$oData->memo 			= ($_POST['editMemo']);

					$oData->activationDate 			= ($_POST['editFechaAlta']);
					$oData->cuit 			= ($_POST['editCuit']);

					if ($_POST['editFechaBaja']!='') {
						$oData->fallDate 			= ($_POST['editFechaBaja']);
					} else {
						$oData->fallDate 			= "1925-01-01";
					}
				
					
					$resultEdit = $services->editServiceByUser($oData,$cnx);
					
					
					if($resultEdit->status!="OK"){
				$resultEdit->status = $db->getLabel("lbl_".$oData->status,"SPA",$cnx);
			} else {
				

					if(($services->deleteServicesByServiceById($oData->id,$cnx))>0){
						$resultDelete = $services->deleteServicesByService($oData->id,$cnx);
												
						} 
						
					
						
						for ($i = 1; $i <= ((int)$_POST['cantEditCargo']); $i++) {
							
							if ($_POST['editServicio-'.$i]) {
							$oData1					= new stdClass();
						
							$oData1->idService		= $oData->id;
						
							
							$oData1->tipo 			= ($_POST['editServicio-'.$i]);
							//acta Nacimiento
							$oData1->nombreActa 			= ($_POST['editNombreActa-'.$i]);
							$oData1->apellidoActa 			= ($_POST['editApellidoActa-'.$i]);
							$oData1->lugarActa 			= ($_POST['editLugarNacimientoActa-'.$i]);
							$oData1->provinciaActa 			= ($_POST['editProvinciaActa-'.$i]);
							$oData1->paisActa 			= ($_POST['editPaisActa-'.$i]);
							$oData1->fechaActa 			= ($_POST['editFechaActa-'.$i]);
							$oData1->nacionalidadActa 			= ($_POST['editNacionalidadActa-'.$i]);
							$oData1->dniActa 			= ($_POST['editDniActa-'.$i]);
							$oData1->padreActa 			= ($_POST['editNomPadreActa-'.$i]);
							$oData1->padreApeActa 			= ($_POST['editApePadreActa-'.$i]);
							$oData1->madreActa 			= ($_POST['editNomMadreActa-'.$i]);
							$oData1->madreApeActa 			= ($_POST['editApeMadreActa-'.$i]);
							
							$oData1->abueloActa 			= ($_POST['editNomAbueloActa-'.$i]);
							$oData1->abueloApeActa 			= ($_POST['editApeAbueloActa-'.$i]);
							$oData1->abuelaActa 			= ($_POST['editNomAbuelaActa-'.$i]);
							$oData1->abuelaApeActa 			= ($_POST['editApeAbuelaActa-'.$i]);
							
							//matrimonio
							$oData1->lugarMatri 			= ($_POST['editLugarMatri-'.$i]);
							$oData1->fechaMatri 			= ($_POST['editFechaMatri-'.$i]);
							$oData1->paisMatri 			= ($_POST['editPaisMatri-'.$i]);
							$oData1->nomapellidoMatri 			= ($_POST['editApellidoMatri-'.$i]);
							$oData1->nombreMatri 			= ($_POST['editNombreMatri-'.$i]);
							$oData1->lugarNaciMatri 			= ($_POST['editLugarNacimientoMatri-'.$i]);
							$oData1->fechaNaciMatri 			= ($_POST['editFechaNaciMatri-'.$i]);
							$oData1->edadMatri 			= ($_POST['editEdadMatri-'.$i]);
							$oData1->dniMatri 			= ($_POST['editDniMatri-'.$i]);
							$oData1->nacionalidadMatri 			= ($_POST['editNacionalidadMatri-'.$i]);
							$oData1->padreApeEsposoMatri 			= ($_POST['editApePadreEsposoMatri-'.$i]);
							$oData1->padreEsposoMatri 			= ($_POST['editNomPadreEsposoMatri-'.$i]);
							$oData1->madreApeEsposoMatri 			= ($_POST['editApeMadreEsposoMatri-'.$i]);
							$oData1->madreEsposoMatri 			= ($_POST['editNomMadreEsposoMatri-'.$i]);
							
							$oData1->nomapellidoEsMatri 			= ($_POST['editApellidoEsMatri-'.$i]);
							$oData1->nomnombreEsMatri 			= ($_POST['editNombreEsMatri-'.$i]);
							$oData1->lugarEsMatri 			= ($_POST['editLugarNacimientoEsMatri-'.$i]);
							$oData1->fechaNaciEsMatri 			= ($_POST['editFechaNaciEsMatri-'.$i]);
							$oData1->edadEsMatri 			= ($_POST['editEdadEsMatri-'.$i]);
							$oData1->dniEsMatri 			= ($_POST['editDniEsMatri-'.$i]);
							$oData1->nacionalidadEsMatri 			= ($_POST['editNacionalidadEsMatri-'.$i]);
							$oData1->padreApeEsposoEsMatri 			= ($_POST['editApePadreEsposoEsMatri-'.$i]);
							$oData1->padreEsposoEsMatri 			= ($_POST['editNomPadreEsposoEsMatri-'.$i]);
							$oData1->madreApeEsposoEsMatri 			= ($_POST['editApeMadreEsposoEsMatri-'.$i]);
							$oData1->madreEsposoEsMatri 			= ($_POST['editNomMadreEsposoEsMatri-'.$i]);
							
							
							//defunción
							$oData1->nombreDefu 			= ($_POST['editNombreDefu-'.$i]);
							$oData1->apellidoDefu			= ($_POST['editApellidoDefu-'.$i]);
							$oData1->paisDefu 			= ($_POST['editPaisDefu-'.$i]);
							$oData1->lugarDefu 			= ($_POST['editLugarDefuDefu-'.$i]);
							$oData1->fechaDefu			= ($_POST['editFechaDefu-'.$i]);
							$oData1->estadoDefu			= ($_POST['editEstadoDefu-'.$i]);
							$oData1->nombreApellidoDefu			= ($_POST['editNombreApeDefu-'.$i]);
							$oData1->fechaNaciDefu 			= ($_POST['editFechaNaciDefu-'.$i]);
							$oData1->lugarNaciDefu 			= ($_POST['editLugarNacimientoDefu-'.$i]);
							$oData1->dniDefu			= ($_POST['editDniDefu-'.$i]);
							$oData1->nacionalidadDefu 			= ($_POST['editNacionalidadDefu-'.$i]);
							$oData1->profeDefu 			= ($_POST['editProfeDefu-'.$i]);
							$oData1->padreApeDefu			= ($_POST['editApePadreDefu-'.$i]);
							$oData1->padreDefu			= ($_POST['editNomPadreDefu-'.$i]);
							$oData1->madreApeDefu 			= ($_POST['editApeMadreDefu-'.$i]);
							$oData1->madreDefu 			= ($_POST['editNomMadreDefu-'.$i]);
							

						
							$result1 = $services->newServicesByServicesUnits($oData1,$cnx);
						}
					}



			}
		echo json_encode($resultEdit);
		exit;
		
		
	
	case "searchServicesByUserId":
	
		$oData = $services->searchServicesByUserId($_POST['ID'],$cnx);
		//var_dump($oData);exit;
		if($oData['queryStatus'] != "OK"){
			$oData['queryStatus'] = $db->getLabel("lbl_".$oData['queryStatus'],"SPA");
		}
		echo json_encode($oData);
		exit;
	break;
	
	case "newServiceByUser":
	//nuevo colegio
	
			$oData					= new stdClass();
			$oData->idUser 			= (int)($_POST['iUserID']);
			$oData->idAgreement = (int)($_POST['newConvenio']);
			$oData->status 			= (int)($_POST['newEstado']);
			$oData->idProperty 	= (isset($_POST['newTipoPropiedad'])?(int)$_POST['newTipoPropiedad']:null);

			$oData->address 		= ($_POST['newDomicilio']);
			if(isset($_POST['newManzanaLote'])){
				$pieces = explode(".", $_POST['newManzanaLote']);
				$oData->apple 		= $pieces[0];
				$oData->portion 	= $pieces[1];
			}
			$oData->email 			= ($_POST['newEmail']);
			//$oData->activationDate 			= ($_POST['newFechaAlta']);
			$oData->fallDate 		= (isset($_POST['newFechaBaja'])?$_POST['newFechaBaja']:null);//($_POST['newFechaBaja']);
			//$oData->name 			= ($_POST['newNombre']);
			//$oData->lastName 			= ($_POST['newApellido']);
			$oData->registryNumber = ($_POST['newCertificado']);
			$oData->typeDocument 	 = (isset($_POST['newTipoDocumento'])?(int)$_POST['newTipoDocumento']:null);;
			$oData->document 		= ($_POST['newPostilla']);
			$oData->cbu 			= ($_POST['newCBU']);
			//$oData->cuit 			= ($_POST['newCuit']);
			$oData->memo 			= ($_POST['newMemo']);
			$oData->neighborhood= (isset($_POST['newBarrio'])?(int)$_POST['newBarrio']:null);
			//$oData->addressPostal = ($_POST['newDomicilioPostal']);
			//$oData->marca 			= ($_POST['newMarca']);
			//$oData->credito 			= ($_POST['newCredito']);
		
			//var_dump($oData);
			$result = $services->newServicesByUser($oData,$cnx);
			//var_dump($result);exit;
			
			if($result->status!="OK"){
				$result->status = $db->getLabel("lbl_".$oData->status,"SPA",$cnx);
			} else {											
				if ($_POST['cantNewCargo']>0){								
						for ($i = 1; $i <= ($_POST['cantNewCargo']); $i++) {
							//var_dump($_POST['cantEditCargo']);
							if ($_POST['newServicio-'.$i]) {
								$oData1					= new stdClass();
															
								$oData1->tipo 			= ($_POST['newServicio-'.$i]);
								//acta Nacimiento
								$oData1->nombreActa 			= ($_POST['newNombreActa-'.$i]);
								$oData1->apellidoActa 			= ($_POST['newApellidoActa-'.$i]);
								$oData1->lugarActa 			= ($_POST['newLugarNacimientoActa-'.$i]);
								$oData1->provinciaActa 			= ($_POST['newProvinciaActa-'.$i]);
								$oData1->paisActa 			= ($_POST['newPaisActa-'.$i]);
								$oData1->fechaActa 			= ($_POST['newFechaActa-'.$i]);
								$oData1->nacionalidadActa 			= ($_POST['newNacionalidadActa-'.$i]);
								$oData1->dniActa 			= ($_POST['newDniActa-'.$i]);
								$oData1->padreActa 			= ($_POST['newNomPadreActa-'.$i]);
								$oData1->padreApeActa 			= ($_POST['newApePadreActa-'.$i]);
								$oData1->madreActa 			= ($_POST['newNomMadreActa-'.$i]);
								$oData1->madreApeActa 			= ($_POST['newApeMadreActa-'.$i]);
								//abuelos
								$oData1->abueloActa 			= ($_POST['newNomAbueloActa-'.$i]);
								$oData1->abueloApeActa 			= ($_POST['newApeAbueloActa-'.$i]);
								$oData1->abuelaActa 			= ($_POST['newNomAbuelaActa-'.$i]);
								$oData1->abuelaApeActa 			= ($_POST['newApeAbuelaActa-'.$i]);
								
								//matrimonio
								$oData1->lugarMatri 			= ($_POST['newLugarMatri-'.$i]);
								$oData1->fechaMatri 			= ($_POST['newFechaMatri-'.$i]);
								$oData1->paisMatri 			= ($_POST['newPaisMatri-'.$i]);
								$oData1->nomapellidoMatri 			= ($_POST['newApellidoMatri-'.$i]);
								$oData1->nombreMatri 			= ($_POST['newNombreMatri-'.$i]);
								$oData1->lugarNaciMatri 			= ($_POST['newLugarNacimientoMatri-'.$i]);
								$oData1->fechaNaciMatri 			= ($_POST['newFechaNaciMatri-'.$i]);
								$oData1->edadMatri 			= ($_POST['newEdadMatri-'.$i]);
								$oData1->dniMatri 			= ($_POST['newDniMatri-'.$i]);
								$oData1->nacionalidadMatri 			= ($_POST['newNacionalidadMatri-'.$i]);
								$oData1->padreApeEsposoMatri 			= ($_POST['newApePadreEsposoMatri-'.$i]);
								$oData1->padreEsposoMatri 			= ($_POST['newNomPadreEsposoMatri-'.$i]);
								$oData1->madreApeEsposoMatri 			= ($_POST['newApeMadreEsposoMatri-'.$i]);
								$oData1->madreEsposoMatri 			= ($_POST['newNomMadreEsposoMatri-'.$i]);
								
								$oData1->nomapellidoEsMatri 			= ($_POST['newApellidoEsMatri-'.$i]);
								$oData1->nomnombreEsMatri 			= ($_POST['newNombreEsMatri-'.$i]);
								$oData1->lugarEsMatri 			= ($_POST['newLugarNacimientoEsMatri-'.$i]);
								$oData1->fechaNaciEsMatri 			= ($_POST['newFechaNaciEsMatri-'.$i]);
								$oData1->edadEsMatri 			= ($_POST['newEdadEsMatri-'.$i]);
								$oData1->dniEsMatri 			= ($_POST['newDniEsMatri-'.$i]);
								$oData1->nacionalidadEsMatri 			= ($_POST['newNacionalidadEsMatri-'.$i]);
								$oData1->padreApeEsposoEsMatri 			= ($_POST['newApePadreEsposoEsMatri-'.$i]);
								$oData1->padreEsposoEsMatri 			= ($_POST['newNomPadreEsposoEsMatri-'.$i]);
								$oData1->madreApeEsposoEsMatri 			= ($_POST['newApeMadreEsposoEsMatri-'.$i]);
								$oData1->madreEsposoEsMatri 			= ($_POST['newNomMadreEsposoEsMatri-'.$i]);
								
								
								//defunción
								$oData1->nombreDefu 			= ($_POST['newNombreDefu-'.$i]);
								$oData1->apellidoDefu			= ($_POST['newApellidoDefu-'.$i]);
								$oData1->paisDefu 			= ($_POST['newPaisDefu-'.$i]);
								$oData1->lugarDefu 			= ($_POST['newLugarDefuDefu-'.$i]);
								$oData1->fechaDefu			= ($_POST['newFechaDefu-'.$i]);
								$oData1->estadoDefu			= ($_POST['newEstadoDefu-'.$i]);
								$oData1->nombreApellidoDefu			= ($_POST['newNombreApeDefu-'.$i]);
								$oData1->fechaNaciDefu 			= ($_POST['newFechaNaciDefu-'.$i]);
								$oData1->lugarNaciDefu 			= ($_POST['newLugarNacimientoDefu-'.$i]);
								$oData1->dniDefu			= ($_POST['newDniDefu-'.$i]);
								$oData1->nacionalidadDefu 			= ($_POST['newNacionalidadDefu-'.$i]);
								$oData1->profeDefu 			= ($_POST['newProfeDefu-'.$i]);
								$oData1->padreApeDefu			= ($_POST['newApePadreDefu-'.$i]);
								$oData1->padreDefu			= ($_POST['newNomPadreDefu-'.$i]);
								$oData1->madreApeDefu 			= ($_POST['newApeMadreDefu-'.$i]);
								$oData1->madreDefu 			= ($_POST['newNomMadreDefu-'.$i]);
														
								$oData1->idService		= $result->lastId;						
							
								$result1 = $services->newServicesByServicesUnits($oData1,$cnx);							
						  }
				    }
			  }
			}

		  echo json_encode($result);
		exit;
			
	break;
	case "deleteServiceByUser":
			$oData = $services->deleteServiceByUserId($_POST['iID'],$cnx);
			if($oData->status != "OK"){
				$oData->status = $db->getLabel("lbl_".$oData->status,"SPA",$cnx);
			}
			
		echo json_encode($oData);
		exit;
	break;
	
		case "newSearch":
			$oData = $services->getServicesByUsers($_POST['searchAbonado'],$_POST['searchName'],$_POST['searchLast'],$_POST['searchCuil'],$_POST['searchEmail'],$_POST['searchDNI'],$_POST['searchApellido'],$cnx);
			if(count($services->getServicesByUsers($_POST['searchAbonado'],$_POST['searchName'],$_POST['searchLast'],$_POST['searchCuil'],$_POST['searchEmail'],$_POST['searchDNI'],$_POST['searchApellido'],$cnx))>=1){
			foreach ($oData as $Item){
				$tpl->set_var("iId",$Item['id']);
				$tpl->set_var("sApellidoA",($Item['lastName']));
				$tpl->set_var("sApellidoCarpeta",$Item['address']);
				$tpl->set_var("sCiu",$Item['ciu']);
				$tpl->set_var("sNombreA",($Item['name']));
				$tpl->set_var("sTelA",($Item['phone']));
				$tpl->set_var("sEmailA",($Item['email']));
				$tpl->set_var("sEstado",($Item['description']));
				$tpl->set_var("sSerial",($Item['seriales']));
				$tpl->set_var("sDNIA",mb_convert_encoding($Item['document'], 'UTF-8'));
				$tpl->set_var("sCant",($Item['cantidad']));
				$tpl->set_var("sCbu",($Item['cbu']));
				$tpl->set_var("sFecha",mb_convert_encoding($Item['activationDate'], 'UTF-8'));
				$tpl->parse("PERSONALBYSCHOOL",true);
			}
			}
break;

	case "importPersonalBySchool":
	if (!empty($_FILES)) {
 $oName=$oArchivo->saveNews($_FILES["file"]["tmp_name"],'news/'.$_FILES["file"]["name"],1);
				
}		
	break;
	
	case "importAll":
	var_dump("todo se procesa");exit;
	break;


	default:
	break;
}
		

		
			if(count($concept->getAgreements($cnx))>=1){
			foreach ($concept->getAgreements($cnx) as $Item){
				$tpl->set_var("iIdConvenio",$Item['id']);
				$tpl->set_var("sConvenioA",mb_convert_encoding($Item['name'], 'UTF-8'));
				$tpl->parse("CONVENIOS",true);
			}
		}


	/*	if(count($db->getTypesServices($cnx))>=1){
			foreach ($db->getTypesServices($cnx) as $Item){
				$tpl->set_var("iIdProperty",$Item['id']);
				$tpl->set_var("sProperty",mb_convert_encoding($Item['description'], 'UTF-8'));
				$tpl->parse("TIPOSPROPIEDAD",true);
			}
		}
		
		
	/*	if(count($db->getNeighborhood($cnx))>=1){
			foreach ($db->getNeighborhood($cnx) as $Item){
				$tpl->set_var("iIdBarrio",$Item['id']);
				$tpl->set_var("sBarrio",mb_convert_encoding($Item['description'], 'UTF-8'));
				$tpl->parse("BARRIO",true);
			}
		}*/


		if(count($db->getStatusServicesUnits($cnx))>=1){
			foreach ($db->getStatusServicesUnits($cnx) as $Item){
				$tpl->set_var("iIdStatusServices",$Item['id']);
				$tpl->set_var("sStatusServices",mb_convert_encoding($Item['description'], 'UTF-8'));
				$tpl->parse("ESTADOSERVICIOS",true);
			}
		}


	/*	if(count($db->getPersonalDocument($cnx))>=1){
			foreach ($db->getPersonalDocument($cnx) as $Item){
				$tpl->set_var("iIdDNI",$Item['id']);
				$tpl->set_var("sDNI",$Item['name']);
				$tpl->parse("TIPOSDOCUMENTO",true);
			}
		}	
*/



	/*if(count($concept->getCategories($cnx))>=1){
			foreach ($concept->getCategories($cnx) as $Item){
				$tpl->set_var("iIdCategoryA",$Item['id']);
				$tpl->set_var("sCategoriaA",$Item['name']);
				
				//$tpl->set_var("iIdConvenioE",$Item['id']);
				//$tpl->set_var("sConvenioAE",$Item['name']);
				
				//$tpl->set_var("iIdConvenioS",$Item['id']);
				//$tpl->set_var("sConvenioAS",$Item['name']);
				
				//if($_POST['searchConvenio'] && $_POST['searchConvenio']==$Item['id']){
				//$tpl->set_var("sSelectCon",'selected');
		//	} else {
			//	$tpl->set_var("sSelectCon",'');
		//	}
				$tpl->parse("CATEGORIES",true);
				//$tpl->parse("CONVENIOSSEARCH",true);
				//$tpl->parse("CONVENIOSEDIT",true);
			}
		}
		*/
		
		
		/*		if(count($personal->getPersonal())>=1){
			foreach ($personal->getPersonal() as $Item){
				$tpl->set_var("iIdPersonal",$Item['id']);
				$tpl->set_var("sNombre",mb_convert_encoding($Item['name'], 'UTF-8'));
				$tpl->set_var("sApellido",mb_convert_encoding($Item['lastName'], 'UTF-8'));
				$tpl->parse("PERSONAL",true);
			}
		}*/
		
	/*		if(count($school->getSchools())>=1){
			foreach ($school->getSchools() as $Item){
				$tpl->set_var("iIdEmpleado",$Item['id']);
				$tpl->set_var("sEmpleador",($Item['name']));
				$tpl->set_var("iCuit",$Item['cuit']);
				if($_POST['searchEmpleador'] && $_POST['searchEmpleador']==$Item['id']){
				$tpl->set_var("sSelectEmpleado",'selected');
			} else {
				$tpl->set_var("sSelectEmpleado",'');
			}
				
				$tpl->parse("EMPLEADORES",true);
				$tpl->parse("EMPLEADORESSEARCH",true);
			}
		}
		
	
		
			if(count($school->getSchoolLevels())>=1){
			foreach ($school->getSchoolLevels() as $Item){
				$tpl->set_var("iIdNivel",$Item['id']);
				$tpl->set_var("sNivel",$Item['name']);
				if($_POST['searchNivel'] && $_POST['searchNivel']==$Item['id']){
				$tpl->set_var("sSelectNivel",'selected');
			} else {
				$tpl->set_var("sSelectNivel",'');
			}
				//$tpl->parse("Niveles",true);
				$tpl->parse("NivelesSEARCH",true);
			}
		}*/
		
	}else{
		header('location: login.php');
	}
}else{
	header('location: login.php');	
}
$tpl->pparse("main");
?>