<?php
ob_start();
session_start();
/* Establecemos que las paginas no pueden ser cacheadas */
header("Expires: Tue, 01 Jul 2001 06:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

include_once ("includes/cnx.inc");
include_once ("includes/class.Templates.php");
include_once ("includes/class.Database.php");
include_once ("includes/class.Utils.php");
include_once ("includes/class.ServicesUnits.php");
include_once ("includes/class.Archivos.php");
include_once ("includes/constants.php");
include_once ("includes/class.Bill.php");
include_once ("includes/class.User.php");
include_once ("includes/class.ConfigSystem.php");
include_once ("includes/class.Pay.php");
//include_once ("includes/class.Billing.php");
//modulo conceptos de liquidación
include_once ("includes/class.Concept.php");
//include_once ("includes/class.Informacion.php");
//módulo materiales
include_once ("includes/class.Provider.php");
include_once ("includes/class.Products.php");
//módulo personal
include_once ("includes/class.Personal.php");
include_once ("includes/class.Coope.php");
include_once ("includes/class.MaterialsMovements.php");
include_once ("includes/class.Movements.php");
include_once ("includes/class.BCM.php");



$tpl			= new Templates("templates");
$db 			= new DB();
$utils 			= new Utils();
$oArchivo		= new Archivo();
$configSystem   = new configSystem();
$user 			= new User();
$provider		= new Provider();
$products       = new Products();
$personal       = new Personal();
$concept 		= new Concept();
$coope       = new Coope();
$movements = new MaterialsMovements();
$movement = new Movements();
$services = new ServicesUnits();
$bill = new Bill();
$pay = new Pay();


//arturo 20161602265
//manuela Soloyo167156 27316116944

	$sPath = $_SERVER['PHP_SELF'];
	$sFile = basename($sPath);
	
//	if($sFile != "cpanel_admin_products.php"  && $sFile != "cpanel_user.php" && $sFile != "users.php" && $sFile != "advertising.php"  && $sFile != "profile.php" && $sFile != "bill.php" && $sFile != "login.php" && $sFile != "userRegister.php" && $sFile != "viewMail.php"){
	
	/*if($utils->pageWhitOutBar($sFile,$cnx)){
	//cargo el login casi completo	
	$tpl->load_file("frame.html","main");
	$tpl->load_file("/header/header.html","header");
	//$tpl->load_file("/menu/menu.html","menu");
	//$tpl->load_file("/footer/footer.html","footer");	
			
	} else {
		//pagina privada
		$tpl->load_file("frame.html","main");
		$tpl->load_file("/header/header.html","header");
		//$tpl->load_file("/pg/menu/menu.html","menu");
		$tpl->load_file("/footer/footer.html","footer");
	}*/
	
//	var_dump($sPath);
//	var_dump($sFile);exit;
	
	
		if($sPath != "/gestion/calendario/index.php") {
	
	if($utils->pageWhitOutBar($sFile,$cnx)){
	//cargo el login casi completo	
	$tpl->load_file("frame.html","main");
	$tpl->load_file("/header/header.html","header");
	//$tpl->load_file("/menu/menu.html","menu");
	//$tpl->load_file("/footer/footer.html","footer");	
			
	} else {
		//pagina privada
		$tpl->load_file("frame.html","main");
		$tpl->load_file("/header/header.html","header");
		//$tpl->load_file("/pg/menu/menu.html","menu");
		$tpl->load_file("/footer/footer.html","footer");
	}
	
	}
?>